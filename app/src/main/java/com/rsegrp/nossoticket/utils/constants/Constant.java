package com.rsegrp.nossoticket.utils.constants;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public class Constant {

    public static final String CDLP = "1001";

    public static final String TYPE_CHECK_TOKEN = "check_token";
    public static final String TYPE_CHANGE_PASS = "change_pass";

    public static final String ARGS_CARD_ID = "card_id";
    public static final String ARGS_SERIAL = "serial";
    public static final String ARGS_DATE = "date";
    public static final String ARGS_TYPE = "type";

    public static final String TYPE_ITEM_STATION = "TRA";
    public static final String TYPE_ITEM_RECHARGE = "REC";
    public static final String TYPE_ITEM_PROGRESS = "PROGRESS";
    public static final String TYPE_ITEM_NO_ITEM = "NO_DATA";
    public static final String TYPE_ITEM_EMPTY_ITEM = "EMPTY_DATA";

    public static final String TYPE_ITEM_SUMMARY_USER = "USER_REGISTER";
    public static final String TYPE_ITEM_SUMMARY_TITLLE = "NEW_TITTLE";
    public static final String TYPE_ITEM_SUMMARY_WALLET = "RELOAD_WALLET";

    public static final String FRAGMENT_SUMMARY = "summary";
    public static final String FRAGMENT_RECHARGE = "recharge";

    public static final String ARGS_REMAINING = "remaining";
    public static final String ARGS_DATA_USER = "data_user";

    public static final String NEXT_PAGE = "page=";

    public static final String ARGS_DATA_TOKEN = "data_token";

    public static final String ARGS_METHOD = "method";
    public static final String ARGS_TARGET = "target";
    public static final String ARGS_ORIGIN = "origin";
    public static final String ARGS_CODE = "code";
    public static final String ARGS_BALANCE = "balance";
    public static final String ARGS_POSITION = "position";
    public static final String ARGS_DESCRIPTION = "description";

    public static final String BASE_URL_LOCAL = "http://192.168.1.10";
    public static final String BASE_URL = "http://api.nossoticket.com/";

    public static final String PORT_SUMMARY = ":5000/";
    public static final String PORT_USER = ":5001/";
    public static final String PORT_TITLE = ":5002/";
    public static final String PORT_MOVEMENT = ":5003/";

    public static final String CONTENT_TYPE = "application/json;charset=utf-8";
    public static final String CLIENT = "5ac4f674ceb1ab61ab69bec0";
    public static final String SECRET = "e77d1b83-5d7f-433f-8fab-a06f218eaf0e";

    public static final String COLOR_ICON = "#ffffff";
    public static final String NORMAL_COLOR = "#586877";
    public static final String PRESSED_COLOR = "#00bbe1";

    public static final String TOKEN = "Bearer ";

    public static final String RETRY_TAG = "retry";

    public static final String NO_TITLE = "no_title";
    public static final String YES_TITLE = "yes_title";

    public static final String QR_CODE = "qr_code";

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final String FAILED = "failed";

    public static final String PROCESSING = "PROCESSING";
    public static final String PENDING = "PENDING";
    public static final String APPROVED = "APPROVED";
    public static final String DENIED = "DENIED";
    public static final String CANCELED = "CANCELED";

    public static final String DATE_FORMAT_RECEIVED = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String DATE_FORMAT_REQUIRED = "dd MM yyyy HH:mm";

    public static final int ITEM_NULL = -1;

    public static final int ITEM_TYPE_PROGRESS = 0;
    public static final int ITEM_TYPE_END = 1;
    public static final int ITEM_TYPE_DATA = 2;
    public static final int ITEM_TYPE_RECHARGE = 3;
    public static final int ITEM_TYPE_STATION = 4;
    public static final int ITEM_TYPE_EMPTY = 5;

    public static final int ITEM_SUMMARY_USER = 0;
    public static final int ITEM_SUMMARY_TITLLE = 1;
    public static final int ITEM_SUMMARY_WALLET = 2;

    public static final int FADE_DURATION = 1000;
}
