package com.rsegrp.nossoticket.utils.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;

/**
 * Created by ManuelMoure on 07/04/2018.
 */

@SuppressLint("ValidFragment")
public class DialogsTwoBtn extends DialogFragment {

    public interface OnSetClickListener {
        void clickListener(String options);
    }

    OnSetClickListener listener;

    private String msg;
    private String txtBtn1;
    private String txtBtn2;

    @SuppressLint("ValidFragment")
    public DialogsTwoBtn(OnSetClickListener listener, String msg, String txtBtn1, String txtBtn2) {
        this.listener = listener;
        this.msg = msg;
        this.txtBtn1 = txtBtn1;
        this.txtBtn2 = txtBtn2;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return retryDialog();
    }

    private AlertDialog retryDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_alert_connection_error, null);

        builder.setView(view);

        TextView txtMsg = view.findViewById(R.id.textView22);
        Button btnCancel = view.findViewById(R.id.btb_cancel);
        Button btnRetry = view.findViewById(R.id.btn_retry);

        txtMsg.setText(msg);
        btnCancel.setText(txtBtn1);
        btnRetry.setText(txtBtn2);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickListener("cancel");
                dismiss();
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickListener("retry");
                dismiss();
            }
        });
        return builder.create();
    }
}
