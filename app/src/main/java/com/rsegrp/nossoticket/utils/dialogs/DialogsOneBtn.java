package com.rsegrp.nossoticket.utils.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;

/**
 * Created by RSE_VZLA_07 on 12/4/2018.
 */

@SuppressLint("ValidFragment")
public class DialogsOneBtn extends DialogFragment {

    private String msg;
    private String txtBtn;

    public interface OnSetClickListener {
        void clickListener(String options);
    }

    OnSetClickListener listener;

    @SuppressLint("ValidFragment")
    public DialogsOneBtn(OnSetClickListener listener, String msg, String txtBtn) {
        this.listener = listener;
        this.msg = msg;
        this.txtBtn = txtBtn;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return dialogFeedback();
    }

    private AlertDialog dialogFeedback() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_one_btn, null);

        builder.setView(view);

        TextView txtMsg = view.findViewById(R.id.textView222);
        Button btnAccept = view.findViewById(R.id.btn_accept);

        txtMsg.setText(msg);
        btnAccept.setText(txtBtn);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.clickListener("accept");
                dismiss();
            }
        });

        return builder.create();
    }
}
