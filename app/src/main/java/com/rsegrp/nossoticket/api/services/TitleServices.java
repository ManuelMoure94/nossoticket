package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.titles.AddTitle;
import com.rsegrp.nossoticket.api.data.titles.NewTitle;
import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.api.data.titles.UpdateTitle;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.RechargeTittle;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.TittleRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by RSE_VZLA_07 on 9/3/2018.
 */

public interface TitleServices {

    @GET("api/tittle/v1")
    Call<List<Titles>> getTitles(@Header("Content-Type") String contentType,
                                 @Header("Authorization") String token);

    @POST("api/tittle/v1")
    Call<AddTitle> addTitle(@Header("Content-Type") String contentType,
                            @Header("Authorization") String token,
                            @Body NewTitle newTitle);

    @GET("api/tittle/{uid}")
    Call<Titles> getTitle(@Header("Content-Type") String contentType,
                          @Header("Authorization") String token,
                          @Path("uid") String uid);

    @PUT("api/tittle/{uid}")
    Call<Titles> updateTitle(@Header("Content-Type") String contentType,
                             @Header("Authorization") String token,
                             @Body UpdateTitle updateTitle,
                             @Path("uid") String uid);

    @DELETE("api/tittle/{uid}")
    Call deleteTitle(@Header("Content-Type") String contentType,
                     @Header("Authorization") String token,
                     @Path("uid") String uid);

    @POST("api/tittle/v1/reload")
    Call<TittleRequest> tittleRecharge(@Header("Content-Type") String contentType,
                                       @Header("Authorization") String token,
                                       @Body RechargeTittle rechargeTittle);
}
