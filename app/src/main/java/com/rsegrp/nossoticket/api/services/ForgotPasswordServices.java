package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.forgot.ChangePassword;
import com.rsegrp.nossoticket.api.data.forgot.CheckToken;
import com.rsegrp.nossoticket.api.data.forgot.SendEmail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by RSE_VZLA_07 on 30/7/2018.
 */

public interface ForgotPasswordServices {

    @POST("api/user/v1/forgot-password")
    Call<String> ForgotPwSendingMail(@Header("Content-Type") String contentType,
                                     @Header("Client") String client,
                                     @Header("Secret") String secret,
                                     @Body SendEmail email);

    @POST("api/user/v1/check-token")
    Call<String> ForgotPwCheckingToken(@Header("Content-Type") String contentType,
                                       @Header("Client") String client,
                                       @Header("Secret") String secret,
                                       @Body CheckToken checkToken);

    @POST("api/user/v1/change-password")
    Call<String> ForgotPwChangePassword(@Header("Content-Type") String contentType,
                                        @Header("Client") String client,
                                        @Header("Secret") String secret,
                                        @Body ChangePassword changePassword);
}