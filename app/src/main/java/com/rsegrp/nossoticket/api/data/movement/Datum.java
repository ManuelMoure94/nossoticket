package com.rsegrp.nossoticket.api.data.movement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public class Datum {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("tittleId")
    @Expose
    private String tittleId;
    @SerializedName("moment")
    @Expose
    private String moment;
    @SerializedName("nodeCode")
    @Expose
    private String nodeCode;
    @SerializedName("line")
    @Expose
    private String line;
    @SerializedName("station")
    @Expose
    private String station;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("movementType")
    @Expose
    private String movementType;
    @SerializedName("facial")
    @Expose
    private String facial;
    @SerializedName("remaining")
    @Expose
    private String remaining;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTittleId() {
        return tittleId;
    }

    public void setTittleId(String tittleId) {
        this.tittleId = tittleId;
    }

    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getFacial() {
        return facial;
    }

    public void setFacial(String facial) {
        this.facial = facial;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }
}
