package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.movement.Movement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public interface MovementServices {

    @GET("api/movements/v1/{uid}")
    Call<Movement> Movement(@Header("Content-Type") String contentType,
                            @Header("Authorization") String token,
                            @Path("uid") String uid);

    @GET("api/movements/v1/{uid}")
    Call<Movement> NextMovement(@Header("Content-Type") String contentType,
                                @Header("Authorization") String token,
                                @Path("uid") String uid,
                                @Query("page") String page);
}
