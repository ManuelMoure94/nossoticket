package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.singIn.DataNewPassword;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public interface SingInServices {

    @POST("api/auth/v1/sing-in")
    Call<SingIn> singIn(@Header("Content-Type") String contentType,
                        @Header("Client") String client,
                        @Header("Secret") String secret,
                        @Body User user);

    @GET("api/user/v1/get-user")
    Call<DataUser> getUser(@Header("Content-Type") String contentType,
                           @Header("Authorization") String token);

    @PUT("api/user/v1/update-user")
    Call<DataUser> updateUser(@Header("Content-Type") String contentType,
                              @Header("Authorization") String token,
                              @Body DataUser dataUser);

    @PUT("api/user/v1/update-password")
    Call<String> changePassword(@Header("Content-Type") String contentType,
                                @Header("Authorization") String token,
                                @Body DataNewPassword dataNewPassword);
}