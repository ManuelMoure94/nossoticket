package com.rsegrp.nossoticket.api.data.titles;

/**
 * Created by RSE_VZLA_07 on 9/3/2018.
 */

public class NewTitle {

    String tittle;
    String description;

    public NewTitle(String tittle, String description) {
        this.tittle = tittle;
        this.description = description;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
