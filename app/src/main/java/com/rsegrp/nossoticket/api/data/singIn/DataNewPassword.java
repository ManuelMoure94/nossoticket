package com.rsegrp.nossoticket.api.data.singIn;

/**
 * Created by RSE_VZLA_07 on 1/8/2018.
 */

public class DataNewPassword {

    private String email;
    private String password;

    public DataNewPassword(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
