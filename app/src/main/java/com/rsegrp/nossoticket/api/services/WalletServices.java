package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.wallet.history.AllHistory;
import com.rsegrp.nossoticket.api.data.wallet.recharge.RechargeRequest;
import com.rsegrp.nossoticket.api.data.wallet.recharge.WalletRecharge;
import com.rsegrp.nossoticket.api.data.wallet.simpleHistory.SimpleHistory;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ManuelMoure on 19/04/2018.
 */

public interface WalletServices {

    @POST("api/wallet/v1/")
    Call<WalletRecharge> walletRecharge(@Header("Content-Type") String contentType,
                                        @Header("Authorization") String token,
                                        @Body RechargeRequest rechargeRequest);

    @GET("api/wallet/v1/")
    Call<List<AllHistory>> walletHistory(@Header("Content-Type") String contentType,
                                         @Header("Authorization") String token);

    @GET("api/wallet/v1/{uid]")
    Call<SimpleHistory> walletSimpleHistory(@Header("Content-Type") String contentType,
                                            @Header("Authorization") String token,
                                            @Path("uid") String uid);
}
