package com.rsegrp.nossoticket.api.data.forgot;

/**
 * Created by RSE_VZLA_07 on 30/7/2018.
 */

public class SendEmail {

    private String email;

    public SendEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
