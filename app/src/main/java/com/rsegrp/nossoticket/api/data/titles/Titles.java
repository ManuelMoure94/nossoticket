package com.rsegrp.nossoticket.api.data.titles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RSE_VZLA_07 on 9/3/2018.
 */

public class Titles {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("maCardId")
    @Expose
    private String maCardId;
    @SerializedName("cardId")
    @Expose
    private String cardId;
    @SerializedName("itemId")
    @Expose
    private String itemId;
    @SerializedName("tittleType")
    @Expose
    private String tittleType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("remaining")
    @Expose
    private Integer remaining;
    @SerializedName("ifeUpdateAt")
    @Expose
    private String ifeUpdateAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMaCardId() {
        return maCardId;
    }

    public void setMaCardId(String maCardId) {
        this.maCardId = maCardId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTittleType() {
        return tittleType;
    }

    public void setTittleType(String tittleType) {
        this.tittleType = tittleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRemaining() {
        return remaining;
    }

    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    public String getIfeUpdateAt() {
        return ifeUpdateAt;
    }

    public void setIfeUpdateAt(String ifeUpdateAt) {
        this.ifeUpdateAt = ifeUpdateAt;
    }
}
