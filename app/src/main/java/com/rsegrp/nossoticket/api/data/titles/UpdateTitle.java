package com.rsegrp.nossoticket.api.data.titles;

/**
 * Created by RSE_VZLA_07 on 9/3/2018.
 */

public class UpdateTitle {

    private String description;

    public UpdateTitle(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
