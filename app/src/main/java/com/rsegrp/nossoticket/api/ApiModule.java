package com.rsegrp.nossoticket.api;

import com.rsegrp.nossoticket.api.services.ForgotPasswordServices;
import com.rsegrp.nossoticket.api.services.MovementServices;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.SingUpServices;
import com.rsegrp.nossoticket.api.services.SummaryServices;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.api.services.WalletServices;
import com.rsegrp.nossoticket.utils.constants.Constant;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

@Module
public class ApiModule {

    @Provides
    public OkHttpClient providesOkHttpClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Provides
    public Retrofit providesRetrofit(String baseUrl, OkHttpClient client) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    public SingInServices providesSingInServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(SingInServices.class);
    }

    @Provides
    public SingUpServices providesSingUpServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(SingUpServices.class);
    }

    @Provides
    public SummaryServices providesSummaryServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(SummaryServices.class);
    }

    @Provides
    public MovementServices providesMovementServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(MovementServices.class);
    }

    @Provides
    public TitleServices providesTitleServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(TitleServices.class);
    }

    @Provides
    public WalletServices providesWalletServices() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(WalletServices.class);
    }

    @Provides
    public ForgotPasswordServices providesForgotPassword() {
        return providesRetrofit(Constant.BASE_URL,
                providesOkHttpClient()).create(ForgotPasswordServices.class);
    }
}
