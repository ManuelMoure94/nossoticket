package com.rsegrp.nossoticket.api.data.forgot;

/**
 * Created by RSE_VZLA_07 on 31/7/2018.
 */

public class ChangePassword {

    private String email;
    private String id;
    private String password;
    private String passwordConfirmation;

    public ChangePassword(String email, String id, String password, String passwordConfirmation) {
        this.email = email;
        this.id = id;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
