package com.rsegrp.nossoticket.api.data.titles.rechargeTittle;

/**
 * Created by ManuelMoure on 20/04/2018.
 */

public class RechargeTittle {

    private String tittle;
    private float amount;

    public RechargeTittle(String tittle, float amount) {
        this.tittle = tittle;
        this.amount = amount;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
