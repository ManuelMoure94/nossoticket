package com.rsegrp.nossoticket.api.data.singIn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public class SingIn {

    @SerializedName("AccessExpiresAt")
    @Expose
    private String accessExpiresAt;
    @SerializedName("RefreshToken")
    @Expose
    private String refreshToken;
    @SerializedName("AccessToken")
    @Expose
    private String accessToken;
    @SerializedName("RefreshExpiresAt")
    @Expose
    private String refreshExpiresAt;

    public String getAccessExpiresAt() {
        return accessExpiresAt;
    }

    public void setAccessExpiresAt(String accessExpiresAt) {
        this.accessExpiresAt = accessExpiresAt;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshExpiresAt() {
        return refreshExpiresAt;
    }

    public void setRefreshExpiresAt(String refreshExpiresAt) {
        this.refreshExpiresAt = refreshExpiresAt;
    }
}
