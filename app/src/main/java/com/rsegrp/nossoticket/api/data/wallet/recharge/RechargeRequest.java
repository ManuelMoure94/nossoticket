package com.rsegrp.nossoticket.api.data.wallet.recharge;

/**
 * Created by ManuelMoure on 19/04/2018.
 */

public class RechargeRequest {

    private String type;
    private String origin;
    private String destination;
    private String code;
    private double amount;
    private String date;

    public RechargeRequest(String type, String origin, String destination, String code, double amount, String date) {
        this.type = type;
        this.origin = origin;
        this.destination = destination;
        this.code = code;
        this.amount = amount;
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
