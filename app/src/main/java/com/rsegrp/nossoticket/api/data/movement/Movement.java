package com.rsegrp.nossoticket.api.data.movement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public class Movement {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("perPage")
    @Expose
    private Integer perPage;
    @SerializedName("firstPageIndex")
    @Expose
    private Integer firstPageIndex;
    @SerializedName("currentPageIndex")
    @Expose
    private Integer currentPageIndex;
    @SerializedName("lastPageIndex")
    @Expose
    private Integer lastPageIndex;
    @SerializedName("nextPageIndex")
    @Expose
    private Integer nextPageIndex;
    @SerializedName("prevPageIndex")
    @Expose
    private Integer prevPageIndex;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("firstPageUrl")
    @Expose
    private String firstPageUrl;
    @SerializedName("currentPageUrl")
    @Expose
    private String currentPageUrl;
    @SerializedName("lastPageUrl")
    @Expose
    private String lastPageUrl;
    @SerializedName("nextPageUrl")
    @Expose
    private String nextPageUrl;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("to")
    @Expose
    private Integer to;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getFirstPageIndex() {
        return firstPageIndex;
    }

    public void setFirstPageIndex(Integer firstPageIndex) {
        this.firstPageIndex = firstPageIndex;
    }

    public Integer getCurrentPageIndex() {
        return currentPageIndex;
    }

    public void setCurrentPageIndex(Integer currentPageIndex) {
        this.currentPageIndex = currentPageIndex;
    }

    public Integer getLastPageIndex() {
        return lastPageIndex;
    }

    public void setLastPageIndex(Integer lastPageIndex) {
        this.lastPageIndex = lastPageIndex;
    }

    public Integer getNextPageIndex() {
        return nextPageIndex;
    }

    public void setNextPageIndex(Integer nextPageIndex) {
        this.nextPageIndex = nextPageIndex;
    }

    public Integer getPrevPageIndex() {
        return prevPageIndex;
    }

    public void setPrevPageIndex(Integer prevPageIndex) {
        this.prevPageIndex = prevPageIndex;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public String getCurrentPageUrl() {
        return currentPageUrl;
    }

    public void setCurrentPageUrl(String currentPageUrl) {
        this.currentPageUrl = currentPageUrl;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
