package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.singUp.Register;
import com.rsegrp.nossoticket.api.data.singUp.SingUp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public interface SingUpServices {

    @POST("api/user/v1/sing-up")
    Call<SingUp> SingUp(@Header("Content-Type") String contentType,
                        @Header("Client") String client,
                        @Header("Secret") String secret,
                        @Body Register register);
}
