package com.rsegrp.nossoticket.api.data.forgot;

/**
 * Created by RSE_VZLA_07 on 31/7/2018.
 */

public class CheckToken {

    private String email;
    private String id;

    public CheckToken(String email, String id) {
        this.email = email;
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
