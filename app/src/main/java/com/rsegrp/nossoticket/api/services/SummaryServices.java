package com.rsegrp.nossoticket.api.services;

import com.rsegrp.nossoticket.api.data.summary.Summary;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by RSE_VZLA_07 on 8/3/2018.
 */

public interface SummaryServices {

    @GET("api/summary/v1/")
    Call<List<Summary>> getSummary(@Header("Content-Type") String contentType,
                                   @Header("Authorization") String token);
}
