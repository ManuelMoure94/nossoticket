package com.rsegrp.nossoticket.ui.register.mvp;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.api.data.singUp.Register;
import com.rsegrp.nossoticket.api.data.singUp.SingUp;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    private RegisterContract.View view;
    private RegisterContract.Interact interact;

    Response<SingIn> responseSingIn;
    Response<SingUp> responseSingUp;

    private Register register;
    private User user;

    public RegisterPresenter(RegisterContract.View view, RegisterContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnRegister() {
        if (checkField()) {
            view.showProgress();
            register = new Register(view.getEmail(), view.getPassword());
            interact.newUser(register, this);
        }
    }

    @Override
    public void onSuccessSingUn(Response<SingUp> response) {

        if (response != null) {
            responseSingUp = response;
            user = new User(response.body().getEmail(), view.getPassword());
            interact.SingInNewUser(user, this);
        }
    }

    @Override
    public void onSuccessSingIp(Response<SingIn> response) {

        view.hideProgress();
        responseSingIn = response;
        view.home();
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void retry() {
        view.hideProgress();
        view.retryPopup();
    }

    @Override
    public void error() {
        view.hideProgress();
        view.error();
    }

    @Override
    public void errorConnection() {
        view.hideProgress();
        view.errorConnection();
    }

    @Override
    public String getToken() {

        if (responseSingIn != null) {
            return responseSingIn.body().getAccessToken();
        } else return null;
    }

    @Override
    public void showMessage(String message) {

    }

    private boolean checkField() {

        return checkEmail() && checkPassword() && checkRePassword() && checkMatchPasswords();
    }

//    private boolean checkUsername() {
//
//        if (TextUtils.isEmpty(view.getUsername())) {
//            view.showError(App.getInstance().getString(R.string.field_user_empty));
//            return false;
//        } else return true;
//    }
//
//    private boolean checkFName() {
//
//        if (TextUtils.isEmpty(view.getFName())) {
//            view.showError(App.getInstance().getString(R.string.field_fName_empty));
//            return false;
//        } else return true;
//    }
//
//    private boolean checkLName() {
//
//        if (TextUtils.isEmpty(view.getLName())) {
//            view.showError(App.getInstance().getString(R.string.field_lName_empty));
//            return false;
//        } else return true;
//    }

    private boolean checkEmail() {

        if (TextUtils.isEmpty(view.getEmail())) {
            view.showError(App.getInstance().getString(R.string.field_mail_empty));
            return false;
        } else return true;
    }

//    private boolean checkId() {
//
//        if (TextUtils.isEmpty(view.getId())) {
//            view.showError(App.getInstance().getString(R.string.field_id_empty));
//            return false;
//        } else return true;
//    }
//
//    private boolean checkPhone() {
//
//        if (TextUtils.isEmpty(view.getPhone())) {
//            view.showError(App.getInstance().getString(R.string.field_phone_empty));
//            return false;
//        } else return true;
//    }

    private boolean checkPassword() {

        if (TextUtils.isEmpty(view.getPassword())) {
            view.showError(App.getInstance().getString(R.string.field_pass_empty));
            return false;
        } else return true;
    }

    private boolean checkRePassword() {

        if (TextUtils.isEmpty(view.getRePassword())) {
            view.showError(App.getInstance().getString(R.string.field_rePass_empty));
            return false;
        } else return true;
    }

    private boolean checkMatchPasswords() {

        if (view.getPassword().equals(view.getRePassword())) {
            return true;
        } else {
            view.showError(App.getInstance().getString(R.string.notMatchPass));
            return false;
        }
    }

//    private boolean validate_email(String email){
//
//        String emailPattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
//                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
//
//        Pattern pattern = Pattern.compile(emailPattern);
//        Matcher matcher = pattern.matcher(email);
//
//        return matcher.matches();
//    }
//
//    private boolean validate_password(String pass) {
//
//        String passPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])\\w{8,15}$";
//
//        Pattern pattern = Pattern.compile(passPattern);
//        Matcher matcher = pattern.matcher(pass);
//
//        return matcher.matches();
//    }
}