package com.rsegrp.nossoticket.ui.express.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.express.di.receiptTicket.TicketReceiptModule;
import com.rsegrp.nossoticket.ui.express.mvp.receiptTicket.TicketReceiptContract;
import com.rsegrp.nossoticket.ui.express.mvp.receiptTicket.TicketReceiptPresenter;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class TicketReceiptFragment extends BaseFragment implements TicketReceiptContract.View {

    @Inject
    public TicketReceiptPresenter presenter;

    @BindView(R.id.progress_qr)
    RelativeLayout progress_qr;

    private Bitmap qr;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new TicketReceiptModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wait_time_express, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        hideProgress();
        ((HomeActivity) getActivity()).hideIcon();
        presenter.btnReceipt();
    }

    @Override
    public void nextFragment() {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        qr.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();

        Bundle args = new Bundle();
        args.putByteArray(Constant.QR_CODE, bytes);

        Fragment fragment = ShowTicketFragment.newInstance(bytes);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        progress_qr.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress_qr.setVisibility(View.GONE);
    }

    @Override
    public void setQr(Bitmap qr) {
        this.qr = qr;
    }
}
