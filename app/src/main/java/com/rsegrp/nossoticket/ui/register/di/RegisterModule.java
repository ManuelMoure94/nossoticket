package com.rsegrp.nossoticket.ui.register.di;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.SingUpServices;
import com.rsegrp.nossoticket.ui.register.mvp.RegisterActivity;
import com.rsegrp.nossoticket.ui.register.mvp.RegisterInteract;
import com.rsegrp.nossoticket.ui.register.mvp.RegisterPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@Module
public class RegisterModule {

    private RegisterActivity activity;

    public RegisterModule(RegisterActivity activity) {
        this.activity = activity;
    }

    @Provides
    @RegisterScope
    public RegisterActivity providesRegisterActivity() {
        return activity;
    }

    @Provides
    @RegisterScope
    public RegisterPresenter providesRegisterPresenter(RegisterInteract interact) {
        return new RegisterPresenter(activity, interact);
    }

    @Provides
    @RegisterScope
    public RegisterInteract providesRegister(SingUpServices singUpServices, SingInServices singInServices) {
        return new RegisterInteract(singUpServices, singInServices);
    }
}
