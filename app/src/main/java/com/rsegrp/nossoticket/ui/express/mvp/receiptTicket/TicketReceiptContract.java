package com.rsegrp.nossoticket.ui.express.mvp.receiptTicket;

import android.graphics.Bitmap;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public interface TicketReceiptContract {

    interface View {

        void nextFragment();

        void showProgress();

        void hideProgress();

        void setQr(Bitmap qr);
    }

    interface Presenter {

        void btnReceipt();
    }

    interface Interact {

    }
}
