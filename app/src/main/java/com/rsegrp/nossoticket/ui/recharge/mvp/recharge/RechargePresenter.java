package com.rsegrp.nossoticket.ui.recharge.mvp.recharge;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class RechargePresenter implements RechargeContract.Presenter {

    private RechargeContract.View view;
    private RechargeContract.Interact interact;

    public RechargePresenter(RechargeContract.View view, RechargeContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnMethodArrow() {
        view.showMethodContent();
    }

    @Override
    public void btnDataArrow() {
        view.showDataContent();
    }

    @Override
    public void btnAmountArrow() {
        view.showAmountContent();
    }

    @Override
    public void btnMethodArrowHide() {
        view.hideMethodContent();
    }

    @Override
    public void btnDataArrowHide() {
        view.hideDataContent();
    }

    @Override
    public void btnAmountArrowHide() {
        view.hideAmountContent();
    }

    @Override
    public void isTransference() {
        view.showOriginBank();
    }

    @Override
    public void isDeposit() {
        view.hideOriginBank();
    }

    @Override
    public void btnRechargeTransference() {

        if (checkFieldTransference()){
            view.nextFragment();
        }
    }

    @Override
    public void btnRechargeDeposit() {

        if (checkFieldDeposit()){
            view.nextFragment();
        }
    }

    private boolean checkFieldDeposit(){

        return checkedBankTarget() && checkedReceipt() && checkecDate()
                && checkedAmount();
    }

    private boolean checkFieldTransference() {

        return checkedBankTarget() && checkedBankOrigin() && checkedReceipt()
                && checkecDate() && checkedAmount();
    }

    private boolean checkedBankTarget(){

        if (TextUtils.isEmpty(view.getBankTarget())){
            view.showError(App.getInstance().getBaseContext().getString(R.string.target_bank_empty));
            return false;
        } else return true;
    }

    private boolean checkedBankOrigin(){

        if (TextUtils.isEmpty(view.getBankOrigin())){
            view.showError(App.getInstance().getBaseContext().getString(R.string.origin_bank_empty));
            return false;
        } else return true;
    }

    private boolean checkecDate(){

        if (TextUtils.isEmpty(view.getDate())){
            view.showError(App.getInstance().getBaseContext().getString(R.string.date_empty));
            return false;
        } else return true;
    }

    private boolean checkedReceipt(){

        if (TextUtils.isEmpty(view.getReceipt())){
            view.showError(App.getInstance().getBaseContext().getString(R.string.receipt_empty));
            return false;
        } else return true;
    }

    private boolean checkedAmount(){

        if (TextUtils.isEmpty(view.getAmount())){
            view.showError(App.getInstance().getBaseContext().getString(R.string.amount_empty));
            return false;
        } else return true;
    }
}
