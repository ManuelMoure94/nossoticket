package com.rsegrp.nossoticket.ui.summary.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.recharge.fragment.RechargeFragment;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.summary.adapter.SummaryAdapter;
import com.rsegrp.nossoticket.ui.summary.di.SummaryModule;
import com.rsegrp.nossoticket.ui.summary.model.SummaryModel;
import com.rsegrp.nossoticket.ui.summary.mvp.SummaryContract;
import com.rsegrp.nossoticket.ui.summary.mvp.SummaryPresenter;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

public class SummaryFragment extends BaseFragment implements SummaryContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    private double remaining;

    @Inject
    public SummaryPresenter presenter;

    @BindView(R.id.recycler_summary)
    RecyclerView recyclerSummary;

    @BindView(R.id.btn_recharge)
    Button btnRecharge;

    @BindView(R.id.progress_summary)
    RelativeLayout progressSummary;

    @BindView(R.id.swipe_summary)
    SwipeRefreshLayout swipeSummary;

    @BindView(R.id.summary_remaining)
    TextView summaryRemaining;

    private RecyclerView.Adapter adapter;

    private List<SummaryModel> items = new ArrayList<>();

    public static SummaryFragment newInstance(double remaining) {

        SummaryFragment summaryFragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putDouble(Constant.ARGS_REMAINING, remaining);
        summaryFragment.setArguments(args);

        return summaryFragment;
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new SummaryModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            remaining = getArguments().getDouble(Constant.ARGS_REMAINING);
        }

        return view;
    }

    @Override
    public void initRecycler() {

        recyclerSummary.setHasFixedSize(true);

        recyclerSummary.setLayoutManager(new LinearLayoutManager(getActivity()));

        items = presenter.getItems();

        adapter = new SummaryAdapter(items, getActivity());
        recyclerSummary.setAdapter(adapter);

        swipeSummary.setRefreshing(false);
    }

    @Override
    public void showProgress() {
        progressSummary.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressSummary.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeActivity) getActivity()).showToolbar();
        presenter.getSummary(true);
        summaryRemaining.setText(String.format("%s Bs", remaining));
        checkedBtn();
    }

    private void checkedBtn() {

        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnRecharge();
            }
        });

        swipeSummary.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (items.size() != 0) {
                    items.clear();
                    adapter.notifyDataSetChanged();
                }
                presenter.getSummary(false);
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void setRemaining(double remaining) {
        summaryRemaining.setText(String.format("%s Bs", remaining));
    }

    @Override
    public void rechargeFragment() {

        Fragment fragment = new RechargeFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void hideProgressUpdate() {
        swipeSummary.setRefreshing(false);
    }

    @Override
    public void retry() {
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
        hideProgress();
    }

    @Override
    public void error() {
        new DialogsOneBtn(this, getString(R.string.fail_summary), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
        hideProgress();
    }

    @Override
    public void clickListener(String options) {
        switch (options) {
            case "cancel":
                swipeSummary.setRefreshing(false);
                break;
            case "retry":
                swipeSummary.setRefreshing(false);
                presenter.getSummary(true);
                break;
            case "accept":
                break;
        }
    }
}
