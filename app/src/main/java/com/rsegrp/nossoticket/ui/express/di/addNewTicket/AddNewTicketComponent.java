package com.rsegrp.nossoticket.ui.express.di.addNewTicket;

import com.rsegrp.nossoticket.ui.express.fragments.AddNewTicketFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@AddNewTicketScope
@Subcomponent(modules = {AddNewTicketModule.class})
public interface AddNewTicketComponent {
    void inject(AddNewTicketFragment target);
}
