package com.rsegrp.nossoticket.ui.user.mvp.update;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface UpdateContract {

    interface View extends BaseContract.View {

        String getFName();

        String getLName();

        String getAddress();

        String getDate();

        String getPhone();

        String getIdUser();

        DataUser getDataUser();

        void setFName(String fName);

        void setLName(String lName);

        void setAddress(String address);

        void setPhone(String phone);

        void setDate(String date);

        void setId(String id);

        void nextFragment();

        void showProgress();

        void hideProgress();

        void onSuccess();

        void onError();

        void onFailed();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnUpdate();

        void onSuccess(Response<DataUser> response);

        void onError();

        void onFailed();
    }

    interface Interact {

        void updateUser(DataUser dataUser, UpdatePresenter presenter);
    }
}
