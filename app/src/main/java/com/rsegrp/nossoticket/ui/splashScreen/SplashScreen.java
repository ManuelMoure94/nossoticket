package com.rsegrp.nossoticket.ui.splashScreen;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;


/**
 * Created by RSE_VZLA_07 on 26/2/2018.
 */

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final Intent intent = new Intent(this, LoginHomeActivity.class);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(intent);
                overridePendingTransition(R.anim.fab_scale_up, R.anim.fab_scale_down);
                finish();
            }
        }, 2000);
    }
}