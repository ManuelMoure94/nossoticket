package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails;

import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.ui.settingsCard.model.DetailsModel;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardDetailsPresenter implements CardDetailsContract.Presenter {

    private CardDetailsContract.View view;
    private CardDetailsContract.Interact interact;

    private Response<Movement> response;

    private List<DetailsModel> items = new ArrayList<>();

    public CardDetailsPresenter(CardDetailsContract.View view, CardDetailsContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void setText() {
        view.setSerial();
        view.setBalance();
        view.setDate();
        view.setColorIcon();
    }

    @Override
    public void btnCardRecharge() {
        view.recharge();
    }

    @Override
    public void btnCardBlock() {
        view.block();
    }

    @Override
    public void getDetails(String cardId) {

        interact.getDetails(cardId, this);
    }

    @Override
    public void nextPage(String cardId) {

        if (response.body().getNextPageIndex() != -1) {
            view.showProgress();
            interact.getMovement(cardId, String.valueOf(response.body().getNextPageIndex()), this);
        } else view.updateList(getLastItem());
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }

    @Override
    public void onSuccess(Response<Movement> response, boolean firstTime) {

        this.response = response;
        if (firstTime) {
            view.initRecycler();
        }
        hideProgress();
        view.updateList(getItems(firstTime));
    }

    @Override
    public void hideProgress() {
        view.hideProgress();
        view.hideProgressUpdate();
    }

    @Override
    public void retry() {
        view.retry();
    }

    @Override
    public void error() {
        view.error();
    }

    private List<DetailsModel> getItems(boolean firstTime) {

        if (response != null) {

            if (firstTime) {
                items.clear();
            }

            for (int i = 0; i < response.body().getData().size(); i++) {
                items.add(new DetailsModel(response.body().getData().get(i).getLine(),
                        response.body().getData().get(i).getStation(),
                        response.body().getData().get(i).getMoment().substring(8, 10) + "/" +
                                response.body().getData().get(i).getMoment().substring(5, 7) + "/" +
                                response.body().getData().get(i).getMoment().substring(0, 4),
                        response.body().getData().get(i).getAction(),
                        response.body().getData().get(i).getFacial(),
                        response.body().getData().get(i).getRemaining(), Constant.ITEM_TYPE_DATA));
            }
            return items;
        } else return null;
    }

    private List<DetailsModel> getLastItem() {

        items.add(new DetailsModel("", "", "", "", "",
                "", Constant.ITEM_TYPE_END));

        return items;
    }

    private List<DetailsModel> progressItems(int type) {

        if (type == 0) {
            items.add(new DetailsModel("", "", "", "", "",
                    "", Constant.ITEM_TYPE_PROGRESS));
        } else items.remove(items.size() - 1);

        return items;
    }
}
