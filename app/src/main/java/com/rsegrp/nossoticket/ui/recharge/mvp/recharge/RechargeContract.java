package com.rsegrp.nossoticket.ui.recharge.mvp.recharge;

import com.rsegrp.nossoticket.ui.base.BaseContract;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public interface RechargeContract {

    interface View extends BaseContract.View{

        String getBankTarget();
        String getBankOrigin();
        String getReceipt();
        String getDate();
        String getAmount();

        void showMethodContent();
        void showDataContent();
        void showAmountContent();
        void showOriginBank();

        void hideMethodContent();
        void hideDataContent();
        void hideAmountContent();
        void hideOriginBank();

        void nextFragment();
    }

    interface Presenter{

        void btnMethodArrow();
        void btnDataArrow();
        void btnAmountArrow();
        void btnMethodArrowHide();
        void btnDataArrowHide();
        void btnAmountArrowHide();
        void isTransference();
        void isDeposit();
        void btnRechargeTransference();
        void btnRechargeDeposit();
    }

    interface Interact{

    }
}
