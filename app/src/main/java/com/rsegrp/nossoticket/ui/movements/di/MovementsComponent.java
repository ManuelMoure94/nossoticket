package com.rsegrp.nossoticket.ui.movements.di;

import com.rsegrp.nossoticket.ui.movements.fragment.MovementsFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

@MovementsScope
@Subcomponent(modules = {MovementsModule.class})
public interface MovementsComponent {
    void inject(MovementsFragment target);
}
