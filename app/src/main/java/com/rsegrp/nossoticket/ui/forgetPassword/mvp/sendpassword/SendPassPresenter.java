package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.forgot.ChangePassword;
import com.rsegrp.nossoticket.api.data.forgot.CheckToken;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

public class SendPassPresenter implements SendPassContract.Presenter {

    private SendPassContract.View view;
    private SendPassContract.Interact interact;

    private CheckToken checkToken;

    public SendPassPresenter(SendPassContract.View view, SendPassContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnNext() {

        if (checkField()) {
            interact.changePassword(new ChangePassword(checkToken.getEmail(), checkToken.getId(),
                    view.getPassword(), view.get2ndPassword()), this);
        }
    }

    @Override
    public void checkToken(CheckToken checkToken) {

        this.checkToken = checkToken;

        view.showProgress();
        interact.checkToken(checkToken, this);
    }

    @Override
    public void changePassword() {
        interact.changePassword(new ChangePassword(checkToken.getEmail(), checkToken.getId(),
                view.getPassword(), view.get2ndPassword()), this);
    }

    @Override
    public void onSuccess(String type) {

        view.hideProgress();
        if (type.equals(Constant.TYPE_CHANGE_PASS)) {
            view.onSuccess(type, true);
        }
    }

    @Override
    public void onError(String type) {

        view.hideProgress();
        view.onError(type);
    }

    @Override
    public void onFailed(String type) {

        view.hideProgress();
        view.onFailed(type);
    }

    private boolean checkField() {

        return checkPassword() && check2ndPassword() && checkMatchPass();
    }

    private boolean checkPassword() {

        if (TextUtils.isEmpty(view.getPassword())) {
            view.showError(App.getInstance().getString(R.string.field_pass_empty));
            return false;
        } else return true;
    }

    private boolean check2ndPassword() {

        if (TextUtils.isEmpty(view.get2ndPassword())) {
            view.showError(App.getInstance().getString(R.string.field_rePass_empty));
            return false;
        } else return true;
    }

    private boolean checkMatchPass() {

        if (view.getPassword().equals(view.get2ndPassword())) {
            return true;
        } else {
            view.showError(App.getInstance().getString(R.string.notMatchPass));
            return false;
        }
    }
}
