package com.rsegrp.nossoticket.ui.recharge.di.receipt;

import com.rsegrp.nossoticket.api.services.WalletServices;
import com.rsegrp.nossoticket.ui.recharge.fragment.ReceiptFragment;
import com.rsegrp.nossoticket.ui.recharge.mvp.receipt.ReceiptInteract;
import com.rsegrp.nossoticket.ui.recharge.mvp.receipt.ReceiptPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Module
public class ReceiptModule {

    private ReceiptFragment receiptFragment;

    public ReceiptModule(ReceiptFragment receiptFragment) {
        this.receiptFragment = receiptFragment;
    }

    @Provides
    @ReceiptScope
    public ReceiptFragment providesReceiptFragment(){
        return receiptFragment;
    }

    @Provides
    @ReceiptScope
    public ReceiptPresenter providesReceiptPresenter(ReceiptInteract interact){
        return new ReceiptPresenter(receiptFragment, interact);
    }

    @Provides
    @ReceiptScope
    public ReceiptInteract providesReceiptInteract(WalletServices walletServices){
        return new ReceiptInteract(walletServices);
    }
}