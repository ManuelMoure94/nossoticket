package com.rsegrp.nossoticket.ui.title.model;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class TitleModel {

    private String cardType;
    private String cardId;
    private String cardSerial;
    private String cardAmount;
    private String cardDate;

    public TitleModel(String cardType, String cardId, String cardSerial, String cardAmount, String cardDate) {
        this.cardType = cardType;

        this.cardId = cardId;
        this.cardSerial = cardSerial;
        this.cardAmount = cardAmount;
        this.cardDate = cardDate;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardSerial() {
        return cardSerial;
    }

    public void setCardSerial(String cardSerial) {
        this.cardSerial = cardSerial;
    }

    public String getCardAmount() {
        return cardAmount;
    }

    public void setCardAmount(String cardAmount) {
        this.cardAmount = cardAmount;
    }

    public String getCardDate() {
        return cardDate;
    }

    public void setCardDate(String cardDate) {
        this.cardDate = cardDate;
    }
}