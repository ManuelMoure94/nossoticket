package com.rsegrp.nossoticket.ui.user.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.ui.user.di.update.UpdateModule;
import com.rsegrp.nossoticket.ui.user.mvp.update.UpdateContract;
import com.rsegrp.nossoticket.ui.user.mvp.update.UpdatePresenter;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class UpdateFragment extends BaseFragment implements UpdateContract.View, DialogsOneBtn.OnSetClickListener,
        DialogsTwoBtn.OnSetClickListener {

    @Inject
    UpdatePresenter presenter;

    @BindView(R.id.text_fName)
    MaterialEditText textFName;
    @BindView(R.id.text_lName)
    MaterialEditText textLName;
    @BindView(R.id.text_address)
    MaterialEditText textAddress;
    @BindView(R.id.text_date)
    MaterialEditText textDate;
    @BindView(R.id.text_phone)
    MaterialEditText textPhone;
    @BindView(R.id.text_id)
    MaterialEditText textId;

    @BindView(R.id.btn_update)
    Button btnUpdate;

    private String type;

    @BindView(R.id.progress_update)
    RelativeLayout progress;

    private DataUser dataUser;

    public static UpdateFragment newInstance(DataUser dataUser) {

        UpdateFragment updateFragment = new UpdateFragment();
        Bundle args = new Bundle();

        args.putSerializable(Constant.ARGS_DATA_USER, dataUser);
        updateFragment.setArguments(args);

        return updateFragment;
    }

    public UpdateFragment() {
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new UpdateModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_user, container, false);
        ButterKnife.bind(this, view);

        dataUser = (DataUser) getArguments().getSerializable(Constant.ARGS_DATA_USER);
        setData();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).showToolbar();

        checkedBtn();
    }

    private void setData() {

        if (!TextUtils.isEmpty(dataUser.getName())) {
            setFName(dataUser.getName());
        }

        if (!TextUtils.isEmpty(dataUser.getLastName())) {
            setLName(dataUser.getLastName());
        }

        if (!TextUtils.isEmpty(dataUser.getBirthDate())) {
            setDate(dataUser.getBirthDate());
        }

        if (!TextUtils.isEmpty(dataUser.getIdentification())) {
            setId(dataUser.getIdentification());
        }

        if (!TextUtils.isEmpty(dataUser.getPhone())) {
            setPhone(dataUser.getPhone());
        }

        if (!TextUtils.isEmpty(dataUser.getAddress())) {
            setAddress(dataUser.getAddress());
        }
    }

    private void checkedBtn() {

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnUpdate();
            }
        });

        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
    }

    private void showDatePickerDialog() {

        DatePickerFragmentUpdate newFragment = DatePickerFragmentUpdate.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                final String selectedDate = dayOfMonth + " / " + (month + 1) + " / " + year;
                textDate.setText(selectedDate);
            }
        });
        newFragment.show(getActivity().getFragmentManager(), "DATEPICKERUPDATE");
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public String getFName() {
        return textFName.getText().toString();
    }

    @Override
    public String getLName() {
        return textLName.getText().toString();
    }

    @Override
    public String getAddress() {
        return textAddress.getText().toString();
    }

    @Override
    public String getDate() {
        return textDate.getText().toString();
    }

    @Override
    public String getPhone() {
        return textPhone.getText().toString();
    }

    @Override
    public String getIdUser() {
        return textId.getText().toString();
    }

    @Override
    public DataUser getDataUser() {
        return dataUser;
    }

    @Override
    public void setFName(String fName) {
        textFName.setText(fName);
    }

    @Override
    public void setLName(String lName) {
        textLName.setText(lName);
    }

    @Override
    public void setAddress(String address) {
        textAddress.setText(address);
    }

    @Override
    public void setPhone(String phone) {
        textPhone.setText(phone);
    }

    @Override
    public void setDate(String date) {
        textDate.setText(date);
    }

    @Override
    public void setId(String id) {
        textId.setText(id);
    }

    @Override
    public void nextFragment() {

        Fragment fragment = new SummaryFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
        btnUpdate.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess() {
        type = Constant.SUCCESS;
        new DialogsOneBtn(this, getString(R.string.update_user), getString(R.string.accept)).show(getChildFragmentManager(), "OnSuccess");
    }

    @Override
    public void onError() {
        type = Constant.ERROR;
        new DialogsOneBtn(this, getString(R.string.error_check_token), getString(R.string.accept)).show(getChildFragmentManager(), "OnSuccess");
    }

    @Override
    public void onFailed() {
        type = Constant.FAILED;
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void clickListener(String options) {

        switch (type) {

            case Constant.SUCCESS:
                nextFragment();
                break;

            case Constant.ERROR:
                break;

            case Constant.FAILED:

                if (options.equals("retry")) {
                    presenter.btnUpdate();
                }
                break;
        }
    }

    public static class DatePickerFragmentUpdate extends DialogFragment {

        private DatePickerDialog.OnDateSetListener listener;

        public static DatePickerFragmentUpdate newInstance(DatePickerDialog.OnDateSetListener listener) {
            DatePickerFragmentUpdate fragmentUpdate = new DatePickerFragmentUpdate();
            fragmentUpdate.listener = listener;
            return fragmentUpdate;
        }

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar calendar = Calendar.getInstance();

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), listener, year, month, day);
        }
    }
}
