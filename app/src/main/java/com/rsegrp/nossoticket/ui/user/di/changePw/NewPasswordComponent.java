package com.rsegrp.nossoticket.ui.user.di.changePw;

import com.rsegrp.nossoticket.ui.user.fragment.NewPasswordFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@NewPasswordScope
@Subcomponent(modules = {NewPasswordModule.class})
public interface NewPasswordComponent {
    void inject(NewPasswordFragment target);
}
