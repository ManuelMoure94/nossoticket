package com.rsegrp.nossoticket.ui.title.mvp;

import com.rsegrp.nossoticket.api.data.titles.Titles;

import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class TitlePresenter implements TitleContract.Presenter {

    private TitleContract.View view;
    private TitleContract.Interact interact;

    private Response<List<Titles>> response;


    public TitlePresenter(TitleContract.View view, TitleContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnAdd() {
        view.addCard();
    }

    @Override
    public void getTitles(boolean state) {

        if (state) {
            view.showProgress();
        }
        interact.getTitles(this);
    }

    @Override
    public int getSize() {

        if (response != null) {
            return response.body().size();
        } else return 0;
    }

    @Override
    public String getCardId(int position) {

        if (response != null) {
            return response.body().get(position).getId();
        } else return null;
    }

    @Override
    public String getCardSerial(int position) {

        if (response != null) {
            return response.body().get(position).getMaCardId();
        } else return null;
    }

    @Override
    public int getCardAmount(int position) {

        if (response != null) {
            return response.body().get(position).getRemaining();
        } else return 0;
    }

    @Override
    public String getCardType(int position) {

        if (response != null) {
            return response.body().get(position).getTittleType();
        } else return null;
    }

    @Override
    public String getCardDate(int position) {

        if (response != null) {
//            String year = response.body().get(position).getCreatedAt().substring(0, 4);
//            String month = response.body().get(position).getCreatedAt().substring(5, 7);
//            String day = response.body().get(position).getCreatedAt().substring(8, 10);
//            return day + "/" + month + "/" + year;
            return response.body().get(position).getDescription();
        } else return null;
    }

    @Override
    public boolean getHaveTitles() {
        return response.body().size() == 0;
    }

    @Override
    public void onSuccess(Response<List<Titles>> response) {
        view.hideProgress();
        this.response = response;
        if (response.body() == null) {

        } else view.initRecycler();
    }


    @Override
    public void hideProgress() {
        view.hideProgress();
        view.hideProgressUpdate();
    }

    @Override
    public void retry() {
        view.retry();
    }

    @Override
    public void error() {
        view.error();
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }
}