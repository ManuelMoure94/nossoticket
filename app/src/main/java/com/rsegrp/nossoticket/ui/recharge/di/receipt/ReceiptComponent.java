package com.rsegrp.nossoticket.ui.recharge.di.receipt;

import com.rsegrp.nossoticket.ui.recharge.fragment.ReceiptFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@ReceiptScope
@Subcomponent(modules = {ReceiptModule.class})
public interface ReceiptComponent {
    void inject(ReceiptFragment target);
}
