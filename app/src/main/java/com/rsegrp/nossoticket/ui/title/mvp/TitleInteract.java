package com.rsegrp.nossoticket.ui.title.mvp;

import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class TitleInteract implements TitleContract.Interact {

    private TitleServices titleServices;

    public TitleInteract(TitleServices titleServices) {
        this.titleServices = titleServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getTitles(final TitlePresenter presenter) {

        Call<List<Titles>> call = titleServices.getTitles(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<List<Titles>>() {
            @Override
            public void onResponse(Call<List<Titles>> call, Response<List<Titles>> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<List<Titles>> call, Throwable t) {

                presenter.retry();
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
//                presenter.hideProgress();
            }
        });
    }
}
