package com.rsegrp.nossoticket.ui.express.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.express.di.showTicket.ShowTicketModule;
import com.rsegrp.nossoticket.ui.express.mvp.showTicket.ShowTicketContract;
import com.rsegrp.nossoticket.ui.express.mvp.showTicket.ShowTicketPresenter;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class ShowTicketFragment extends BaseFragment implements ShowTicketContract.View {

    @Inject
    public ShowTicketPresenter presenter;

    @BindView(R.id.progress_show_qr)
    RelativeLayout progress;

    @BindView(R.id.imageQR)
    ImageView imageQr;

    private byte[] bytes;

    private Bitmap qr;

    public static ShowTicketFragment newInstance(byte[] bytes) {

        ShowTicketFragment showTicketFragment = new ShowTicketFragment();

        Bundle args = new Bundle();
        args.putByteArray(Constant.QR_CODE, bytes);
        showTicketFragment.setArguments(args);

        return showTicketFragment;
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new ShowTicketModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ticket_express, container, false);
        ButterKnife.bind(this, view);

        bytes = getArguments().getByteArray(Constant.QR_CODE);

        ((HomeActivity) getActivity()).showIcon();
        ((HomeActivity) getActivity()).setImageToolbar(R.drawable.ic_arrow_back_black_24px, 1);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        qr = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        setQr(qr);
//        presenter.generateQR();
    }

    @Override
    public void setQr(Bitmap qr) {
        imageQr.setImageBitmap(qr);
    }

    @Override
    public void showQr() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideQr() {
        progress.setVisibility(View.GONE);
    }
}
