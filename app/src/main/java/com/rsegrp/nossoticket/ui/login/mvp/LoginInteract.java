package com.rsegrp.nossoticket.ui.login.mvp;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class LoginInteract implements LoginContract.Interact {

    private static final String TAG = "tag";

    private SingInServices singInServices;

    public LoginInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void checkUser(User user, final LoginContract.Presenter presenter) {

        Call<SingIn> call = singInServices.singIn(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, user);

        call.enqueue(new Callback<SingIn>() {
            @Override
            public void onResponse(Call<SingIn> call, Response<SingIn> response) {

                if (response.isSuccessful()) {
                    presenter.isSuccess(response);
//                    Date date = new Date(System.currentTimeMillis() + response.body().getAccessExpiresAt());
                    Log.i(TAG, "onResponse: " + response.code());
                } else if (response.code() >= 400){
                    presenter.failLogin();
                }
            }

            @Override
            public void onFailure(Call<SingIn> call, Throwable t) {

                presenter.retryConnection();
            }
        });
    }
}
