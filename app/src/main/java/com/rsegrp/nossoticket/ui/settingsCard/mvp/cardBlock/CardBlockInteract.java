package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock;

import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.root.App;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardBlockInteract implements CardBlockContract.Interact {

    private TitleServices titleServices;

    public CardBlockInteract(TitleServices titleServices) {
        this.titleServices = titleServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void deletedCard(String cardId, final CardBlockPresenter presenter) {

//        Call call = titleServices.deleteTitle(Constant.CONTENT_TYPE, Constant.TOKEN +
//                presenter.getToken(), cardId);
//
//        call.enqueue(new Callback() {
//            @Override
//            public void onResponse(Call call, Response response) {
//
//                if (response.isSuccessful()) {
//                    presenter.onSuccess();
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
//                presenter.hideProgress();
//            }
//        });
    }
}
