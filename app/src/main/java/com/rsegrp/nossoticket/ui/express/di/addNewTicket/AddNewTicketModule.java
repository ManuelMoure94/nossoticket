package com.rsegrp.nossoticket.ui.express.di.addNewTicket;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.express.fragments.AddNewTicketFragment;
import com.rsegrp.nossoticket.ui.express.mvp.addNewTicket.AddNewTicketInteract;
import com.rsegrp.nossoticket.ui.express.mvp.addNewTicket.AddNewTicketPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@Module
public class AddNewTicketModule {

    private AddNewTicketFragment addNewTicketFragment;

    public AddNewTicketModule(AddNewTicketFragment addNewTicketFragment) {
        this.addNewTicketFragment = addNewTicketFragment;
    }

    @Provides
    @AddNewTicketScope
    public AddNewTicketFragment providesAddNewTicketFragment() {
        return addNewTicketFragment;
    }

    @Provides
    @AddNewTicketScope
    public AddNewTicketPresenter providesAddNewTicketPresenter(AddNewTicketInteract interact) {
        return new AddNewTicketPresenter(addNewTicketFragment, interact);
    }

    @Provides
    @AddNewTicketScope
    public AddNewTicketInteract providesAddNewTicketInteract(SingInServices singInServices) {
        return new AddNewTicketInteract(singInServices);
    }
}
