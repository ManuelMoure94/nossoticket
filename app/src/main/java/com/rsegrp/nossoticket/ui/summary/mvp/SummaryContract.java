package com.rsegrp.nossoticket.ui.summary.mvp;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.summary.Summary;
import com.rsegrp.nossoticket.ui.base.BaseContract;
import com.rsegrp.nossoticket.ui.summary.model.SummaryModel;

import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

public interface SummaryContract {

    interface View extends BaseContract.View {

        void setRemaining(double remaining);

        void rechargeFragment();

        void initRecycler();

        void showProgress();

        void hideProgress();

        void hideProgressUpdate();

        void retry();

        void error();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnRecharge();

        void isSuccess(Response<List<Summary>> response);

        void isSuccessUser(Response<DataUser> response);

        void getSummary(boolean state);

        void hideProgress();

        void retry();

        void error();

        List<SummaryModel> getItems();
    }

    interface Interact {

        void getSummary(SummaryPresenter presenter);

        void getUser(SummaryPresenter presenter);
    }
}
