package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword;

import com.rsegrp.nossoticket.api.data.forgot.ChangePassword;
import com.rsegrp.nossoticket.api.data.forgot.CheckToken;
import com.rsegrp.nossoticket.api.services.ForgotPasswordServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendPassInteract implements SendPassContract.Interact {

    private ForgotPasswordServices forgotPasswordServices;

    public SendPassInteract(ForgotPasswordServices forgotPasswordServices) {
        this.forgotPasswordServices = forgotPasswordServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void checkToken(CheckToken checkToken, final SendPassPresenter presenter) {

        Call<String> call = forgotPasswordServices.ForgotPwCheckingToken(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, checkToken);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(Constant.TYPE_CHECK_TOKEN);
                } else {
                    presenter.onError(Constant.TYPE_CHECK_TOKEN);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                presenter.onFailed(Constant.TYPE_CHECK_TOKEN);
            }
        });
    }

    @Override
    public void changePassword(ChangePassword changePassword, final SendPassPresenter presenter) {

        Call<String> call = forgotPasswordServices.ForgotPwChangePassword(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, changePassword);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(Constant.TYPE_CHANGE_PASS);
                } else {
                    presenter.onError(Constant.TYPE_CHANGE_PASS);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                presenter.onFailed(Constant.TYPE_CHANGE_PASS);
            }
        });
    }
}
