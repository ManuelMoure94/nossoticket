package com.rsegrp.nossoticket.ui.express.mvp.ticketList;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class TicketListPresenter implements TicketListContract.Presenter {

    private TicketListContract.View view;
    private TicketListContract.Interact interact;

    public TicketListPresenter(TicketListContract.View view, TicketListContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }
}