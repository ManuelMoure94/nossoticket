package com.rsegrp.nossoticket.ui.movements.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.CardView;
import android.view.ViewGroup;

import com.rsegrp.nossoticket.ui.movements.adapter.interfaces.CardAdapter;
import com.rsegrp.nossoticket.ui.movements.fragment.CardFragment;
import com.rsegrp.nossoticket.ui.movements.fragment.CardIfeFragment;
import com.rsegrp.nossoticket.ui.movements.model.PagerFragmentModel;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public class MovementsPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {

    private List<PagerFragmentModel> data = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();

    private float baseElevation;

    public MovementsPagerAdapter(FragmentManager fm, float baseElevation, List<PagerFragmentModel> data) {
        super(fm);

        for (int i = 0; i < data.size(); i++) {
//            this.fragments.add(fragments.get(i).getCardFragment());
//            addCardFragment(new CardFragment());
            if (data.get(i).getCardType().equals(Constant.CDLP)) {
                addCardFragment(new CardIfeFragment());
            } else {
                addCardFragment(new CardFragment());
            }
        }
        this.data.addAll(data);
        this.baseElevation = baseElevation;
    }

    @Override
    public Fragment getItem(int position) {

        if (!data.get(position).getCardType().equals(Constant.CDLP)) {
            return CardFragment.getInstance(position, data.get(position).getCardId(), data.get(position).getSerial(), data.get(position).getBalance(), data.get(position).getDescription());
        } else
            return CardIfeFragment.getInstance(position, data.get(position).getCardId(), data.get(position).getSerial(), data.get(position).getBalance(), data.get(position).getDescription());
//        return CardFragment.getInstance(position);
    }

    @Override
    public float getBaseElevation() {
        return baseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {

        if (!data.get(position).getCardType().equals(Constant.CDLP)) {
            CardFragment fragment = (CardFragment) fragments.get(position);
            return fragment.getCardView();
        } else {
            CardIfeFragment fragment = (CardIfeFragment) fragments.get(position);
            return fragment.getCardView();
        }
//        return fragments.get(position).getCardView();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object fragment = super.instantiateItem(container, position);
//        fragments.set(position, (CardFragment) fragment);
        if (!data.get(position).getCardType().equals(Constant.CDLP)) {
            fragments.set(position, (CardFragment) fragment);
        } else fragments.set(position, (CardIfeFragment) fragment);
        return fragment;
    }

    public void addCardFragment(Fragment fragment) {
        fragments.add(fragment);
    }
}
