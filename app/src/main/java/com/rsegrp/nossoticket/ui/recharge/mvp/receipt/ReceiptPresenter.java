package com.rsegrp.nossoticket.ui.recharge.mvp.receipt;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.wallet.recharge.RechargeRequest;
import com.rsegrp.nossoticket.api.data.wallet.recharge.WalletRecharge;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ReceiptPresenter implements ReceiptContract.Presenter {

    private ReceiptContract.View view;
    private ReceiptContract.Interact interact;

    public ReceiptPresenter(ReceiptContract.View view, ReceiptContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnReceipt(RechargeRequest rechargeRequest) {

        if (view.getTextBtn().equals(App.getInstance().getString(R.string.confirm))) {
            interact.rechargeRequest(this, rechargeRequest);
            view.showProgress();
        } else if (view.getTextBtn().equals(App.getInstance().getString(R.string.accept))) {
            view.nextFragment(Constant.FRAGMENT_SUMMARY);
        }
    }

    @Override
    public void checkedMethod(String method) {
        if (method.equals("Transference")) {
            view.showOrigin();
            view.setOrigin();
        } else {
            view.hideOrigin();
        }
        view.setMethod();
        view.setTarget();
        view.setAmount();
        view.setDate();
        view.setReceipt();
    }

    @Override
    public void isSuccess(Response<WalletRecharge> response) {
        view.hideProgress();
        view.setMessage(App.getInstance().getString(R.string.successful_operation));
        view.setTextBtn(App.getInstance().getString(R.string.accept));
    }

    @Override
    public void rechargingError() {
        view.hideProgress();
        view.rechargingError();
    }

    @Override
    public void retryConnection() {
        view.hideProgress();
        view.retryConnection();
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }
}
