package com.rsegrp.nossoticket.ui.express.di.receiptTicket;

import com.rsegrp.nossoticket.ui.express.fragments.TicketReceiptFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@TicketReceiptScope
@Subcomponent(modules = {TicketReceiptModule.class})
public interface TicketReceiptComponent {
    void inject(TicketReceiptFragment target);
}
