package com.rsegrp.nossoticket.ui.register.di;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@Scope
public @interface RegisterScope {
}