package com.rsegrp.nossoticket.ui.login.di;

import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@LoginScope
@Subcomponent(modules = {LoginModule.class})
public interface LoginComponent {
    void inject(LoginFragment target);
}
