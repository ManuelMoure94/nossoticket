package com.rsegrp.nossoticket.ui.express.di.receiptTicket;

import com.rsegrp.nossoticket.ui.express.fragments.TicketReceiptFragment;
import com.rsegrp.nossoticket.ui.express.mvp.receiptTicket.TicketReceiptInteract;
import com.rsegrp.nossoticket.ui.express.mvp.receiptTicket.TicketReceiptPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@Module
public class TicketReceiptModule {

    private TicketReceiptFragment tickerReceiptFragment;

    public TicketReceiptModule(TicketReceiptFragment tickerReceiptFragment) {
        this.tickerReceiptFragment = tickerReceiptFragment;
    }

    @Provides
    @TicketReceiptScope
    public TicketReceiptFragment providesTicketReceiptFragment() {
        return tickerReceiptFragment;
    }

    @Provides
    @TicketReceiptScope
    public TicketReceiptPresenter providesTicketReceiptPresenter(TicketReceiptInteract interact) {
        return new TicketReceiptPresenter(tickerReceiptFragment, interact);
    }

    @Provides
    @TicketReceiptScope
    public TicketReceiptInteract providesTicketReceiptInteract() {
        return new TicketReceiptInteract();
    }
}