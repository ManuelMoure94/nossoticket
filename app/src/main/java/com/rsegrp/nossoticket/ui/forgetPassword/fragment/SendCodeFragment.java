package com.rsegrp.nossoticket.ui.forgetPassword.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendcode.SendCodeModule;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode.SendCodeContract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode.SendCodePresenter;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rengwuxian.materialedittext.MaterialEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendCodeFragment extends BaseFragment implements SendCodeContract.View {

    @Inject
    public SendCodePresenter presenter;

    @BindView(R.id.inputCode)
    MaterialEditText inputCode;

    @BindView(R.id.btn_next_2)
    Button btnNext;

    @BindView(R.id.text_mail_show)
    TextView textEmail;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new SendCodeModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_code, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        checkBtn();
    }

    private void checkBtn() {

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnNext();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(App.getInstance().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public String getCode() {
        return inputCode.getText().toString();
    }

    @Override
    public void setEmail(String email) {
        textEmail.setText(email);
    }

    @Override
    public void nextFragment() {

        Fragment fragment = new SendPassFragment();
        ((LoginHomeActivity) getActivity()).setFragment(fragment);
    }
}
