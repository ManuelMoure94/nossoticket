package com.rsegrp.nossoticket.ui.login.mvp;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private LoginContract.Interact interact;

    private Response<SingIn> response;

    private User user;

    public LoginPresenter(LoginContract.View view, LoginContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnLogin() {

        if (checkField()) {
            user = new User(view.getEmail(), view.getPassword());
            view.showProgress();
            interact.checkUser(user, this);
        }
    }

    @Override
    public void forgetPassword() {
        view.forgetFragment();
    }

    @Override
    public void sinUp() {
        view.singUp();
    }

    @Override
    public void isSuccess(Response<SingIn> response) {
        this.response = response;
        view.home();
    }

    public long getDateExpire() {

//        if (response != null) {
//            Date date = new Date(System.currentTimeMillis() + response.body().getAccessExpiresAt());
//            return date.getTime();
//        }
        return 0;
    }

    @Override
    public String getAccessToken() {

        if (response != null) {
            return response.body().getAccessToken();
        } else return null;
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }

    @Override
    public void retryConnection() {

        view.retryPopup();
    }

    @Override
    public void failLogin() {

        view.failLogin();
    }

    private boolean checkField() {

        return checkEmail() && checkPass();
    }

    private boolean checkEmail() {

        if (TextUtils.isEmpty(view.getEmail())) {
            view.showError(App.getInstance().getString(R.string.field_mail_empty));
            return false;
        } else return true;
    }

    private boolean checkPass() {

        if (TextUtils.isEmpty(view.getPassword())) {
            view.showError(App.getInstance().getString(R.string.field_pass_empty));
            return false;
        } else return true;
    }
}