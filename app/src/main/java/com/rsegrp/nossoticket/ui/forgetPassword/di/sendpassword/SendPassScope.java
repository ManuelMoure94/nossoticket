package com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SendPassScope {
}
