package com.rsegrp.nossoticket.ui.home.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.home.model.DrawerMenu;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

public class DrawerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements onClickListenerRecycler.clickHolder {

    private List<DrawerMenu> items;
    private onClickListenerRecycler.clickAdapter clickAdapter;

    private static final int ITEM_TYPE_1 = 0;
    private static final int ITEM_TYPE_2 = 1;
    private static final int ITEM_TYPE_3 = 2;

    private int position = 0;
    private int normalColor = Color.parseColor(Constant.NORMAL_COLOR);
    private int pressedColor = Color.parseColor(Constant.PRESSED_COLOR);

    public DrawerAdapter(List<DrawerMenu> items, final onClickListenerRecycler.clickAdapter clickAdapter) {
        this.items = items;
        this.clickAdapter = clickAdapter;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 5) {
            return ITEM_TYPE_2;
        } else if (position == 6) {
            return ITEM_TYPE_3;
        } else return ITEM_TYPE_1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == ITEM_TYPE_1) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_drawer, parent, false);
            return new DrawerViewHolder(view, this);
        } else if (viewType == ITEM_TYPE_2) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_drawer_type_2, parent, false);
            return new DrawerViewHolderItem2(view, this);
        } else if (viewType == ITEM_TYPE_3) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_drawer_type_3, parent, false);
            return new DrawerViewHolderItem3(view, this);
        } else return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DrawerViewHolder) {

            DrawerViewHolder drawerViewHolder = (DrawerViewHolder) holder;
            drawerViewHolder.icon.setImageResource(items.get(position).getIcon());
            drawerViewHolder.textName.setText(items.get(position).getName());

            for (int i = 0; i < items.size(); i++) {

                drawerViewHolder.textName.setTextColor(normalColor);
                if (position == this.position) {
                    drawerViewHolder.textName.setTextColor(pressedColor);
                }
            }

        } else if (holder instanceof DrawerViewHolderItem2) {

            DrawerViewHolderItem2 drawerViewHolderItem2 = (DrawerViewHolderItem2) holder;
            drawerViewHolderItem2.icon2.setImageResource(items.get(position).getIcon());
            drawerViewHolderItem2.textName2.setText(items.get(position).getName());

            if (this.position == 5) {
                drawerViewHolderItem2.textName2.setTextColor(pressedColor);
            } else drawerViewHolderItem2.textName2.setTextColor(normalColor);

        } else if (holder instanceof DrawerViewHolderItem3) {

            DrawerViewHolderItem3 drawerViewHolderItem3 = (DrawerViewHolderItem3) holder;
            drawerViewHolderItem3.icon3.setImageResource(items.get(position).getIcon());
            drawerViewHolderItem3.textName3.setText(items.get(position).getName());

            if (this.position == 6) {
                drawerViewHolderItem3.textName3.setTextColor(pressedColor);
            } else drawerViewHolderItem3.textName3.setTextColor(normalColor);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClicked(int position) {
        clickAdapter.onClicked(position);
        this.position = position;
        notifyDataSetChanged();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView textName;

        DrawerViewHolder(View itemView, final onClickListenerRecycler.clickHolder clickHolder) {
            super(itemView);

            icon = itemView.findViewById(R.id.iconDrawer);
            textName = itemView.findViewById(R.id.textDrawer);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        clickHolder.onClicked(position);
                    }
                }
            });
        }
    }

    class DrawerViewHolderItem2 extends RecyclerView.ViewHolder {

        ImageView icon2;
        TextView textName2;

        DrawerViewHolderItem2(View itemView, final onClickListenerRecycler.clickHolder clickHolder) {
            super(itemView);

            icon2 = itemView.findViewById(R.id.iconDrawer2);
            textName2 = itemView.findViewById(R.id.textDrawer2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        clickHolder.onClicked(position);
                    }
                }
            });
        }
    }

    class DrawerViewHolderItem3 extends RecyclerView.ViewHolder {

        ImageView icon3;
        TextView textName3;

        DrawerViewHolderItem3(View itemView, final onClickListenerRecycler.clickHolder clickHolder) {
            super(itemView);

            icon3 = itemView.findViewById(R.id.iconDrawer3);
            textName3 = itemView.findViewById(R.id.textDrawer3);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        clickHolder.onClicked(position);
                    }
                }
            });
        }
    }
}
