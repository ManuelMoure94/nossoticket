package com.rsegrp.nossoticket.ui.user.di.profile;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.user.fragment.ProfileFragment;
import com.rsegrp.nossoticket.ui.user.mvp.profile.ProfileInteract;
import com.rsegrp.nossoticket.ui.user.mvp.profile.ProfilePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Module
public class ProfileModule {

    private ProfileFragment profileFragment;

    public ProfileModule(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
    }

    @Provides
    @ProfileScope
    public ProfileFragment providesProfileFragment() {
        return new ProfileFragment();
    }

    @Provides
    @ProfileScope
    public ProfilePresenter providesProfilePresenter(ProfileInteract interact) {
        return new ProfilePresenter(profileFragment, interact);
    }

    @Provides
    @ProfileScope
    public ProfileInteract providesProfileInteract(SingInServices singInServices) {
        return new ProfileInteract(singInServices);
    }
}