package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardBlockPresenter implements CardBlockContract.Presenter {

    private CardBlockContract.View view;
    private CardBlockContract.Interact interact;

    public CardBlockPresenter(CardBlockContract.View view, CardBlockContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void setText() {
        view.setBalance();
        view.setColorIcon();
        view.setDate();
        view.setSerial();
    }

    @Override
    public void btnBlock() {
        if (checkComment()) {
            view.showAlertDialog();
        }
    }

    @Override
    public void blockCard(String cardId) {
        interact.deletedCard(cardId, this);
    }

    @Override
    public void onSuccess() {
        view.backCardList();
    }

    @Override
    public void hideProgress() {
    }

    private boolean checkComment() {

        if (TextUtils.isEmpty(view.getComment())) {
            view.showError(App.getInstance().getString(R.string.empty_comment));
            return false;
        } else return true;
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }
}
