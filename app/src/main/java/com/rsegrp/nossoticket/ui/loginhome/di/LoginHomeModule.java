package com.rsegrp.nossoticket.ui.loginhome.di;

import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeInteract;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@Module
public class LoginHomeModule {

    private LoginHomeActivity activity;

    public LoginHomeModule(LoginHomeActivity activity) {
        this.activity = activity;
    }

    @Provides
    @LoginHomeScope
    public LoginHomeActivity providesLoginHomeActivity(){
        return activity;
    }

    @Provides
    @LoginHomeScope
    public LoginHomePresenter providesLoginHomePresenter(LoginHomeInteract interact){
        return new LoginHomePresenter(activity, interact);
    }

    @Provides
    @LoginHomeScope
    public LoginHomeInteract providesLoginHomeInteract(){
        return new LoginHomeInteract();
    }
}
