package com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword;

import com.rsegrp.nossoticket.api.services.ForgotPasswordServices;
import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendPassFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword.SendPassInteract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword.SendPassPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@Module
public class SendPassModule {

    private SendPassFragment sendPassFragment;

    public SendPassModule(SendPassFragment sendPassFragment) {
        this.sendPassFragment = sendPassFragment;
    }

    @Provides
    @SendPassScope
    public SendPassFragment providesSendPassFragment() {
        return sendPassFragment;
    }

    @Provides
    @SendPassScope
    public SendPassPresenter providesSendPassPresenter(SendPassInteract interact) {
        return new SendPassPresenter(sendPassFragment, interact);
    }

    @Provides
    @SendPassScope
    public SendPassInteract providesSendPassInteract(ForgotPasswordServices services) {
        return new SendPassInteract(services);
    }
}
