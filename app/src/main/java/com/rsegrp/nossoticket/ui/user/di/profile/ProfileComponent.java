package com.rsegrp.nossoticket.ui.user.di.profile;

import com.rsegrp.nossoticket.ui.user.fragment.ProfileFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@ProfileScope
@Subcomponent(modules = {ProfileModule.class})
public interface ProfileComponent {
    void inject(ProfileFragment target);
}
