package com.rsegrp.nossoticket.ui.movements.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.movements.adapter.interfaces.OnClick;
import com.rsegrp.nossoticket.ui.movements.model.ListMovementsModel;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RSE_VZLA_07 on 4/7/2018.
 */

public class MovementsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnClick.OnClickHolder {

    private List<ListMovementsModel> items = new ArrayList<>();
    private OnClick.OnClickAdapter onClickAdapter;
    private Context context;

    private boolean animated = true;

    public MovementsListAdapter(Context context, final OnClick.OnClickAdapter onClickAdapter) {
        this.context = context;
        this.onClickAdapter = onClickAdapter;
    }

    public void updateListItem(List<ListMovementsModel> items, boolean animated) {
        this.items = items;
        this.animated = animated;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {

        if (items.get(position).getType().equals(Constant.TYPE_ITEM_RECHARGE)) {
            return Constant.ITEM_TYPE_RECHARGE;
        } else if (items.get(position).getType().equals(Constant.TYPE_ITEM_STATION)) {
            return Constant.ITEM_TYPE_STATION;
        } else if (items.get(position).getType().equals(Constant.TYPE_ITEM_PROGRESS)) {
            return Constant.ITEM_TYPE_PROGRESS;
        } else if (items.get(position).getType().equals(Constant.TYPE_ITEM_EMPTY_ITEM)) {
            return Constant.ITEM_TYPE_EMPTY;
        } else return Constant.ITEM_TYPE_END;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == Constant.ITEM_TYPE_END) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_no_data, parent, false);

            return new NoDataItemViewHolder(view);
        } else if (viewType == Constant.ITEM_TYPE_RECHARGE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movements_recharge, parent, false);

            return new RechargeItemViewHolder(view);
        } else if (viewType == Constant.ITEM_TYPE_PROGRESS) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progress, parent, false);

            return new ProgressItemViewHolder(view);
        } else if (viewType == Constant.ITEM_TYPE_EMPTY) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_empty_data, parent, false);

            return new EmptyItemViewHolder(view, this);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movements_station, parent, false);

            return new StationItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RechargeItemViewHolder) {

            RechargeItemViewHolder viewHolder = (RechargeItemViewHolder) holder;

            viewHolder.icon.setImageResource(R.drawable.ic_recharge);
            viewHolder.title.setText(items.get(position).getText1());
            viewHolder.date.setText(items.get(position).getData());
            viewHolder.balance.setText(items.get(position).getAmount());

        } else if (holder instanceof StationItemViewHolder) {

            StationItemViewHolder viewHolder = (StationItemViewHolder) holder;

            viewHolder.station.setText(items.get(position).getText1());
            viewHolder.date.setText(items.get(position).getData());
            viewHolder.balance.setText(items.get(position).getAmount());
        }

        if (animated) {
            animate(holder);
        }
    }

    private void animate(RecyclerView.ViewHolder holder) {

        final Animation animation = AnimationUtils.loadAnimation(App.getInstance().getBaseContext(), R.anim.fall_down);
        holder.itemView.setAnimation(animation);
    }

    private void clearAnimation(RecyclerView.ViewHolder holder) {
        holder.itemView.clearAnimation();
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);

        clearAnimation(holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClickAddTitle(int position) {
        onClickAdapter.onClickAddTitle(position);
    }

    public class RechargeItemViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView title;
        TextView date;
        TextView balance;

        public RechargeItemViewHolder(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.iv_item_movement_recharge);
            title = itemView.findViewById(R.id.tv_station_text_item_movement_recharge);
            date = itemView.findViewById(R.id.tv_date_text_item_movement_recharge);
            balance = itemView.findViewById(R.id.tv_amount_text_item_movement_recharge);
        }
    }

    public class StationItemViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView station;
        TextView date;
        TextView balance;

        public StationItemViewHolder(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.iv_item_movement_station);
            station = itemView.findViewById(R.id.tv_station_text_item_movement_station);
            date = itemView.findViewById(R.id.tv_date_text_item_movement_station);
            balance = itemView.findViewById(R.id.tv_amount_text_item_movement_station);
        }
    }

    public class NoDataItemViewHolder extends RecyclerView.ViewHolder {

        public NoDataItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ProgressItemViewHolder extends RecyclerView.ViewHolder {

        public ProgressItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class EmptyItemViewHolder extends RecyclerView.ViewHolder {

        Button btnAddTitle;

        public EmptyItemViewHolder(View itemView, final OnClick.OnClickHolder clickHolder) {
            super(itemView);

            btnAddTitle = itemView.findViewById(R.id.btnAddTitle);
            btnAddTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (position != RecyclerView.NO_POSITION) {
                        clickHolder.onClickAddTitle(position);
                    }
                }
            });
        }
    }
}
