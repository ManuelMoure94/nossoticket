package com.rsegrp.nossoticket.ui.settingsCard.fragment;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.adapter.DetailsAdapter;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails.CardDetailsModule;
import com.rsegrp.nossoticket.ui.settingsCard.model.DetailsModel;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails.CardDetailsContract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails.CardDetailsPresenter;
import com.rsegrp.nossoticket.utils.OnScrollRecycler;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardDetailsFragment extends BaseFragment implements CardDetailsContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    @Inject
    CardDetailsPresenter presenter;

    private String cardType;
    private String cardId;
    private String serial;
    private String balance;
    private String date;

    private boolean update;
    private boolean isUpdate = false;

    private DetailsAdapter adapter = new DetailsAdapter();

    final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

    @BindView(R.id.text_card_serial1)
    TextView showSerial;
    @BindView(R.id.text_card_balance1)
    TextView showBalance;
    @BindView(R.id.text_card_date1)
    TextView showDate;
    @BindView(R.id.balance_amount1)
    TextView showBalanceCarnet;
    @BindView(R.id.serial_carnet1)
    TextView showSerialCarnet;
    @BindView(R.id.code_carnet1)
    TextView showDateCarnet;
    @BindView(R.id.card_options_ife)
    TextView ifeOptions;
    @BindView(R.id.cdlp_options_carnet)
    TextView cdlpOptions;

    @BindView(R.id.item_carnet)
    CardView item_carnet;
    @BindView(R.id.item_ife)
    CardView item_ife;

    @BindView(R.id.recycler_details)
    RecyclerView recyclerDetails;

    @BindView(R.id.progress_movement)
    RelativeLayout progressMovement;

    @BindView(R.id.fab_movement)
    FloatingActionButton fabMovement;

    @BindView(R.id.swipe_movement)
    SwipeRefreshLayout swipeMovement;

    public static CardDetailsFragment newInstance(String cardType, String cardId, String serial, String balance, String date) {

        CardDetailsFragment cardDetailsFragment = new CardDetailsFragment();
        Bundle args = new Bundle();
        args.putString(Constant.ARGS_TYPE, cardType);
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);
        cardDetailsFragment.setArguments(args);

        return cardDetailsFragment;
    }

    public CardDetailsFragment() {
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new CardDetailsModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_detail, container, false);
        ButterKnife.bind(this, view);

        cardType = getArguments().getString(Constant.ARGS_TYPE);
        cardId = getArguments().getString(Constant.ARGS_CARD_ID);
        serial = getArguments().getString(Constant.ARGS_SERIAL);
        balance = getArguments().getString(Constant.ARGS_BALANCE);
        date = getArguments().getString(Constant.ARGS_DATE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (cardType.equals(Constant.CDLP)) {
            item_carnet.setVisibility(View.VISIBLE);
            item_ife.setVisibility(View.GONE);
        } else {
            item_ife.setVisibility(View.VISIBLE);
            item_carnet.setVisibility(View.GONE);
        }

        initIcon();

        presenter.getDetails(serial);
        presenter.setText();
        checkedBtn();

        Drawable iconArrow = ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_upward_black_24px);
        iconArrow.mutate().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        fabMovement.setImageDrawable(iconArrow);
//        recyclerDetails.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                if (dy > 0) {
//
//                    int visibleItemCount = layoutManager.getChildCount();
//                    int totalItemCount = layoutManager.getItemCount();
//                    int pastVisibleItem = layoutManager.findFirstVisibleItemPosition();
//
//                    if (update) {
//                        if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
//
//                            update = false;
//                            presenter.nextPage(cardId);
//                        }
//                    }
//                }
//            }
//        });
    }

    private void initIcon() {
        ((HomeActivity) getActivity()).setImageToolbar(R.drawable.ic_arrow_back_black_24px, 2);
    }

    private void checkedBtn() {

        swipeMovement.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getDetails(serial);
            }
        });

        fabMovement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerDetails.smoothScrollToPosition(0);
            }
        });

        recyclerDetails.addOnScrollListener(new OnScrollRecycler() {

            @Override
            public void show() {

                fabMovement.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {

                fabMovement.animate().translationY(5)
                        .setInterpolator(new LinearInterpolator())
                        .setDuration(750);
            }

            @Override
            public void refresh() {

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItem = layoutManager.findFirstVisibleItemPosition();

                isUpdate = true;

                if (update) {
                    if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {

                        update = false;
                        presenter.nextPage(serial);
                    }
                }
            }
        });

        ifeOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsMenu(ifeOptions);
            }
        });

        cdlpOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsMenu(cdlpOptions);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void optionsMenu(TextView view) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.inflate(R.menu.menu_card_movements);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.recharging:

                        if (cardType.equals(Constant.CDLP)) {
                            recharge();
                        } else {
                            Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.disaffiliate:
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.block:
                        block();
                        break;
                }
                return false;
            }
        });

        MenuPopupHelper menuHelper = new MenuPopupHelper(getActivity(), (MenuBuilder) popupMenu.getMenu(),
                view);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();
    }

    @Override
    public void initRecycler() {

        recyclerDetails.setHasFixedSize(true);

        recyclerDetails.setLayoutManager(layoutManager);

        recyclerDetails.setAdapter(adapter);

        swipeMovement.setRefreshing(false);

        update = true;
    }

    @Override
    public void updateList(List<DetailsModel> items) {
        adapter.updateListItem(items);
        update = items.get(items.size() - 1).getItemType() != Constant.ITEM_TYPE_END;
    }

    @Override
    public void showProgress() {
        progressMovement.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressMovement.setVisibility(View.GONE);
    }

    @Override
    public void hideProgressUpdate() {
        swipeMovement.setRefreshing(false);
    }

    @Override
    public void retry() {

        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
        hideProgress();
    }

    @Override
    public void error() {
        new DialogsOneBtn(this, getString(R.string.fail_movements), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
    }

    @Override
    public void setSerial() {
        showSerial.setText(serial);
        showSerialCarnet.setText(serial);
    }

    @Override
    public void setBalance() {
        showBalance.setText(balance);
        showBalanceCarnet.setText(balance);
    }

    @Override
    public void setDate() {
        showDate.setText(date);
        showDateCarnet.setText(date);
    }

    @Override
    public void setColorIcon() {

    }

    @Override
    public void recharge() {

        Bundle args = new Bundle();
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);

        Fragment fragment = CardRechargeFragment.newInstance(cardId, serial, balance, date);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void block() {

        Bundle args = new Bundle();
        args.putString(Constant.ARGS_TYPE, cardType);
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);

        Fragment fragment = CardBlockFragment.newInstance(cardType, cardId, serial, balance, date);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {

        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void clickListener(String options) {
        switch (options) {
            case "cancel":
                swipeMovement.setRefreshing(false);
                break;
            case "retry":
                if (isUpdate) {
                    presenter.nextPage(cardId);
                } else presenter.getDetails(cardId);
                break;
            case "accept":
                break;
        }
    }
}
