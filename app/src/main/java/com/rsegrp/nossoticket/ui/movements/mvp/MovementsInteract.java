package com.rsegrp.nossoticket.ui.movements.mvp;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.api.services.MovementServices;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public class MovementsInteract implements MovementsContract.Interact {

    private TitleServices titleServices;
    private MovementServices movementServices;

    public MovementsInteract(TitleServices titleServices, MovementServices movementServices) {
        this.titleServices = titleServices;
        this.movementServices = movementServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getTitles(final MovementsPresenter presenter) {

        Call<List<Titles>> call = titleServices.getTitles(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<List<Titles>>() {
            @Override
            public void onResponse(Call<List<Titles>> call, Response<List<Titles>> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<List<Titles>> call, Throwable t) {
                presenter.retry();
            }
        });
    }

    @Override
    public void getMovements(String cardId, final MovementsPresenter presenter, final int request, final boolean isRefresh) {

        Call<Movement> call = movementServices.Movement(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), cardId);

        call.enqueue(new Callback<Movement>() {
            @Override
            public void onResponse(Call<Movement> call, Response<Movement> response) {

                Log.i("TAG", "onResponse: " + response.code());

                if (response.isSuccessful()) {
                    presenter.onSuccessMovements(response, request + 1, isRefresh);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<Movement> call, Throwable t) {
                Log.i("TAG", "onResponse: " + Arrays.toString(t.getStackTrace()));
                presenter.retry();
            }
        });
    }

    @Override
    public void getNextPageMovements(String cardId, final String page, final int position, final MovementsPresenter presenter) {

        Call<Movement> call = movementServices.NextMovement(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), cardId, page);

        call.enqueue(new Callback<Movement>() {
            @Override
            public void onResponse(Call<Movement> call, Response<Movement> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccessNextMovements(response, position);
                } else presenter.error();
            }

            @Override
            public void onFailure(Call<Movement> call, Throwable t) {
                presenter.retry();
            }
        });
    }
}
