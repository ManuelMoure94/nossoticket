package com.rsegrp.nossoticket.ui.settingsCard.di.addCard;

import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.AddCardFragment;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard.AddCardInteract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard.AddCardPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@Module
public class AddCardModule {

    private AddCardFragment addCardFragment;

    public AddCardModule(AddCardFragment addCardFragment) {
        this.addCardFragment = addCardFragment;
    }

    @Provides
    @AddCardScope
    public AddCardFragment providesAddCardFragment() {
        return addCardFragment;
    }

    @Provides
    @AddCardScope
    public AddCardPresenter providesAddCardPresenter(AddCardInteract interact) {
        return new AddCardPresenter(addCardFragment, interact);
    }

    @Provides
    @AddCardScope
    public AddCardInteract providesAddCardInteract(TitleServices titleServices) {
        return new AddCardInteract(titleServices);
    }
}
