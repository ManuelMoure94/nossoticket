package com.rsegrp.nossoticket.ui.express.di.showTicket;

import com.rsegrp.nossoticket.ui.express.fragments.ShowTicketFragment;
import com.rsegrp.nossoticket.ui.express.mvp.showTicket.ShowTicketInteract;
import com.rsegrp.nossoticket.ui.express.mvp.showTicket.ShowTicketPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@Module
public class ShowTicketModule {

    private ShowTicketFragment showTicketFragment;

    public ShowTicketModule(ShowTicketFragment showTicketFragment) {
        this.showTicketFragment = showTicketFragment;
    }

    @Provides
    @ShowTicketScope
    public ShowTicketFragment providesShowTicketFragment() {
        return showTicketFragment;
    }

    @Provides
    @ShowTicketScope
    public ShowTicketPresenter providesShowTicketPresenter(ShowTicketInteract interact) {
        return new ShowTicketPresenter(showTicketFragment, interact);
    }

    @Provides
    @ShowTicketScope
    public ShowTicketInteract providesShowTicketInteract() {
        return new ShowTicketInteract();
    }
}
