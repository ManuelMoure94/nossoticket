package com.rsegrp.nossoticket.ui.summary.model;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class SummaryModel {

    private String type;
    private String day;
    private String year;
    private String info;
    private ChildSummaryModel childSummaryModel;

    public SummaryModel(String type, String day, String year, String info, ChildSummaryModel childSummaryModel) {
        this.type = type;
        this.day = day;
        this.year = year;
        this.info = info;
        this.childSummaryModel = childSummaryModel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ChildSummaryModel getChildSummaryModel() {
        return childSummaryModel;
    }

    public void setChildSummaryModel(ChildSummaryModel childSummaryModel) {
        this.childSummaryModel = childSummaryModel;
    }
}
