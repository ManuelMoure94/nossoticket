package com.rsegrp.nossoticket.ui.movements.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MovementsScope {
}
