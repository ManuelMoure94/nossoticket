package com.rsegrp.nossoticket.ui.loginhome.mvp;

import android.support.v4.app.Fragment;

import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class LoginHomePresenter implements LoginHomeContract.Presenter {

    private LoginHomeContract.View view;
    private LoginHomeContract.Interact interact;

    private Fragment fragment;

    public LoginHomePresenter(LoginHomeContract.View view, LoginHomeContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void initFragment() {
        login();
    }

    @Override
    public void checkAutoLogin(boolean state) {

        if (state){
            view.autoLogin();
        }
    }

    private void login(){

        fragment = new LoginFragment();
        view.setFragment(fragment);
    }
}
