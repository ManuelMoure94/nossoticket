package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock;

import com.rsegrp.nossoticket.ui.base.BaseContract;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public interface CardBlockContract {

    interface View extends BaseContract.View {

        void setSerial();

        void setBalance();

        void setDate();

        void setColorIcon();

        String getComment();

        void backCardList();

        void showAlertDialog();
    }

    interface Presenter extends BaseContract.Presenter {

        void setText();

        void btnBlock();

        void blockCard(String cardId);

        void onSuccess();

        void hideProgress();
    }

    interface Interact {

        void deletedCard(String cardId, CardBlockPresenter presenter);
    }
}
