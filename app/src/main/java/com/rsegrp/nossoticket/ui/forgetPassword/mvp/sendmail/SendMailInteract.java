package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.forgot.SendEmail;
import com.rsegrp.nossoticket.api.services.ForgotPasswordServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendMailInteract implements SendMailContract.Interact {

    private ForgotPasswordServices forgotPasswordServices;

    public SendMailInteract(ForgotPasswordServices forgotPasswordServices) {
        this.forgotPasswordServices = forgotPasswordServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void sendMail(String email, final SendMailPresenter presenter) {

        SendEmail sendEmail = new SendEmail(email);

        Call<String> call = forgotPasswordServices.ForgotPwSendingMail(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, sendEmail);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.i("TAG", "onResponse");

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else {
                    Log.i("TAG", "onError " + response.code());
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.i("TAG", "onFailure " + t.getMessage());
                presenter.onFailed();
            }
        });
    }
}
