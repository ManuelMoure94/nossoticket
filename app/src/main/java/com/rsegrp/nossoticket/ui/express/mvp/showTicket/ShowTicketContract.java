package com.rsegrp.nossoticket.ui.express.mvp.showTicket;

import android.graphics.Bitmap;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public interface ShowTicketContract {

    interface View {

        void setQr(Bitmap qr);

        void showQr();

        void hideQr();
    }

    interface Presenter {

        void generateQR();
    }

    interface Interact {

    }
}
