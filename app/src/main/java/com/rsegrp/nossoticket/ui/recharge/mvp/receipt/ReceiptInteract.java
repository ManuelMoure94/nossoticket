package com.rsegrp.nossoticket.ui.recharge.mvp.receipt;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.wallet.recharge.RechargeRequest;
import com.rsegrp.nossoticket.api.data.wallet.recharge.WalletRecharge;
import com.rsegrp.nossoticket.api.services.WalletServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ReceiptInteract implements ReceiptContract.Interact {

    private String TAG = "tag";

    private WalletServices walletServices;

    public ReceiptInteract(WalletServices walletServices) {
        this.walletServices = walletServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void rechargeRequest(final ReceiptPresenter presenter, final RechargeRequest request) {

        Call<WalletRecharge> call = walletServices.walletRecharge(Constant.CONTENT_TYPE,
                Constant.TOKEN + presenter.getToken(), request);

        call.enqueue(new Callback<WalletRecharge>() {
            @Override
            public void onResponse(Call<WalletRecharge> call, Response<WalletRecharge> response) {

                Log.i(TAG, "onResponse: " + request.getType() + " " + request.getDate());

                if (response.isSuccessful()) {
                    Log.i(TAG, "onResponse: " + response.code());
                    presenter.isSuccess(response);
                } else if (response.code() >= 400) {
                    Log.i(TAG, "onResponse: " + response.code() + " " + response.raw());
                    presenter.rechargingError();
                }
            }

            @Override
            public void onFailure(Call<WalletRecharge> call, Throwable t) {
                Log.i(TAG, "onResponse: " + t.getMessage() + t.getCause());
                presenter.retryConnection();
            }
        });
    }
}
