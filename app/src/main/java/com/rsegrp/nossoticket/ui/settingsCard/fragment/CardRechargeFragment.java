package com.rsegrp.nossoticket.ui.settingsCard.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.RechargeTittle;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardRecharge.CardRechargeModule;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge.CardRechargeContract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge.CardRechargePresenter;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.ui.title.fragment.TitleFragment;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class CardRechargeFragment extends BaseFragment implements CardRechargeContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    private String cardId;
    private String serial;
    private String balance;
    private String date;

    private RechargeTittle rechargeTittle;

    @BindView(R.id.show_balance)
    TextView showBalance;
    @BindView(R.id.text_serial)
    TextView showSerial;
    @BindView(R.id.text_balance)
    TextView showAmount;
    @BindView(R.id.text_date)
    TextView showDate;
    @BindView(R.id.text_message)
    TextView showMessage;
    @BindView(R.id.title_amount)
    TextView titleAmount;
    @BindView(R.id.processing)
    TextView processing;

    @BindView(R.id.tittle_progress)
    ProgressBar tittleProgress;

    @BindView(R.id.text_amount_card)
    MaterialEditText textAmount;

    @BindView(R.id.btn_balance_card)
    Button btnBalance;

    @BindView(R.id.containt_balance)
    ConstraintLayout containerBalance;

    @Inject
    CardRechargePresenter presenter;

    public static CardRechargeFragment newInstance(String cardId, String serial, String balance, String date) {

        CardRechargeFragment cardRechargeFragment = new CardRechargeFragment();
        Bundle args = new Bundle();
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);
        cardRechargeFragment.setArguments(args);

        return cardRechargeFragment;
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new CardRechargeModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt_card, container, false);
        ButterKnife.bind(this, view);

        cardId = getArguments().getString(Constant.ARGS_CARD_ID);
        serial = getArguments().getString(Constant.ARGS_SERIAL);
        balance = getArguments().getString(Constant.ARGS_BALANCE);
        date = getArguments().getString(Constant.ARGS_DATE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter.getBalance();
        presenter.setText();
        checkBtn();
    }

    private void checkBtn() {

        btnBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechargeTittle = new RechargeTittle(serial, Float.parseFloat(getAmount()));
                presenter.btnBalance(rechargeTittle);
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void setSerial() {
        showSerial.setText(serial);
    }

    @Override
    public void setBalance() {
        showAmount.setText(balance + " Bs");
    }

    @Override
    public void setDate() {
        showDate.setText(date);
    }

    @Override
    public void setBalanceAccount(String balanceAccount) {
        showBalance.setText(balanceAccount);
    }

    @Override
    public void setTextBtn(String textBtn) {
        btnBalance.setText(textBtn);
    }

    @Override
    public String getAmount() {
        return textAmount.getText().toString();
    }

    @Override
    public String getTextBtn() {
        return btnBalance.getText().toString();
    }

    @Override
    public void showMessage() {
        showMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showHeader() {
        containerBalance.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFieldRecharge() {
        textAmount.setVisibility(View.VISIBLE);
        titleAmount.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMessage() {
        showMessage.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideHeader() {
        containerBalance.setVisibility(View.GONE);
    }

    @Override
    public void hideFieldRecharge() {
        textAmount.setVisibility(View.GONE);
        titleAmount.setVisibility(View.GONE);
    }

    @Override
    public void backCardList() {
        Fragment fragment = new TitleFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void goSummary() {
        Fragment fragment = new SummaryFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void retryConnection() {
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void errorConnection() {
        new DialogsOneBtn(this, getString(R.string.fail_title_recharge), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
    }

    @Override
    public void hideProgress() {
        tittleProgress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        tittleProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideBtn() {
        btnBalance.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showBtn() {
        btnBalance.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProcess() {
        processing.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProcess() {
        processing.setVisibility(View.GONE);
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                backCardList();
                break;

            case "retry":
                rechargeTittle = new RechargeTittle(serial, Float.parseFloat(balance));
                presenter.btnBalance(rechargeTittle);
                break;

            case "accept":
                backCardList();
                break;
        }
    }
}