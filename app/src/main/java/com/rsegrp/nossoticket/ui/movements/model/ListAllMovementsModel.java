package com.rsegrp.nossoticket.ui.movements.model;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 5/7/2018.
 */

public class ListAllMovementsModel {

    private String cardId;
    private int page;
    private List<ListMovementsModel> listMovementsModels;

    public ListAllMovementsModel(String cardId, int page, List<ListMovementsModel> listMovementsModels) {
        this.cardId = cardId;
        this.page = page;
        this.listMovementsModels = listMovementsModels;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<ListMovementsModel> getListMovementsModels() {
        return listMovementsModels;
    }

    public void setListMovementsModels(List<ListMovementsModel> listMovementsModels) {
        this.listMovementsModels = listMovementsModels;
    }

    public void addListMovementsModels(String type, String text, String date, String amount) {
        listMovementsModels.add(new ListMovementsModel(type, text, date, amount));
    }
}
