package com.rsegrp.nossoticket.ui.home.mvp;

import android.support.v4.app.Fragment;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.express.fragments.AddNewTicketFragment;
import com.rsegrp.nossoticket.ui.movements.fragment.MovementsFragment;
import com.rsegrp.nossoticket.ui.recharge.fragment.RechargeFragment;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.ui.user.fragment.NewPasswordFragment;
import com.rsegrp.nossoticket.ui.user.fragment.ProfileFragment;
import com.rsegrp.nossoticket.ui.user.fragment.UpdateFragment;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

public class HomePresenter implements HomeContract.Presenter {

    public HomeContract.View view;
    public HomeContract.Interact interact;

    private Fragment fragment;

    private Response<DataUser> response;

    public HomePresenter(HomeContract.View view, HomeContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnDrawer(int control) {

        if (view.getImageToolbar() == R.drawable.ic_arrow_back_black_24px) {
            view.setImageToolbar(R.drawable.ic_menu, 0);
            if (control == 1) {
                ticketExpress();
            } else if (control == 2) {
                title();
            } else summary();
        } else view.openDrawer();
    }

    @Override
    public void initFragment() {
        summary();
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void selectFragment(int position) {

        switch (position) {

            case 0:
                summary();
                view.closeDrawer();
                break;

            case 1:
                title();
                view.closeDrawer();
                break;

            case 2:
                recharge();
                view.closeDrawer();
                break;

            case 3:
                ticketExpress();
                view.closeDrawer();
                break;

            case 4:
                profile();
                view.closeDrawer();
                break;

            case 5:
                update();
                view.closeDrawer();
                break;

            case 6:
                newPassword();
                view.closeDrawer();
                break;

            case 7:
                logout();
                view.closeDrawer();
                break;
        }
    }

    @Override
    public void isSuccess(Response<DataUser> response) {

        this.response = response;

        String year = response.body().getCreatedAt().substring(0, 4);
        String month = response.body().getCreatedAt().substring(5, 7);
        String day = response.body().getCreatedAt().substring(8, 10);

        view.setFullName(response.body().getEmail());
        view.setEmail(response.body().getRemaining() + " Bs");
        view.setPhone(day + "/" + month + "/" + year);
    }

    @Override
    public String getUserFullName() {

//        if (response != null) {
//            return response.body().getName() + " " + response.body().getLastName();
//        } else return null;
        return null;
    }

    @Override
    public String getUserId() {

//        if (response != null) {
//            return response.body().getIdentification();
//        } else return null;
        return null;
    }

    @Override
    public String getUserEmail() {

        if (response != null) {
            return response.body().getEmail();
        } else return null;
    }

    @Override
    public String getUserDate() {

        if (response != null) {
            return response.body().getCreatedAt();
        } else return null;
    }

    @Override
    public void getUser() {
        interact.getUser(this);
    }

    private void summary() {

        if (response != null) {
            fragment = SummaryFragment.newInstance(response.body().getRemaining());
        } else {
            fragment = SummaryFragment.newInstance(0);
        }
        view.setFragment(fragment);
    }

    private void title() {

        fragment = new MovementsFragment();
        view.setFragment(fragment);
    }

    private void recharge() {

        fragment = new RechargeFragment();
        view.setFragment(fragment);
    }

    private void ticketExpress() {

        if (response != null) {
            fragment = AddNewTicketFragment.newInstance(response.body().getRemaining());
        } else {
            fragment = AddNewTicketFragment.newInstance(0);
        }
        view.setFragment(fragment);
    }

    private void profile() {

        fragment = new ProfileFragment();
        view.setFragment(fragment);
    }

    private void update() {

        fragment = UpdateFragment.newInstance(response.body());
        view.setFragment(fragment);
    }

    private void newPassword() {

        fragment = new NewPasswordFragment();
        view.setFragment(fragment);
    }

    private void logout() {

        view.logout();
    }
}
