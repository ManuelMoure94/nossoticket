package com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard;

import com.rsegrp.nossoticket.api.data.titles.AddTitle;
import com.rsegrp.nossoticket.api.data.titles.NewTitle;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public interface AddCardContract {

    interface View extends BaseContract.View {

        String getSerial();

        String getDescription();

        void backCardList();

        void retry();

        void error();

        void showProgress();

        void hideProgress();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnAdd();

        void onSuccess(Response<AddTitle> response);

        void retry();

        void error();
    }

    interface Interact {

        void addTitle(NewTitle newTitle, final AddCardPresenter presenter);
    }
}
