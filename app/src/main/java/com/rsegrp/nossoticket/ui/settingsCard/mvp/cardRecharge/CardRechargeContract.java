package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.RechargeTittle;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.TittleRequest;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface CardRechargeContract {

    interface View extends BaseContract.View {

        void setSerial();

        void setBalance();

        void setDate();

        void setBalanceAccount(String balanceAccount);

        void setTextBtn(String textBtn);

        String getAmount();

        String getTextBtn();

        void showMessage();

        void showHeader();

        void showFieldRecharge();

        void hideMessage();

        void hideHeader();

        void hideFieldRecharge();

        void backCardList();

        void goSummary();

        void retryConnection();

        void errorConnection();

        void hideProgress();

        void showProgress();

        void hideBtn();

        void showBtn();

        void showProcess();

        void hideProcess();
    }

    interface Presenter extends BaseContract.Presenter {

        void getBalance();

        void setText();

        void btnBalance(RechargeTittle rechargeTittle);

        void isSuccess(Response<TittleRequest> response);

        void successAmount(Response<DataUser> response);

        void retryConnection();

        void errorConnection();
    }

    interface Interact {

        void rechargeTittle(CardRechargePresenter presenter, RechargeTittle rechargeTittle);

        void getAmount(final CardRechargePresenter presenter);
    }
}
