package com.rsegrp.nossoticket.ui.home.di;

import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

@HomeScope
@Subcomponent(modules = {HomeModule.class})
public interface HomeComponent {
    void inject(HomeActivity target);
}
