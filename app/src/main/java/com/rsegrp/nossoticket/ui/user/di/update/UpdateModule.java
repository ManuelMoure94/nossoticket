package com.rsegrp.nossoticket.ui.user.di.update;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.user.fragment.UpdateFragment;
import com.rsegrp.nossoticket.ui.user.mvp.update.UpdateInteract;
import com.rsegrp.nossoticket.ui.user.mvp.update.UpdatePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Module
public class UpdateModule {

    private UpdateFragment updateFragment;

    public UpdateModule(UpdateFragment updateFragment) {
        this.updateFragment = updateFragment;
    }

    @Provides
    @UpdateScope
    public UpdateFragment providesUpdateFragment() {
        return updateFragment;
    }

    @Provides
    @UpdateScope
    public UpdatePresenter providesUpdatePresenter(UpdateInteract interact) {
        return new UpdatePresenter(updateFragment, interact);
    }

    @Provides
    @UpdateScope
    public UpdateInteract providesUpdateInteract(SingInServices services) {
        return new UpdateInteract(services);
    }
}
