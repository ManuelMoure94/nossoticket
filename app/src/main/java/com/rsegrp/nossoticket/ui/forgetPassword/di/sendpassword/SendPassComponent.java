package com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword;

import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendPassFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@SendPassScope
@Subcomponent(modules = {SendPassModule.class})
public interface SendPassComponent {
    void inject(SendPassFragment target);
}
