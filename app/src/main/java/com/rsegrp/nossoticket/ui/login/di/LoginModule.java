package com.rsegrp.nossoticket.ui.login.di;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;
import com.rsegrp.nossoticket.ui.login.mvp.LoginInteract;
import com.rsegrp.nossoticket.ui.login.mvp.LoginPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@Module
public class LoginModule {

    private LoginFragment loginFragment;

    public LoginModule(LoginFragment loginFragment) {
        this.loginFragment = loginFragment;
    }

    @Provides
    @LoginScope
    public LoginFragment providesLoginFragment() {
        return loginFragment;
    }

    @Provides
    @LoginScope
    public LoginPresenter providesLoginPresenter(LoginInteract interact) {
        return new LoginPresenter(loginFragment, interact);
    }

    @Provides
    @LoginScope
    public LoginInteract providesLoginInteract(SingInServices singInServices) {
        return new LoginInteract(singInServices);
    }
}