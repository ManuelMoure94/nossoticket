package com.rsegrp.nossoticket.ui.root;

import android.app.Application;

import com.rsegrp.nossoticket.api.ApiModule;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.root.component.DaggerApplicationComponent;
import com.rsegrp.nossoticket.ui.root.module.ApplicationModule;

/**
 * Created by RSE_VZLA_07 on 2/2/2018.
 */

public class App extends Application{

    private ApplicationComponent appComponent;
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);

        appComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModule(new ApiModule())
                .build();
    }

    public ApplicationComponent getAppComponent() {
        return appComponent;
    }


}
