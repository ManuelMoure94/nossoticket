package com.rsegrp.nossoticket.ui.loginhome.di;

import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@LoginHomeScope
@Subcomponent(modules = {LoginHomeModule.class})
public interface LoginHomeComponent {
    void inject(LoginHomeActivity target);
}
