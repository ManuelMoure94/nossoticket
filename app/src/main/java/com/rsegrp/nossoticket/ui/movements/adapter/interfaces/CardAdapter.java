package com.rsegrp.nossoticket.ui.movements.adapter.interfaces;

import android.support.v7.widget.CardView;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public interface CardAdapter {

    public final int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
