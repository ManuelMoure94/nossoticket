package com.rsegrp.nossoticket.ui.home.di;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.home.mvp.HomeInteract;
import com.rsegrp.nossoticket.ui.home.mvp.HomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

@Module
public class HomeModule {

    private HomeActivity activity;

    public HomeModule(HomeActivity activity) {
        this.activity = activity;
    }

    @Provides
    @HomeScope
    public HomeActivity providesHomeActivity() {
        return activity;
    }

    @Provides
    @HomeScope
    public HomePresenter providesHomePresenter(HomeInteract interact) {
        return new HomePresenter(activity, interact);
    }

    @Provides
    @HomeScope
    public HomeInteract providesHomeInteract(SingInServices singInServices) {
        return new HomeInteract(singInServices);
    }
}
