package com.rsegrp.nossoticket.ui.settingsCard.di.cardRecharge;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardRechargeFragment;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge.CardRechargeInteract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge.CardRechargePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Module
public class CardRechargeModule {

    private CardRechargeFragment cardRechargeFragment;

    public CardRechargeModule(CardRechargeFragment cardRechargeFragment) {
        this.cardRechargeFragment = cardRechargeFragment;
    }

    @Provides
    @CardRechargeScope
    public CardRechargeFragment providesCardRechargeFragment() {
        return cardRechargeFragment;
    }

    @Provides
    @CardRechargeScope
    public CardRechargePresenter providesCardRechargePresenter(CardRechargeInteract interact) {
        return new CardRechargePresenter(cardRechargeFragment, interact);
    }

    @Provides
    @CardRechargeScope
    public CardRechargeInteract providesCardRechargeInteract(TitleServices titleServices, SingInServices singInServices) {
        return new CardRechargeInteract(titleServices, singInServices);
    }
}