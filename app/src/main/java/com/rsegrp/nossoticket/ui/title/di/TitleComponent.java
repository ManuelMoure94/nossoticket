package com.rsegrp.nossoticket.ui.title.di;

import com.rsegrp.nossoticket.ui.title.fragment.TitleFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

@TitleScope
@Subcomponent(modules = {TitleModule.class})
public interface TitleComponent {
    void inject(TitleFragment target);
}
