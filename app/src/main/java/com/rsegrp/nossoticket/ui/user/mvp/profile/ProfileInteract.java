package com.rsegrp.nossoticket.ui.user.mvp.profile;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ProfileInteract implements ProfileContract.interact {

    private SingInServices singInServices;

    public ProfileInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getUser(final ProfilePresenter presenter) {

        Call<DataUser> call = singInServices.getUser(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
                presenter.hideProgress();
            }
        });
    }
}
