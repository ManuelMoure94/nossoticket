package com.rsegrp.nossoticket.ui.express.mvp.showTicket;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class ShowTicketPresenter implements ShowTicketContract.Presenter {

    private ShowTicketContract.View view;
    private ShowTicketContract.Interact interact;

    private final static int QR_CODE_WIDTH = 500;
    private final static int QR_CODE_HEIGHT = 500;
    private final static String IMAGE_DIRECTORY = "/QRcodeDemonuts";

    private Bitmap bitmap;

    public ShowTicketPresenter(ShowTicketContract.View view, ShowTicketContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void generateQR() {

        new DrawQr().execute();
    }

    private String getQR() {
        return "https://" + System.currentTimeMillis() + "/info/dbbe98637929414ab21a25283fb56b8c/0005225958/V11179904";
    }

    private Bitmap TextToImageEncode(String qr) throws WriterException {

        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    qr,
                    BarcodeFormat.QR_CODE,
                    QR_CODE_WIDTH, QR_CODE_HEIGHT, null);
        } catch (IllegalArgumentException illegalArgumentException) {
            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ?
                        App.getInstance().getResources().getColor(R.color.blackQR) :
                        App.getInstance().getResources().getColor(R.color.whiteQR);
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);

        return bitmap;
    }

    private class DrawQr extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showQr();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                bitmap = TextToImageEncode(getQR());
            } catch (WriterException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            view.hideQr();
            view.setQr(bitmap);
        }
    }
}
