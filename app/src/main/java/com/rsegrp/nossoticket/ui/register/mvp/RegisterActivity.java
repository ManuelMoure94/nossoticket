package com.rsegrp.nossoticket.ui.register.mvp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseActivity;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.register.di.RegisterModule;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class RegisterActivity extends BaseActivity implements RegisterContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    @Inject
    RegisterPresenter presenter;

    @BindView(R.id.input_email)
    MaterialEditText inputMail;
    @BindView(R.id.input_pass)
    MaterialEditText inputPassword;
    @BindView(R.id.input_rePass)
    MaterialEditText inputRePassword;

    @BindView(R.id.btn_menu)
    ImageView btnMenu;

    @BindView(R.id.btn_sign_in)
    Button btnSingUp;

    @BindView(R.id.rlProgressRegister)
    RelativeLayout rlProgressRegister;

    private boolean registered;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new RegisterModule(this)).inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        btnMenu.setVisibility(View.GONE);
        Drawable iconArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24px);
        iconArrow.mutate().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        btnMenu.setImageDrawable(iconArrow);

        checkBtn();
    }

    private void checkBtn() {

        btnSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnRegister();
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginHomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public String getEmail() {
        return inputMail.getText().toString();
    }

    @Override
    public String getPassword() {
        return inputPassword.getText().toString();
    }

    @Override
    public String getRePassword() {
        return inputRePassword.getText().toString();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public void home() {
        Intent intent = new Intent(this, HomeActivity.class);
        savePreferences(presenter.getToken());
        startActivity(intent);
        finish();
    }

    @Override
    public void retryPopup() {

        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getSupportFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void error() {

        registered = false;
        new DialogsOneBtn(this, getString(R.string.fail_register), getString(R.string.accept)).show(getSupportFragmentManager(), "Fail");
    }

    @Override
    public void errorConnection() {

        registered = true;
        new DialogsOneBtn(this, getString(R.string.message_failure), getString(R.string.accept)).show(getSupportFragmentManager(), "Fail");
    }

    @Override
    public void showProgress() {
        rlProgressRegister.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        rlProgressRegister.setVisibility(View.GONE);
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                break;
            case "retry":
                presenter.btnRegister();
                break;
            case "accept":
                if (registered) {
                    Intent intent = new Intent(this, LoginHomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;
        }
    }
}
