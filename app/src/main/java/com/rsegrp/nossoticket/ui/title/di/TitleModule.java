package com.rsegrp.nossoticket.ui.title.di;

import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.title.fragment.TitleFragment;
import com.rsegrp.nossoticket.ui.title.mvp.TitleInteract;
import com.rsegrp.nossoticket.ui.title.mvp.TitlePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

@Module
public class TitleModule {

    private TitleFragment titleFragment;

    public TitleModule(TitleFragment titleFragment) {
        this.titleFragment = titleFragment;
    }

    @Provides
    @TitleScope
    public TitleFragment providesTitleFragment() {
        return titleFragment;
    }

    @Provides
    @TitleScope
    public TitlePresenter providesTitlePresenter(TitleInteract interact) {
        return new TitlePresenter(titleFragment, interact);
    }

    @Provides
    @TitleScope
    public TitleInteract providesTitleInteract(TitleServices titleServices) {
        return new TitleInteract(titleServices);
    }
}
