package com.rsegrp.nossoticket.ui.summary.di;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.SummaryServices;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.ui.summary.mvp.SummaryInteract;
import com.rsegrp.nossoticket.ui.summary.mvp.SummaryPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

@Module
public class SummaryModule {

    private SummaryFragment summaryFragment;

    public SummaryModule(SummaryFragment summaryFragment) {
        this.summaryFragment = summaryFragment;
    }

    @Provides
    @SummaryScope
    public SummaryFragment providesSummaryFragment() {
        return summaryFragment;
    }

    @Provides
    @SummaryScope
    public SummaryPresenter providesSummaryPresenter(SummaryInteract interact) {
        return new SummaryPresenter(summaryFragment, interact);
    }

    @Provides
    @SummaryScope
    public SummaryInteract providesSummaryInteract(SummaryServices summaryServices, SingInServices singInServices) {
        return new SummaryInteract(summaryServices, singInServices);
    }
}
