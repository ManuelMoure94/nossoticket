package com.rsegrp.nossoticket.ui.user.mvp.profile;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    private ProfileContract.View view;
    private ProfileContract.interact interact;

    Response<DataUser> response;

    public ProfilePresenter(ProfileContract.View view, ProfileContract.interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void initProfile() {
        view.showProgress();
        interact.getUser(this);
    }

    @Override
    public void btnProfile() {
        DataUser dataUser = response.body();
        view.nextFragment(dataUser);
    }

    @Override
    public void onSuccess(Response<DataUser> response) {

        view.hideProgress();
        if (response != null) {
            this.response = response;
            view.setTextFName(response.body().getName());
            view.setTextLName(response.body().getLastName());
            view.setTextDate(response.body().getBirthDate());
            view.setTextId(response.body().getIdentification());
            view.setTextAddress(response.body().getAddress());
            view.setTextMail(response.body().getEmail());
            view.setTextPhone(response.body().getPhone());
        }
    }

    @Override
    public void hideProgress() {
        view.hideProgress();
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }
}
