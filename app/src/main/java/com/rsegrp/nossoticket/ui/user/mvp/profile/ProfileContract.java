package com.rsegrp.nossoticket.ui.user.mvp.profile;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface ProfileContract {

    interface View extends BaseContract.View {

        void setTextFName(String fName);

        void setTextLName(String lName);

        void setTextDate(String date);

        void setTextId(String id);

        void setTextAddress(String address);

        void setTextMail(String mail);

        void setTextPhone(String phone);

        void nextFragment(DataUser dataUser);

        void showProgress();

        void hideProgress();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnProfile();

        void initProfile();

        void onSuccess(Response<DataUser> response);

        void hideProgress();
    }

    interface interact {

        void getUser(ProfilePresenter presenter);
    }
}
