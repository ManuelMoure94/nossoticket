package com.rsegrp.nossoticket.ui.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;

/**
 * Created by RSE_VZLA_07 on 2/2/2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract void injectDependencies(ApplicationComponent applicationComponent);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies(App.getInstance().getAppComponent());
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public Boolean loadPreferencesState() {

        SharedPreferences sharedPreferences = getSharedPreferences("RememberPreference", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("Remember", false);
    }

    public String loadPreferencesEmail() {

        SharedPreferences sharedPreferences = getSharedPreferences("RememberPreference", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Email", " ");
    }

    public void saveUser(boolean checked, String Email) {

        SharedPreferences sharedPreferences = getSharedPreferences("RememberPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("Remember", checked);
        editor.putString("Email", Email);
        editor.apply();
    }

    public long loadPreferencesDate() {

        SharedPreferences sharedPreferences = getSharedPreferences("LoginPreference", Context.MODE_PRIVATE);
        return sharedPreferences.getLong("Date", 0);
    }

    public String loadPreferencesToken() {

        SharedPreferences sharedPreferences = getSharedPreferences("LoginPreference", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Token", " ");
    }

    public void savePreferences(String token) {

        SharedPreferences sharedPreferences = getSharedPreferences("LoginPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Token", token);
        editor.apply();
    }
}
