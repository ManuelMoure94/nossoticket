package com.rsegrp.nossoticket.ui.forgetPassword.di.sendcode;

import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendCodeFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@SendCodeScope
@Subcomponent(modules = {SendCodeModule.class})
public interface SendCodeComponent {
    void inject(SendCodeFragment target);
}
