package com.rsegrp.nossoticket.ui.user.mvp.changePw;

import com.rsegrp.nossoticket.api.data.singIn.DataNewPassword;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface NewPasswordContract {

    interface View extends BaseContract.View {

        String getPassword();

        String getNewPassword();

        String getNewRePassword();

        void hideProgress();

        void showProgress();

        void nextFragment();

        void onSuccess();

        void onError();

        void onFailed();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnNewPassword();

        void onSuccess(Response<String> response);

        void onError();

        void onFailed();
    }

    interface Interact {

        void changedPassword(DataNewPassword dataNewPassword, NewPasswordPresenter presenter);
    }
}
