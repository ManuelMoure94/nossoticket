package com.rsegrp.nossoticket.ui.title.mvp;

import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public interface TitleContract {

    interface View extends BaseContract.View {

        void addCard();

        void initRecycler();

        void showProgress();

        void hideProgress();

        void hideProgressUpdate();

        void retry();

        void error();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnAdd();

        void getTitles(boolean state);

        int getSize();

        int getCardAmount(int position);

        String getCardType(int position);

        String getCardId(int position);

        String getCardSerial(int position);

        String getCardDate(int position);

        boolean getHaveTitles();

        void onSuccess(Response<List<Titles>> response);

        void hideProgress();

        void retry();

        void error();
    }

    interface Interact {

        void getTitles(TitlePresenter presenter);
    }
}
