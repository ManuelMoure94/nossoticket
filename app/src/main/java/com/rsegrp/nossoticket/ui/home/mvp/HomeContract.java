package com.rsegrp.nossoticket.ui.home.mvp;

import android.support.v4.app.Fragment;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

public interface HomeContract {

    interface View extends BaseContract.View {

        void openDrawer();

        void closeDrawer();

        void setFragment(Fragment fragment);

        void showToolbar();

        void hideToolbar();

        void logout();

        void setImageToolbar(int image, int control);

        int getImageToolbar();

        void setFullName(String fullName);

        void setEmail(String email);

        void setPhone(String phone);

        void showPlusBottom();

        void hidePlusBottom();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnDrawer(int control);

        void initFragment();

        void getUser();

        void selectFragment(int position);

        void isSuccess(Response<DataUser> response);

        String getUserFullName();

        String getUserId();

        String getUserEmail();

        String getUserDate();
    }

    interface Interact {

        void getUser(HomePresenter presenter);
    }
}
