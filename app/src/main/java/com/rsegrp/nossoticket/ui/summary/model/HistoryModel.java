package com.rsegrp.nossoticket.ui.summary.model;

/**
 * Created by RSE_VZLA_07 on 14/8/2018.
 */

public class HistoryModel {

    private int icon;
    private String status;
    private String date;

    public HistoryModel(int icon, String status, String date) {
        this.icon = icon;
        this.status = status;
        this.date = date;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
