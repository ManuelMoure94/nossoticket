package com.rsegrp.nossoticket.ui.movements.mvp;

import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.ui.base.BaseContract;
import com.rsegrp.nossoticket.ui.movements.model.ListAllMovementsModel;

import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public interface MovementsContract {

    interface View extends BaseContract.View {

        void addCard();

        void showProgress();

        void hideProgress();

        void hideProgressUpdate();

        void initAdapterPager();

        void showProgressList();

        void hideProgressList();

        void hidePager();

        void update(List<ListAllMovementsModel> listAllMovementsModels, int position, boolean animated);

        void deleteProgressItem(int position);

//        void isClickable();
    }

    interface Presenter extends BaseContract.Presenter {

        void getTitles(boolean state);

        void refreshMovements(int position);

        void getNextItems(int position);

        void error();

        void retry();

        void onSuccess(Response<List<Titles>> response);

        void onSuccessMovements(Response<Movement> response, int request, boolean isRefresh);

        void onSuccessNextMovements(Response<Movement> response, int position);

        int getSize();

        int getCardAmount(int position);

        String getCardSerial(int position);

        String getCardType(int position);

        String getCardDate(int position);

        String getDescription(int position);

        String getCardId(int position);
    }

    interface Interact extends BaseContract.Interactor {

        void getTitles(MovementsPresenter presenter);

        void getMovements(String cardId, MovementsPresenter presenter, int request, boolean isRefresh);

        void getNextPageMovements(String cardId, final String page, final int position, final MovementsPresenter presenter);
    }
}
