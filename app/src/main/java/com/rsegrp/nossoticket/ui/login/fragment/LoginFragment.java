package com.rsegrp.nossoticket.ui.login.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendMailFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.login.di.LoginModule;
import com.rsegrp.nossoticket.ui.login.mvp.LoginContract;
import com.rsegrp.nossoticket.ui.login.mvp.LoginPresenter;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.register.mvp.RegisterActivity;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class LoginFragment extends BaseFragment implements LoginContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    @Inject
    public LoginPresenter presenter;

    @BindView(R.id.inputPass)
    MaterialEditText inputPass;
    @BindView(R.id.inputMail)
    MaterialEditText inputMail;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.check_remember)
    CheckBox checkRemember;

    @BindView(R.id.forgetPassword)
    TextView forgetPassword;
    @BindView(R.id.sing_up)
    TextView singUp;

    private Intent intent;

    private boolean touch = true;
    private boolean retry = false;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new LoginModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ((LoginHomeActivity) getActivity()).setColorBackground(R.color.colorPrimary);
        ((LoginHomeActivity) getActivity()).hideArrow();

        boolean state = ((LoginHomeActivity) getActivity()).loadPreferencesState();
        if (state) {
            checkRemember.setChecked(true);
            setEmail(((LoginHomeActivity) getActivity()).loadPreferencesEmail());
        }

        if (touch) {
            checkBtn();
        }
    }

    private void checkBtn() {

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnLogin();
            }
        });

        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.forgetPassword();
            }
        });

        singUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sinUp();
            }
        });
    }

    @Override
    public String getEmail() {
        return inputMail.getText().toString();
    }

    @Override
    public String getPassword() {
        return inputPass.getText().toString();
    }

    @Override
    public void setEmail(String email) {
        inputMail.setText(email);
    }

    @Override
    public void setPassword(String password) {
        inputPass.setText(password);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(App.getInstance().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public void singUp() {
        intent = new Intent(getActivity(), RegisterActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.fab_scale_up, R.anim.fab_scale_down);
        getActivity().finish();
    }

    @Override
    public void home() {

        if (checkRemember.isChecked()) {
            ((LoginHomeActivity) getActivity()).saveUser(true, getEmail());
        } else {
            ((LoginHomeActivity) getActivity()).saveUser(false, "");
        }

        intent = new Intent(getActivity(), HomeActivity.class);
        ((LoginHomeActivity) getActivity()).savePreferences(presenter.getAccessToken());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.fab_scale_up, R.anim.fab_scale_down);
        getActivity().finish();
    }

    @Override
    public void forgetFragment() {

        Fragment fragment = new SendMailFragment();
        ((LoginHomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        ((LoginHomeActivity) getActivity()).showProgress();
        touch = false;
    }

    @Override
    public void hideProgress() {

        ((LoginHomeActivity) getActivity()).hideProgress();
        touch = true;
    }

    @Override
    public void retryPopup() {

        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
        hideProgress();
    }

    @Override
    public void failLogin() {

        new DialogsOneBtn(this, getString(R.string.fail), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
    }

    public void onPositiveButtonClick() {
        presenter.btnLogin();
    }

    public void onNegativeButtonClick() {
        hideProgress();
        touch = true;
    }

//    private AlertDialog retryDialog() {
//
//        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View view = inflater.inflate(R.layout.dialog_alert_connection_error, null);
//
//        builder.setView(view);
//
//        Button btnCancel = view.findViewById(R.id.btb_cancel);
//        Button btnRetry = view.findViewById(R.id.btn_retry);
//
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("TAG", "onClick: cancel");
////                onNegativeButtonClick();
//            }
//        });
//
//        btnRetry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("TAG", "onClick: retry");
////                onPositiveButtonClick();
//                retryDialog().dismiss();
//            }
//        });
////
////        builder.setTitle(App.getInstance().getString(R.string.title_failure))
////                .setMessage(App.getInstance().getString(R.string.message_failure))
////                .setPositiveButton(App.getInstance().getString(R.string.retry), new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        onPositiveButtonClick();
////                    }
////                })
////                .setNegativeButton(App.getInstance().getString(R.string.cancel), new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        onNegativeButtonClick();
////                    }
////                });
////
//        return builder.create();
//    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                onNegativeButtonClick();
                break;
            case "retry":
                onPositiveButtonClick();
                break;
            case "accept":
                onNegativeButtonClick();
                break;
        }
    }
}