package com.rsegrp.nossoticket.ui.user.mvp.update;

import android.text.TextUtils;
import android.util.Log;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class UpdatePresenter implements UpdateContract.Presenter {

    private UpdateContract.View view;
    private UpdateContract.Interact interact;

    private DataUser dataUser;

    public UpdatePresenter(UpdateContract.View view, UpdateContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnUpdate() {

        if (checkedField()) {
            view.showProgress();
            dataUser = view.getDataUser();
            interact.updateUser(setData(dataUser), this);
        }
    }

    @Override
    public void onSuccess(Response<DataUser> response) {

        view.hideProgress();
        view.onSuccess();
    }

    @Override
    public void onError() {
        view.hideProgress();
        view.onError();
    }

    @Override
    public void onFailed() {
        view.hideProgress();
        view.onFailed();
    }

    private boolean checkedField() {
        return checkFName() && checkLName() && checkAddress()
                && checkDate() && checkPhone() && checkId();
    }

    private boolean checkFName() {

        if (TextUtils.isEmpty(view.getFName())) {
            view.showError(App.getInstance().getString(R.string.field_fName_empty));
            return false;
        } else return true;
    }

    private boolean checkLName() {

        if (TextUtils.isEmpty(view.getLName())) {
            view.showError(App.getInstance().getString(R.string.field_lName_empty));
            return false;
        } else return true;
    }

    private boolean checkAddress() {

        if (TextUtils.isEmpty(view.getAddress())) {
            view.showError(App.getInstance().getString(R.string.address_empty));
            return false;
        } else return true;
    }

    private boolean checkDate() {

        if (TextUtils.isEmpty(view.getDate())) {
            view.showError(App.getInstance().getString(R.string.date_empty));
            return false;
        } else return true;
    }

    private boolean checkPhone() {

        if (TextUtils.isEmpty(view.getPhone())) {
            view.showError(App.getInstance().getString(R.string.field_phone_empty));
            return false;
        } else return true;
    }

    private boolean checkId() {

        if (TextUtils.isEmpty(view.getIdUser())) {
            view.showError(App.getInstance().getString(R.string.field_id_empty));
            return false;
        } else return true;
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }

    private DataUser setData(DataUser data) {
        data.setName(view.getFName());
        data.setLastName(view.getLName());
        data.setAddress(view.getAddress());
        data.setBirthDate(view.getDate());
        data.setIdentification(view.getIdUser());
        data.setPhone(view.getPhone());

        return data;
    }
}
