package com.rsegrp.nossoticket.ui.title.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.title.model.TitleModel;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class TitleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements OnClick.OnClickHolder {

    private List<TitleModel> items;
    private OnClick.OnClickAdapter onClickAdapter;
    private Context context;

    public TitleAdapter(Context context, List<TitleModel> items, final OnClick.OnClickAdapter onClickAdapter) {
        this.context = context;
        this.items = items;
        this.onClickAdapter = onClickAdapter;
    }

    public class TitleViewHolder extends RecyclerView.ViewHolder {

        TextView cardSerial;
        TextView cardAmount;
        TextView cardDate;
        TextView cardOptions;

        public TitleViewHolder(View itemView, final OnClick.OnClickHolder clickHolder) {
            super(itemView);

            cardSerial = itemView.findViewById(R.id.text_card_serial);
            cardAmount = itemView.findViewById(R.id.text_card_balance_movement);
            cardDate = itemView.findViewById(R.id.text_card_date);
            cardOptions = itemView.findViewById(R.id.card_options);

            cardOptions.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(View v) {

                    Log.i("TAG", "onClick: card");

                    PopupMenu popupMenu = new PopupMenu(context, cardOptions);
                    popupMenu.inflate(R.menu.menu_card_options);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            int position = getAdapterPosition();

                            switch (item.getItemId()) {

                                case R.id.movements:
                                    if (position != RecyclerView.NO_POSITION) {
                                        clickHolder.onClickDetails(position);
                                    }
                                    break;

                                case R.id.recharging:
                                    //if (position != RecyclerView.NO_POSITION) {
                                    //    clickHolder.onClickRecharge(position);
                                    //}
                                    Toast.makeText(context, "Proximamente", Toast.LENGTH_SHORT).show();
                                    break;

                                case R.id.disaffiliate:
                                    Toast.makeText(context, "Proximamente", Toast.LENGTH_SHORT).show();
                                    break;

                                case R.id.block:
                                    if (position != RecyclerView.NO_POSITION) {
                                        clickHolder.onClickBlock(position);
                                    }
                                    break;
                            }
                            return false;
                        }
                    });

                    MenuPopupHelper menuHelper = new MenuPopupHelper(context, (MenuBuilder) popupMenu.getMenu(),
                            cardOptions);
                    menuHelper.setForceShowIcon(true);
                    menuHelper.show();
                }
            });

            /**iconRecharge.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
            clickHolder.onClickRecharge(position);
            }
            }
            });

             iconDetails.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
            clickHolder.onClickDetails(position);
            }
            }
            });

             iconBlock.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
            clickHolder.onClickBlock(position);
            }
            }
            });*/
        }
    }

    public class noTitle extends RecyclerView.ViewHolder {

        public noTitle(View itemView) {
            super(itemView);
        }
    }

    public class CarnetViewHolder extends RecyclerView.ViewHolder {

        TextView balanceCarnet;
        TextView serialCarnet;
        TextView codeCarnet;
        TextView cdlpOptions;

        public CarnetViewHolder(View itemView, final OnClick.OnClickHolder clickHolder) {
            super(itemView);

            balanceCarnet = itemView.findViewById(R.id.balance_amount);
            serialCarnet = itemView.findViewById(R.id.serial_carnet);
            codeCarnet = itemView.findViewById(R.id.code_carnet);
            cdlpOptions = itemView.findViewById(R.id.cdlp_options);

            cdlpOptions.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(View v) {

                    Log.i("TAG", "onClick: canet");

                    PopupMenu popupMenu = new PopupMenu(context, cdlpOptions);
                    popupMenu.inflate(R.menu.menu_card_options);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            int position = getAdapterPosition();

                            switch (item.getItemId()) {

                                case R.id.movements:
                                    if (position != RecyclerView.NO_POSITION) {
                                        clickHolder.onClickDetails(position);
                                    }
                                    break;

                                case R.id.recharging:
                                    if (position != RecyclerView.NO_POSITION) {
                                        clickHolder.onClickRecharge(position);
                                    }
                                    break;

                                case R.id.disaffiliate:
                                    Toast.makeText(context, "Proximamente", Toast.LENGTH_SHORT).show();
                                    break;

                                case R.id.block:
                                    if (position != RecyclerView.NO_POSITION) {
                                        clickHolder.onClickBlock(position);
                                    }
                                    break;
                            }
                            return false;
                        }
                    });

                    MenuPopupHelper menuHelper = new MenuPopupHelper(context, (MenuBuilder) popupMenu.getMenu(),
                            cdlpOptions);
                    menuHelper.setForceShowIcon(true);
                    menuHelper.show();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getCardType().equals("1001")) {
            return 1;
        } else if (items.get(position).getCardType().equals(Constant.NO_TITLE)) {
            return 2;
        } else return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_carnet, parent, false);

            return new CarnetViewHolder(view, this);
        } else if (viewType == 2) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_no_title, parent, false);

            return new noTitle(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_card, parent, false);

            return new TitleViewHolder(view, this);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof TitleViewHolder) {

            TitleViewHolder titleViewHolder = (TitleViewHolder) holder;
            titleViewHolder.cardSerial.setText(items.get(position).getCardSerial());
            titleViewHolder.cardAmount.setText(items.get(position).getCardAmount());
            titleViewHolder.cardDate.setText(items.get(position).getCardDate());

        } else if (holder instanceof CarnetViewHolder) {

            CarnetViewHolder carnetViewHolder = (CarnetViewHolder) holder;
            carnetViewHolder.balanceCarnet.setText(items.get(position).getCardAmount());
            carnetViewHolder.serialCarnet.setText(items.get(position).getCardSerial());
            carnetViewHolder.codeCarnet.setText(items.get(position).getCardDate());
        }
        setFadeAnimation(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClickRecharge(int position) {
        onClickAdapter.onClickRecharge(position, items.get(position).getCardId(), items.get(position).getCardSerial(),
                items.get(position).getCardAmount(), items.get(position).getCardDate());
    }

    @Override
    public void onClickDetails(int position) {
        onClickAdapter.onClickDetails(position, items.get(position).getCardId(), items.get(position).getCardSerial(),
                items.get(position).getCardAmount(), items.get(position).getCardDate());
    }

    @Override
    public void onClickBlock(int position) {
        onClickAdapter.onClickBlock(position, items.get(position).getCardId(), items.get(position).getCardSerial(),
                items.get(position).getCardAmount(), items.get(position).getCardDate());
    }

    private void setFadeAnimation(View view) {

        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(Constant.FADE_DURATION);
        view.startAnimation(animation);
    }
}
