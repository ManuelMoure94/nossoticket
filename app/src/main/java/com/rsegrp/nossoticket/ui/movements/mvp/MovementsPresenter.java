package com.rsegrp.nossoticket.ui.movements.mvp;

import android.util.Log;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.api.data.titles.Titles;
import com.rsegrp.nossoticket.ui.movements.model.ListAllMovementsModel;
import com.rsegrp.nossoticket.ui.movements.model.ListMovementsModel;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public class MovementsPresenter implements MovementsContract.Presenter {

    private MovementsContract.View view;
    private MovementsContract.Interact interact;

    private Response<List<Titles>> response;

    private List<ListAllMovementsModel> listAllMovementsModels = new ArrayList<>();

    public MovementsPresenter(MovementsContract.View view, MovementsContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void getTitles(boolean state) {

        if (state) {
            view.showProgress();
        }
        interact.getTitles(this);
    }

    @Override
    public void refreshMovements(int position) {
        interact.getMovements(getCardSerial(position), this, position, true);
    }

    @Override
    public void getNextItems(int position) {
        interact.getNextPageMovements(listAllMovementsModels.get(position).getCardId(),
                String.valueOf(listAllMovementsModels.get(position).getPage()), position, this);
    }

    @Override
    public void onSuccess(Response<List<Titles>> response) {

        if (response != null) {

            if (response.body().size() != 0) {
                this.response = response;
                view.hideProgress();
                view.initAdapterPager();
                interact.getMovements(response.body().get(0).getMaCardId(), this, 0, false);
                view.showProgressList();
            } else {
                view.hideProgress();
                view.hidePager();
                emptyDataCard();
                view.update(listAllMovementsModels, 0, false);
            }
        }
    }

    @Override
    public void error() {

    }

    @Override
    public void retry() {

    }

    @Override
    public void onSuccessMovements(Response<Movement> response, int request, boolean isRefresh) {

        Log.i("TAG", "onSuccessMovements: " + request);

        if (response != null) {
            if (request == getSize() && !isRefresh) {
                saveDataListMovements(response, request);
                view.hideProgressList();
                view.update(listAllMovementsModels, 0, false);
            } else if (isRefresh) {
                view.hideProgressUpdate();
                listAllMovementsModels.get(request - 1).getListMovementsModels().clear();
                listAllMovementsModels.get(request - 1).getListMovementsModels().addAll(getData(response));
                view.update(listAllMovementsModels, request - 1, false);
            } else {
                saveDataListMovements(response, request);
                interact.getMovements(this.response.body().get(request).getMaCardId(), this, request, false);
            }
        }
    }

    @Override
    public void onSuccessNextMovements(Response<Movement> response, int position) {

        if (response != null) {
            view.deleteProgressItem(position);

            listAllMovementsModels.get(position).setPage(response.body().getNextPageIndex());
            listAllMovementsModels.get(position).getListMovementsModels().addAll(getData(response));

            view.hideProgressUpdate();
            view.update(listAllMovementsModels, position, false);

            Log.i("TAG", "onSuccessNextMovements: " + listAllMovementsModels.get(position).getPage());
        }
    }

    @Override
    public int getSize() {

        if (response != null) {
            return response.body().size();
        } else return 0;
    }

    @Override
    public int getCardAmount(int position) {

        if (response != null) {
            return response.body().get(position).getRemaining();
        } else return 0;
    }

    @Override
    public String getCardSerial(int position) {

        if (response != null) {
            return response.body().get(position).getMaCardId();
        } else return null;
    }

    @Override
    public String getCardType(int position) {

        if (response != null) {
            return response.body().get(position).getTittleType();
        } else return null;
    }

    @Override
    public String getCardDate(int position) {

        if (response != null) {
            String year = response.body().get(position).getCreatedAt().substring(0, 4);
            String month = response.body().get(position).getCreatedAt().substring(5, 7);
            String day = response.body().get(position).getCreatedAt().substring(8, 10);
            return day + "/" + month + "/" + year;
        } else return null;
    }

    @Override
    public String getDescription(int position) {

        if (response != null) {
            return response.body().get(position).getDescription();
        } else return null;
    }

    @Override
    public String getCardId(int position) {

        if (response != null) {
            return response.body().get(position).getCardId();
        } else return null;
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }

    private List<ListMovementsModel> getData(Response<Movement> response) {

        List<ListMovementsModel> list = new ArrayList<>();
        for (int i = 0; i < response.body().getData().size(); i++) {

            if (response.body().getData().get(i).getAction().equals("REC")) {
                list.add(new ListMovementsModel(response.body().getData().get(i).getAction(),
                        App.getInstance().getBaseContext().getString(R.string.balance),
                        response.body().getData().get(i).getMoment().substring(8, 10) + "/" +
                                response.body().getData().get(i).getMoment().substring(5, 7) + "/" +
                                response.body().getData().get(i).getMoment().substring(0, 4),
                        response.body().getData().get(i).getFacial()));
            } else if (response.body().getData().get(i).getAction().equals("TRA")) {
                list.add(new ListMovementsModel(response.body().getData().get(i).getAction(),
                        response.body().getData().get(i).getStation(),
                        response.body().getData().get(i).getMoment().substring(8, 10) + "/" +
                                response.body().getData().get(i).getMoment().substring(5, 7) + "/" +
                                response.body().getData().get(i).getMoment().substring(0, 4),
                        response.body().getData().get(i).getFacial()));
            }
        }

        if (response.body().getNextPageIndex() == -1) {
            list.add(new ListMovementsModel(Constant.TYPE_ITEM_NO_ITEM, "", "", ""));
        }

        return list;
    }

    private void saveDataListMovements(Response<Movement> response, int request) {

        listAllMovementsModels.add(new ListAllMovementsModel(getCardSerial(request - 1),
                response.body().getNextPageIndex(), getData(response)));
    }

    private void emptyDataCard() {
        listAllMovementsModels.add(new ListAllMovementsModel("",
                0, getDataEmpty()));
    }

    public List<ListMovementsModel> getDataEmpty() {

        List<ListMovementsModel> list = new ArrayList<>();
        list.add(new ListMovementsModel(Constant.TYPE_ITEM_EMPTY_ITEM, "", "", ""));

        return list;
    }
}
