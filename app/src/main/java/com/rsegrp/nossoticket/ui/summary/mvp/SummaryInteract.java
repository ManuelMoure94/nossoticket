package com.rsegrp.nossoticket.ui.summary.mvp;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.summary.Summary;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.SummaryServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

public class SummaryInteract implements SummaryContract.Interact {

    private SummaryServices summaryServices;
    private SingInServices singInServices;

    public SummaryInteract(SummaryServices summaryServices, SingInServices singInServices) {
        this.summaryServices = summaryServices;
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getSummary(final SummaryPresenter presenter) {

        Call<List<Summary>> call = summaryServices.getSummary(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<List<Summary>>() {
            @Override
            public void onResponse(Call<List<Summary>> call, Response<List<Summary>> response) {

                if (response.isSuccessful()) {
                    presenter.isSuccess(response);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<List<Summary>> call, Throwable t) {

                presenter.retry();
            }
        });
    }

    @Override
    public void getUser(final SummaryPresenter presenter) {

        Call<DataUser> call = singInServices.getUser(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    Log.i("TAG", "onResponse: " + response.body().getLockedBalance());
                    presenter.isSuccessUser(response);
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

            }
        });
    }
}
