package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendMailPresenter implements SendMailContract.Presenter {

    private SendMailContract.View view;
    private SendMailContract.Interact interact;

    public SendMailPresenter(SendMailContract.View view, SendMailContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnNext() {

        if (checkEmail()) {
            view.showProgress();
            interact.sendMail(view.getEmail(), this);
        }
    }

    @Override
    public void onSuccess(Response<String> response) {

        if (response != null) {
            view.hideProgress();
            view.showMsgSuccess("");
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFailed() {

    }

    private boolean checkEmail() {

        if (TextUtils.isEmpty(view.getEmail())) {
            view.showError(App.getInstance().getString(R.string.field_mail_empty));
            return false;
        } else {
            if (checkFormatEmail()) {
                return true;
            } else {
                view.showError(App.getInstance().getString(R.string.invalid_mail));
                return false;
            }
        }
    }

    private boolean checkFormatEmail() {

        String emailPattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(view.getEmail());

        return matcher.matches();
    }
}
