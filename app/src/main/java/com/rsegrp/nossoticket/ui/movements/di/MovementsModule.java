package com.rsegrp.nossoticket.ui.movements.di;

import com.rsegrp.nossoticket.api.services.MovementServices;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.movements.fragment.MovementsFragment;
import com.rsegrp.nossoticket.ui.movements.mvp.MovementsInteract;
import com.rsegrp.nossoticket.ui.movements.mvp.MovementsPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

@Module
public class MovementsModule {

    private MovementsFragment fragment;

    public MovementsModule(MovementsFragment fragment) {
        this.fragment = fragment;
    }

    @MovementsScope
    @Provides
    public MovementsFragment providesMovementsFragment() {
        return fragment;
    }

    @MovementsScope
    @Provides
    public MovementsPresenter providesMovementsPresenter(MovementsInteract interact) {
        return new MovementsPresenter(fragment, interact);
    }

    @MovementsScope
    @Provides
    public MovementsInteract providesMovementsInteract(TitleServices titleServices, MovementServices movementServices) {
        return new MovementsInteract(titleServices, movementServices);
    }
}
