package com.rsegrp.nossoticket.ui.express.mvp.addNewTicket;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class AddNewTicketInteract implements AddNewTicketContract.Interact {

    private SingInServices singInServices;

    public AddNewTicketInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
    }

    @Override
    public void getUser(final AddNewTicketPresenter presenter) {

        Call<DataUser> call = singInServices.getUser(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    presenter.isSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

//                presenter.showMessage(t.getMessage());
            }
        });
    }
}
