package com.rsegrp.nossoticket.ui.home.mvp;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

public class HomeInteract implements HomeContract.Interact {

    private SingInServices singInServices;

    public HomeInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void getUser(final HomePresenter presenter) {

        Call<DataUser> call = singInServices.getUser(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    presenter.isSuccess(response);
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

//                presenter.showMessage(t.getMessage());
            }
        });
    }
}
