package com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard;

import com.rsegrp.nossoticket.api.data.titles.AddTitle;
import com.rsegrp.nossoticket.api.data.titles.NewTitle;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class AddCardInteract implements AddCardContract.Interact {

    private TitleServices titleServices;

    public AddCardInteract(TitleServices titleServices) {
        this.titleServices = titleServices;
    }

    @Override
    public void addTitle(NewTitle newTitle, final AddCardPresenter presenter) {

        Call<AddTitle> call = titleServices.addTitle(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), newTitle);

        call.enqueue(new Callback<AddTitle>() {
            @Override
            public void onResponse(Call<AddTitle> call, Response<AddTitle> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<AddTitle> call, Throwable t) {

                presenter.retry();
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
            }
        });
    }
}
