package com.rsegrp.nossoticket.ui.recharge.constant;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class Constant {

    public static final String ARGS_METHOD = "method";
    public static final String ARGS_TARGET = "target";
    public static final String ARGS_ORIGIN = "origin";
    public static final String ARGS_BALANCE = "balance";
}
