package com.rsegrp.nossoticket.ui.user.di.changePw;

import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.user.fragment.NewPasswordFragment;
import com.rsegrp.nossoticket.ui.user.mvp.changePw.NewPasswordInteract;
import com.rsegrp.nossoticket.ui.user.mvp.changePw.NewPasswordPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Module
public class NewPasswordModule {

    private NewPasswordFragment newPasswordFragment;

    public NewPasswordModule(NewPasswordFragment newPasswordFragment) {
        this.newPasswordFragment = newPasswordFragment;
    }

    @Provides
    @NewPasswordScope
    public NewPasswordFragment providesNewPasswordFragment() {
        return newPasswordFragment;
    }

    @Provides
    @NewPasswordScope
    public NewPasswordPresenter providesNewPasswordPresenter(NewPasswordInteract interact) {
        return new NewPasswordPresenter(newPasswordFragment, interact);
    }

    @Provides
    @NewPasswordScope
    public NewPasswordInteract providesNewPasswordInteract(SingInServices singInServices) {
        return new NewPasswordInteract(singInServices);
    }
}
