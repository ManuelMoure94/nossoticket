package com.rsegrp.nossoticket.ui.register.mvp;

import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.api.data.singUp.Register;
import com.rsegrp.nossoticket.api.data.singUp.SingUp;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.SingUpServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class RegisterInteract implements RegisterContract.Interact {

    private SingUpServices singUpServices;
    private SingInServices singInServices;

    public RegisterInteract(SingUpServices singUpServices, SingInServices singInServices) {
        this.singUpServices = singUpServices;
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void newUser(final Register register, final RegisterPresenter presenter) {

        Call<SingUp> call = singUpServices.SingUp(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, register);

        call.enqueue(new Callback<SingUp>() {
            @Override
            public void onResponse(Call<SingUp> call, Response<SingUp> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccessSingUn(response);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<SingUp> call, Throwable t) {

                presenter.retry();
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
//                presenter.hideProgress();
            }
        });
    }

    @Override
    public void SingInNewUser(final User user, final RegisterPresenter presenter) {

        Call<SingIn> call = singInServices.singIn(Constant.CONTENT_TYPE, Constant.CLIENT,
                Constant.SECRET, user);

        call.enqueue(new Callback<SingIn>() {
            @Override
            public void onResponse(Call<SingIn> call, Response<SingIn> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccessSingIp(response);
                }
            }

            @Override
            public void onFailure(Call<SingIn> call, Throwable t) {

                presenter.errorConnection();
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
//                presenter.hideProgress();
            }
        });
    }
}
