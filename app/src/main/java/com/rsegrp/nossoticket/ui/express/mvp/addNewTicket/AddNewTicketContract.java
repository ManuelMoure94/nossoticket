package com.rsegrp.nossoticket.ui.express.mvp.addNewTicket;

import android.support.v4.app.Fragment;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public interface AddNewTicketContract {

    interface View extends BaseContract.View {

        void nextFragment(Fragment fragment);

        void setRemaining(double remaining);
    }

    interface Presenter extends BaseContract.Presenter{

        void getUser();

        void btnAddTicket();

        void btnExistingTickets();

        void isSuccess(Response<DataUser> response);
    }

    interface Interact {

        void getUser(AddNewTicketPresenter presenter);
    }
}
