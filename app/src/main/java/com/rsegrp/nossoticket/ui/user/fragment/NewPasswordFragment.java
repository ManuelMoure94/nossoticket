package com.rsegrp.nossoticket.ui.user.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.ui.user.di.changePw.NewPasswordModule;
import com.rsegrp.nossoticket.ui.user.mvp.changePw.NewPasswordContract;
import com.rsegrp.nossoticket.ui.user.mvp.changePw.NewPasswordPresenter;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewPasswordFragment extends BaseFragment implements NewPasswordContract.View,
        DialogsOneBtn.OnSetClickListener, DialogsTwoBtn.OnSetClickListener {

    @Inject
    NewPasswordPresenter presenter;

    @BindView(R.id.text_pw)
    MaterialEditText textPassword;
    @BindView(R.id.text_new_pw)
    MaterialEditText textNewPassword;
    @BindView(R.id.text_new_rePw)
    MaterialEditText textNewRePassword;

    @BindView(R.id.progress_new_password)
    RelativeLayout rlProgress;

    @BindView(R.id.back_pass)
    ImageView backPass;

    @BindView(R.id.btn_new_pass)
    Button btnNewPass;

    private String type;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new NewPasswordModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pass, container, false);
        ButterKnife.bind(this, view);

        ((HomeActivity) getActivity()).hideToolbar();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkedBtn();
    }

    private void checkedBtn() {

        btnNewPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnNewPassword();
            }
        });

        backPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextFragment();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public String getPassword() {
        return textPassword.getText().toString();
    }

    @Override
    public String getNewPassword() {
        return textNewPassword.getText().toString();
    }

    @Override
    public String getNewRePassword() {
        return textNewRePassword.getText().toString();
    }

    @Override
    public void hideProgress() {
        rlProgress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        rlProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void nextFragment() {

        ((HomeActivity) getActivity()).showToolbar();
        Fragment fragment = new SummaryFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void onSuccess() {
        type = Constant.SUCCESS;
        new DialogsOneBtn(this, getString(R.string.update_user), getString(R.string.accept)).show(getChildFragmentManager(), "OnSuccess");
    }

    @Override
    public void onError() {
        type = Constant.ERROR;
        new DialogsOneBtn(this, getString(R.string.error_check_token), getString(R.string.accept)).show(getChildFragmentManager(), "OnSuccess");
    }

    @Override
    public void onFailed() {
        type = Constant.FAILED;
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void clickListener(String options) {

        switch (type) {

            case Constant.SUCCESS:
                nextFragment();
                break;

            case Constant.FAILED:

                if (options.equals("retry")) {
                    presenter.btnNewPassword();
                }
                break;

            case Constant.ERROR:
                break;
        }
    }
}
