package com.rsegrp.nossoticket.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.WindowManager;

import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;

/**
 * Created by RSE_VZLA_07 on 6/2/2018.
 */

public abstract class BaseFragment extends Fragment {

    protected abstract void injectDependencies(ApplicationComponent applicationComponent);

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        injectDependencies(App.getInstance().getAppComponent());
    }
}
