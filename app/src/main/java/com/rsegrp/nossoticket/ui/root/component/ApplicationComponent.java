package com.rsegrp.nossoticket.ui.root.component;

import com.rsegrp.nossoticket.api.ApiModule;
import com.rsegrp.nossoticket.ui.express.di.addNewTicket.AddNewTicketComponent;
import com.rsegrp.nossoticket.ui.express.di.addNewTicket.AddNewTicketModule;
import com.rsegrp.nossoticket.ui.express.di.receiptTicket.TicketReceiptComponent;
import com.rsegrp.nossoticket.ui.express.di.receiptTicket.TicketReceiptModule;
import com.rsegrp.nossoticket.ui.express.di.showTicket.ShowTicketComponent;
import com.rsegrp.nossoticket.ui.express.di.showTicket.ShowTicketModule;
import com.rsegrp.nossoticket.ui.express.di.ticketList.TicketListComponent;
import com.rsegrp.nossoticket.ui.express.di.ticketList.TicketListModule;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendcode.SendCodeComponent;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendcode.SendCodeModule;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendmail.SendMailComponent;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendmail.SendMailModule;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword.SendPassComponent;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword.SendPassModule;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail.SendMailInteract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword.SendPassInteract;
import com.rsegrp.nossoticket.ui.home.di.HomeComponent;
import com.rsegrp.nossoticket.ui.home.di.HomeModule;
import com.rsegrp.nossoticket.ui.home.mvp.HomeInteract;
import com.rsegrp.nossoticket.ui.login.di.LoginComponent;
import com.rsegrp.nossoticket.ui.login.di.LoginModule;
import com.rsegrp.nossoticket.ui.login.mvp.LoginInteract;
import com.rsegrp.nossoticket.ui.loginhome.di.LoginHomeComponent;
import com.rsegrp.nossoticket.ui.loginhome.di.LoginHomeModule;
import com.rsegrp.nossoticket.ui.movements.di.MovementsComponent;
import com.rsegrp.nossoticket.ui.movements.di.MovementsModule;
import com.rsegrp.nossoticket.ui.movements.mvp.MovementsInteract;
import com.rsegrp.nossoticket.ui.recharge.di.receipt.ReceiptComponent;
import com.rsegrp.nossoticket.ui.recharge.di.receipt.ReceiptModule;
import com.rsegrp.nossoticket.ui.recharge.di.recharge.RechargeComponent;
import com.rsegrp.nossoticket.ui.recharge.di.recharge.RechargeModule;
import com.rsegrp.nossoticket.ui.recharge.mvp.receipt.ReceiptInteract;
import com.rsegrp.nossoticket.ui.register.di.RegisterComponent;
import com.rsegrp.nossoticket.ui.register.di.RegisterModule;
import com.rsegrp.nossoticket.ui.register.mvp.RegisterInteract;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.module.ApplicationModule;
import com.rsegrp.nossoticket.ui.settingsCard.di.addCard.AddCardComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.addCard.AddCardModule;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardBlock.CardBlockComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardBlock.CardBlockModule;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails.CardDetailsComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails.CardDetailsModule;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardRecharge.CardRechargeComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardRecharge.CardRechargeModule;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock.CardBlockInteract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge.CardRechargeInteract;
import com.rsegrp.nossoticket.ui.summary.di.SummaryComponent;
import com.rsegrp.nossoticket.ui.summary.di.SummaryModule;
import com.rsegrp.nossoticket.ui.summary.mvp.SummaryInteract;
import com.rsegrp.nossoticket.ui.title.di.TitleComponent;
import com.rsegrp.nossoticket.ui.title.di.TitleModule;
import com.rsegrp.nossoticket.ui.title.mvp.TitleInteract;
import com.rsegrp.nossoticket.ui.user.di.changePw.NewPasswordComponent;
import com.rsegrp.nossoticket.ui.user.di.changePw.NewPasswordModule;
import com.rsegrp.nossoticket.ui.user.di.profile.ProfileComponent;
import com.rsegrp.nossoticket.ui.user.di.profile.ProfileModule;
import com.rsegrp.nossoticket.ui.user.di.update.UpdateComponent;
import com.rsegrp.nossoticket.ui.user.di.update.UpdateModule;
import com.rsegrp.nossoticket.ui.user.mvp.changePw.NewPasswordInteract;
import com.rsegrp.nossoticket.ui.user.mvp.profile.ProfileInteract;
import com.rsegrp.nossoticket.ui.user.mvp.update.UpdateInteract;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by RSE_VZLA_07 on 2/2/2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {

    void inject(App app);

    void inject(LoginInteract loginInteract);

    void inject(HomeInteract homeInteract);

    void inject(SummaryInteract summaryInteract);

    void inject(RegisterInteract registerInteract);

    void inject(TitleInteract titleInteract);

    void inject(ProfileInteract profileInteract);

    void inject(CardBlockInteract cardBlockInteract);

    void inject(ReceiptInteract receiptInteract);

    void inject(CardRechargeInteract cardRechargeInteract);

    void inject(MovementsInteract movementsInteract);

    void inject(SendMailInteract sendMailInteract);

    void inject(SendPassInteract sendPassInteract);

    void inject(UpdateInteract updateInteract);

    void inject(NewPasswordInteract newPasswordInteract);

    MovementsComponent plus(MovementsModule movementsModule);

    LoginHomeComponent plus(LoginHomeModule loginHomeModule);

    LoginComponent plus(LoginModule loginModule);

    RegisterComponent plus(RegisterModule registerModule);

    SendMailComponent plus(SendMailModule sendMailModule);

    SendCodeComponent plus(SendCodeModule sendCodeModule);

    SendPassComponent plus(SendPassModule sendPassModule);

    HomeComponent plus(HomeModule homeModule);

    SummaryComponent plus(SummaryModule summaryModule);

    TitleComponent plus(TitleModule titleModule);

    RechargeComponent plus(RechargeModule rechargeModule);

    ReceiptComponent plus(ReceiptModule receiptModule);

    ProfileComponent plus(ProfileModule profileModule);

    UpdateComponent plus(UpdateModule updateModule);

    NewPasswordComponent plus(NewPasswordModule newPasswordModule);

    CardRechargeComponent plus(CardRechargeModule cardRechargeModule);

    CardDetailsComponent plus(CardDetailsModule cardDetailsModule);

    CardBlockComponent plus(CardBlockModule cardBlockModule);

    AddCardComponent plus(AddCardModule addCardModule);

    AddNewTicketComponent plus(AddNewTicketModule addNewTicketModule);

    TicketReceiptComponent plus(TicketReceiptModule ticketReceiptModule);

    TicketListComponent plus(TicketListModule ticketListModule);

    ShowTicketComponent plus(ShowTicketModule showTicketModule);
}