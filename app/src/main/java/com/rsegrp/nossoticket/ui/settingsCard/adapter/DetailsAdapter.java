package com.rsegrp.nossoticket.ui.settingsCard.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.settingsCard.model.DetailsModel;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class DetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DetailsModel> items;

    public DetailsAdapter() {
    }

    public void updateListItem(List<DetailsModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder {

        TextView line;
        TextView station;
        TextView date;
        TextView action;
        TextView amount;
        TextView finalBalance;

        public DetailsViewHolder(View itemView) {
            super(itemView);

            line = itemView.findViewById(R.id.details_line);
            station = itemView.findViewById(R.id.details_station);
            date = itemView.findViewById(R.id.details_date);
            action = itemView.findViewById(R.id.details_action);
            amount = itemView.findViewById(R.id.details_amount);
            finalBalance = itemView.findViewById(R.id.details_final_balance);
        }
    }

    public class ProgressItemViewHolder extends RecyclerView.ViewHolder {

        public ProgressItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class NoDataItemViewHolder extends RecyclerView.ViewHolder {

        public NoDataItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == Constant.ITEM_TYPE_PROGRESS) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progress, parent, false);

            return new ProgressItemViewHolder(view);
        } else if (viewType == Constant.ITEM_TYPE_END) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_no_data, parent, false);

            return new NoDataItemViewHolder(view);
        } else {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_card_detail, parent, false);

            return new DetailsViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DetailsViewHolder) {

            DetailsViewHolder detailsViewHolder = (DetailsViewHolder) holder;
            detailsViewHolder.line.setText(items.get(position).getLine());
            detailsViewHolder.station.setText(items.get(position).getStation());
            detailsViewHolder.date.setText(items.get(position).getDate());
            detailsViewHolder.action.setText(items.get(position).getAction());
            detailsViewHolder.amount.setText(items.get(position).getAmount());
            detailsViewHolder.finalBalance.setText(items.get(position).getFinalBalance());
        }

//        setFadeAnimation(holder.itemView);

        if ((position % 2) == 0)
            holder.itemView.setBackgroundResource(R.color.whiteTransparent);
        else
            holder.itemView.setBackgroundResource(R.color.backgroundMenuExpandable);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (items.get(position).getItemType() == Constant.ITEM_TYPE_PROGRESS) {
            return Constant.ITEM_TYPE_PROGRESS;
        } else if (items.get(position).getItemType() == Constant.ITEM_TYPE_END) {
            return Constant.ITEM_TYPE_END;
        } else return Constant.ITEM_TYPE_DATA;
    }

    private void setFadeAnimation(View view) {

        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(Constant.FADE_DURATION);
        view.startAnimation(animation);
    }
}
