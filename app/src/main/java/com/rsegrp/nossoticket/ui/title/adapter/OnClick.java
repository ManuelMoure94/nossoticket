package com.rsegrp.nossoticket.ui.title.adapter;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface OnClick {

    interface OnClickAdapter {

        void onClickRecharge(int position, String cardId, String serial, String balance, String date);
        void onClickDetails(int position, String cardId, String serial, String balance, String date);
        void onClickBlock(int position, String cardId, String serial, String balance, String date);
    }

    interface OnClickHolder {

        void onClickRecharge(int position);
        void onClickDetails(int position);
        void onClickBlock(int position);
    }
}
