package com.rsegrp.nossoticket.ui.settingsCard.fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.cardBlock.CardBlockModule;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock.CardBlockContract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock.CardBlockPresenter;
import com.rsegrp.nossoticket.ui.title.fragment.TitleFragment;
import com.rsegrp.nossoticket.utils.constants.Constant;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardBlockFragment extends BaseFragment implements CardBlockContract.View {

    private String cardType;
    private String cardId;
    private String serial;
    private String balance;
    private String date;

    @Inject
    CardBlockPresenter presenter;

    @BindView(R.id.text_card_serial1_ife_block)
    TextView showSerial;
    @BindView(R.id.text_card_balance1_ife_block)
    TextView showBalance;
    @BindView(R.id.text_card_date1_ife_block)
    TextView showDate;
    @BindView(R.id.serial_carnet1_cdlp_block)
    TextView showSerialCdlp;
    @BindView(R.id.balance_amount1_cdlp_block)
    TextView showBalanceCdlp;
    @BindView(R.id.code_carnet1_cdlp_block)
    TextView showCodeCdlp;

    @BindView(R.id.item_carnet_block)
    CardView item_carnet;
    @BindView(R.id.item_ife_block)
    CardView item_ife;

    @BindView(R.id.text_comment)
    MaterialEditText textComment;

    @BindView(R.id.btn_block)
    Button btnBlock;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new CardBlockModule(this)).inject(this);
    }

    public static CardBlockFragment newInstance(String cardType, String cardId, String serial, String balance, String date) {

        CardBlockFragment cardBlockFragment = new CardBlockFragment();
        Bundle args = new Bundle();
        args.putString(Constant.ARGS_TYPE, cardType);
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);
        cardBlockFragment.setArguments(args);

        return cardBlockFragment;
    }

    public CardBlockFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_card, container, false);
        ButterKnife.bind(this, view);

        cardType = getArguments().getString(Constant.ARGS_TYPE);
        cardId = getArguments().getString(Constant.ARGS_CARD_ID);
        serial = getArguments().getString(Constant.ARGS_SERIAL);
        balance = getArguments().getString(Constant.ARGS_BALANCE);
        date = getArguments().getString(Constant.ARGS_DATE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.setText();

        if (cardType.equals("1001")) {
            item_carnet.setVisibility(View.VISIBLE);
            item_ife.setVisibility(View.GONE);
        } else {
            item_ife.setVisibility(View.VISIBLE);
            item_carnet.setVisibility(View.GONE);
        }

        checkedBtn();
    }

    private void checkedBtn() {

        btnBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnBlock();
            }
        });
    }

    @Override
    public void setSerial() {
        showSerial.setText(serial);
        showSerialCdlp.setText(serial);
    }

    @Override
    public void setBalance() {
        showBalance.setText(balance);
        showBalanceCdlp.setText(balance);
    }

    @Override
    public void setDate() {
        showDate.setText(date);
        showCodeCdlp.setText(date);
    }

    @Override
    public void setColorIcon() {
    }

    @Override
    public String getComment() {
        return textComment.getText().toString();
    }

    @Override
    public void backCardList() {

        Fragment fragment = new TitleFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void showAlertDialog() {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }

        builder.setTitle(getString(R.string.report_title))
                .setMessage(getString(R.string.message) + serial + "?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.blockCard(cardId);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}
