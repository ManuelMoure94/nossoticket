package com.rsegrp.nossoticket.ui.user.mvp.changePw;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataNewPassword;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class NewPasswordPresenter implements NewPasswordContract.Presenter {

    private NewPasswordContract.View view;
    private NewPasswordContract.Interact interact;

    public NewPasswordPresenter(NewPasswordContract.View view, NewPasswordContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnNewPassword() {

        if (checkField()) {
            view.showProgress();
            interact.changedPassword(new DataNewPassword(view.getPassword(), view.getNewPassword()), this);
        }
    }

    @Override
    public void onSuccess(Response<String> response) {

        view.hideProgress();
        if (response != null) {
            view.onSuccess();
        }
    }

    @Override
    public void onError() {

        view.hideProgress();
        view.onError();
    }

    @Override
    public void onFailed() {

        view.hideProgress();
        view.onFailed();
    }

    private boolean checkField() {

        return checkPassword() && checkNewPassword() && checkNewRePassword()
                && checkMatchPw();
    }

    private boolean checkPassword() {

        if (TextUtils.isEmpty(view.getPassword())) {
            view.showError(App.getInstance().getString(R.string.field_pass_empty));
            return false;
        } else return true;
    }

    private boolean checkNewPassword() {

        if (TextUtils.isEmpty(view.getNewPassword())) {
            view.showError(App.getInstance().getString(R.string.field_new_pass_empty));
            return false;
        } else return true;
    }

    private boolean checkNewRePassword() {

        if (TextUtils.isEmpty(view.getNewRePassword())) {
            view.showError(App.getInstance().getString(R.string.field_new_rePass_empty));
            return false;
        } else return true;
    }

    private boolean checkMatchPw() {

        if (view.getNewPassword().equals(view.getNewRePassword())) {
            return true;
        } else {
            view.showError(App.getInstance().getString(R.string.notMatchPass));
            return false;
        }
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }
}
