package com.rsegrp.nossoticket.ui.recharge.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.wallet.recharge.RechargeRequest;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.recharge.di.receipt.ReceiptModule;
import com.rsegrp.nossoticket.ui.recharge.mvp.receipt.ReceiptContract;
import com.rsegrp.nossoticket.ui.recharge.mvp.receipt.ReceiptPresenter;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ReceiptFragment extends BaseFragment implements ReceiptContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    private String method;
    private String targetBank;
    private String originBank;
    private String code;
    private String balance;
    private String date;
    private String receipt;

    private RechargeRequest rechargeRequest;

    @Inject
    ReceiptPresenter presenter;

    @BindView(R.id.show_method)
    TextView showMethod;
    @BindView(R.id.show_origin)
    TextView showOrigin;
    @BindView(R.id.show_target)
    TextView showTarget;
    @BindView(R.id.show_amount)
    TextView showAmount;
    @BindView(R.id.show_date)
    TextView showDate;
    @BindView(R.id.message_confirmation)
    TextView msgConfirmation;
    @BindView(R.id.title_origin)
    TextView titleOrigin;
    @BindView(R.id.input_receipt)
    TextView inputReceipt;

    @BindView(R.id.progress_recharge)
    ProgressBar progressRecharge;

    @BindView(R.id.btn_receipt)
    Button btn_receipt;

    public static ReceiptFragment newInstance(String method, String targetBank,
                                              String originBank, String code,
                                              String balance, String date) {

        ReceiptFragment receiptFragment = new ReceiptFragment();
        Bundle args = new Bundle();
        args.putString(Constant.ARGS_METHOD, method);
        args.putString(Constant.ARGS_TARGET, targetBank);
        args.putString(Constant.ARGS_ORIGIN, originBank);
        args.putString(Constant.ARGS_CODE, code);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);
        receiptFragment.setArguments(args);

        return receiptFragment;
    }

    public ReceiptFragment() {
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new ReceiptModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt, container, false);
        ButterKnife.bind(this, view);

        method = getArguments().getString(Constant.ARGS_METHOD);
        targetBank = getArguments().getString(Constant.ARGS_TARGET);
        originBank = getArguments().getString(Constant.ARGS_ORIGIN);
        code = getArguments().getString(Constant.ARGS_CODE);
        balance = getArguments().getString(Constant.ARGS_BALANCE);
        date = getArguments().getString(Constant.ARGS_DATE);
        receipt = getArguments().getString(Constant.ARGS_CODE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        presenter.checkedMethod(method);
        hideProgress();
        setRequest();
        checkedBtm();
    }

    private void setRequest() {

        String methodRequest;
        if (method.equals("Transference")) {
            methodRequest = "transferencia";
        } else methodRequest = "deposito";

        rechargeRequest = new RechargeRequest(methodRequest, originBank,
                targetBank, code, Double.parseDouble(balance), date);
    }

    private void checkedBtm() {

        btn_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnReceipt(rechargeRequest);
            }
        });
    }

    @Override
    public void setMessage(String message) {
        msgConfirmation.setText(message);
    }

    @Override
    public void setMethod() {

        if (method.equals("Transference")) {
            showMethod.setText(getString(R.string.transference));
        } else showMethod.setText(getString(R.string.deposit));
    }

    @Override
    public void setTarget() {
        showTarget.setText(targetBank);
    }

    @Override
    public void setOrigin() {
        showOrigin.setText(originBank);
    }

    @Override
    public void setAmount() {
        String message = balance + " Bs";
        showAmount.setText(message);
    }

    @Override
    public void setDate() {
        showDate.setText(date);
    }

    @Override
    public void setReceipt() {
        inputReceipt.setText(receipt);
    }

    @Override
    public void showOrigin() {
        showOrigin.setVisibility(View.VISIBLE);
        titleOrigin.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideOrigin() {
        showOrigin.setVisibility(View.GONE);
        titleOrigin.setVisibility(View.GONE);
    }

    @Override
    public void nextFragment(String fragmentTag) {

        Fragment fragment;
        if (fragmentTag.equals(Constant.FRAGMENT_SUMMARY)) {
            fragment = new SummaryFragment();
        } else fragment = new RechargeFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public String getTextBtn() {
        return btn_receipt.getText().toString();
    }

    @Override
    public void showProgress() {
        progressRecharge.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressRecharge.setVisibility(View.GONE);
    }

    @Override
    public void rechargingError() {
        new DialogsOneBtn(this, getString(R.string.fail_recharging), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
    }

    @Override
    public void retryConnection() {
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void setTextBtn(String textBtn) {
        btn_receipt.setText(textBtn);
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                nextFragment(Constant.FRAGMENT_RECHARGE);
                break;
            case "retry":
                presenter.btnReceipt(rechargeRequest);
                break;
            case "accept":
                nextFragment(Constant.FRAGMENT_RECHARGE);
                break;
        }
    }
}