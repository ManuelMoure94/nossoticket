package com.rsegrp.nossoticket.ui.express.mvp.addNewTicket;

import android.support.v4.app.Fragment;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.express.fragments.TicketListFragment;
import com.rsegrp.nossoticket.ui.express.fragments.TicketReceiptFragment;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class AddNewTicketPresenter implements AddNewTicketContract.Presenter {

    private AddNewTicketContract.View view;
    private AddNewTicketContract.Interact interact;

    private Fragment fragment;

    public AddNewTicketPresenter(AddNewTicketContract.View view, AddNewTicketContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void getUser() {
        interact.getUser(this);
    }

    @Override
    public void btnAddTicket() {
        receipt();
    }

    @Override
    public void btnExistingTickets() {
        existingTickets();
    }

    @Override
    public void isSuccess(Response<DataUser> response) {

        view.setRemaining(response.body().getRemaining());
    }

    private void receipt() {

        fragment = new TicketReceiptFragment();
        view.nextFragment(fragment);
    }

    private void existingTickets() {

        fragment = new TicketListFragment();
        view.nextFragment(fragment);
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }
}