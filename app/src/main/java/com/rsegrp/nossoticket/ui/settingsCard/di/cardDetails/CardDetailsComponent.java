package com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails;

import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardDetailsFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@CardDetailsScope
@Subcomponent(modules = {CardDetailsModule.class})
public interface CardDetailsComponent {
    void inject(CardDetailsFragment target);
}
