package com.rsegrp.nossoticket.ui.summary.adapter;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.summary.model.SummaryModel;
import com.rsegrp.nossoticket.utils.AnimLayoutParams;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SummaryModel> items;
    private boolean[] isOpenView;
    private Context context;

    private HistoryAdapter adapter;

    public SummaryAdapter(List<SummaryModel> items, Context context) {
        this.context = context;
        this.items = items;
        isOpenView = new boolean[items.size()];

        for (int i = 0; i < isOpenView.length; i++) {
            isOpenView[i] = false;
        }
    }

    public class NewUserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_day_new_user)
        TextView dateDay;
        @BindView(R.id.date_year_new_user)
        TextView dateYear;
        @BindView(R.id.email_new_user)
        TextView email;

        public NewUserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class NewTittleHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_day_new_tittle)
        TextView dateDay;
        @BindView(R.id.date_year_new_tittle)
        TextView dateYear;
        @BindView(R.id.code_new_tittle)
        TextView code;

        public NewTittleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class RechargeWalletHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date_day_recharge_wallet)
        TextView dateDay;
        @BindView(R.id.date_year_recharge_wallet)
        TextView dateYear;
        @BindView(R.id.state_recharge_wallet)
        TextView state;
        @BindView(R.id.amount_recharge_wallet)
        TextView amountData;
        @BindView(R.id.input_bank_recharge_wallet)
        TextView inputBank;
        @BindView(R.id.input_code_recharge_wallet)
        TextView inputCode;
        @BindView(R.id.input_type_recharge_wallet)
        TextView inputType;
        @BindView(R.id.input_admin_recharge_wallet)
        TextView inputAdmin;

        @BindView(R.id.expandable_recharge_wallet)
        LinearLayout expandableContent;
        @BindView(R.id.content_summary_recharge_wallet)
        LinearLayout contentItem;

        @BindView(R.id.rv_history)
        RecyclerView history;

        @BindView(R.id.arrow_drop)
        ImageView arrowDrop;

        final Animation animation = AnimationUtils.loadAnimation(App.getInstance().getBaseContext(), R.anim.rotate);

        public RechargeWalletHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            contentItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && !isOpenView[position]) {
                        animateLayoutUp(expandableContent, position);
                        arrowDrop.startAnimation(animation);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                arrowDrop.setImageResource(R.drawable.ic_arrow_drop_up);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    } else if (position != RecyclerView.NO_POSITION && isOpenView[position]) {
                        animateLayoutDown(expandableContent, position);
                        arrowDrop.startAnimation(animation);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                arrowDrop.setImageResource(R.drawable.ic_arrow_drop_down);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                }
            });
        }

        public void animateLayoutUp(LinearLayout linearLayout, int position) {

            AnimLayoutParams animLayoutParams = new AnimLayoutParams();
            AnimatorSet animatorSet = new AnimatorSet();
            ValueAnimator valueAnimator = animLayoutParams.animarLayoutHeight(linearLayout, RelativeLayout.LayoutParams.WRAP_CONTENT, 350);
            animatorSet.playTogether(valueAnimator);
            animatorSet.start();
            isOpenView[position] = true;
        }

        public void animateLayoutDown(LinearLayout linearLayout, int position) {

            AnimLayoutParams animLayoutParams = new AnimLayoutParams();
            AnimatorSet animatorSet = new AnimatorSet();
            ValueAnimator valueAnimator = animLayoutParams.animarLayoutHeight(linearLayout, 0, 350);
            animatorSet.playTogether(valueAnimator);
            animatorSet.start();
            isOpenView[position] = false;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (items.get(position).getType().equals(Constant.TYPE_ITEM_SUMMARY_USER)) {
            return Constant.ITEM_SUMMARY_USER;
        } else if (items.get(position).getType().equals(Constant.TYPE_ITEM_SUMMARY_TITLLE)) {
            return Constant.ITEM_SUMMARY_TITLLE;
        } else if ((items.get(position).getType().equals(Constant.TYPE_ITEM_SUMMARY_WALLET))) {
            return Constant.ITEM_SUMMARY_WALLET;
        } else return Constant.ITEM_NULL;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == Constant.ITEM_SUMMARY_USER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_summary_user_register, parent, false);

            return new NewUserHolder(view);
        } else if (viewType == Constant.ITEM_SUMMARY_TITLLE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_summary_new_card, parent, false);

            return new NewTittleHolder(view);
        } else if (viewType == Constant.ITEM_SUMMARY_WALLET) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_summary_recharge_wallet, parent, false);

            return new RechargeWalletHolder(view);
        } else return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NewUserHolder) {

            NewUserHolder userHolder = (NewUserHolder) holder;
            userHolder.dateDay.setText(items.get(position).getDay());
            userHolder.dateYear.setText(items.get(position).getYear());
            userHolder.email.setText(items.get(position).getInfo());

        } else if (holder instanceof NewTittleHolder) {

            NewTittleHolder tittleHolder = (NewTittleHolder) holder;
            tittleHolder.dateDay.setText(items.get(position).getDay());
            tittleHolder.dateYear.setText(items.get(position).getYear());
            tittleHolder.code.setText(items.get(position).getInfo());

        } else if (holder instanceof RechargeWalletHolder) {

            RechargeWalletHolder walletHolder = (RechargeWalletHolder) holder;
            walletHolder.dateDay.setText(items.get(position).getDay());
            walletHolder.dateYear.setText(items.get(position).getYear());
            walletHolder.state.setText(items.get(position).getInfo());
            walletHolder.amountData.setText(String.valueOf(items.get(position).getChildSummaryModel().getAmount()));
            walletHolder.inputBank.setText(items.get(position).getChildSummaryModel().getBank());
            walletHolder.inputCode.setText(items.get(position).getChildSummaryModel().getCode());
            walletHolder.inputType.setText(items.get(position).getChildSummaryModel().getType());
            walletHolder.inputAdmin.setText(items.get(position).getChildSummaryModel().getAdmin());

            walletHolder.history.setHasFixedSize(true);
            walletHolder.history.setLayoutManager(new LinearLayoutManager(context));

            adapter = new HistoryAdapter(items.get(position).getChildSummaryModel().getHistoryModel());
            walletHolder.history.setAdapter(adapter);

        }

        setFadeAnimation(holder.itemView);

        if ((position % 2) == 0)
            holder.itemView.setBackgroundResource(R.color.whiteTransparent);
        else
            holder.itemView.setBackgroundResource(R.color.backgroundMenuExpandable);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setFadeAnimation(View view) {

        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(Constant.FADE_DURATION);
        view.startAnimation(animation);
    }
}
