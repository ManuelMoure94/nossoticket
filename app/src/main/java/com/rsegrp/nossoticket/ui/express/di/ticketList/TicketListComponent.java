package com.rsegrp.nossoticket.ui.express.di.ticketList;

import com.rsegrp.nossoticket.ui.express.fragments.TicketListFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@TicketListScope
@Subcomponent(modules = {TicketListModule.class})
public interface TicketListComponent {
    void inject(TicketListFragment target);
}
