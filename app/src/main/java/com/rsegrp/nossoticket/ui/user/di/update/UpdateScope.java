package com.rsegrp.nossoticket.ui.user.di.update;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface UpdateScope {
}
