package com.rsegrp.nossoticket.ui.movements.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.movements.adapter.interfaces.CardAdapter;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardBlockFragment;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardRechargeFragment;
import com.rsegrp.nossoticket.utils.constants.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public class CardFragment extends Fragment {

    @BindView(R.id.card_options_movement)
    TextView optionsMovement;
    @BindView(R.id.text_card_serial_movement)
    TextView serialMovement;
    @BindView(R.id.text_card_balance_movement)
    TextView balanceMovement;
    @BindView(R.id.text_card_date_movement)
    TextView dateMovement;

    @BindView(R.id.card_movements)
    CardView cardView;

    public static Fragment getInstance(int position, String cardId, String serial, int balance, String description) {

        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.ARGS_POSITION, position);
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putInt(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DESCRIPTION, description);
        fragment.setArguments(args);

        return fragment;
    }

    public CardFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_movement, container, false);
        ButterKnife.bind(this, view);

        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        return view;
    }

    @Override

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setData();
        onListenerClick();
    }

    private void onListenerClick() {

        optionsMovement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsMenu(optionsMovement);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void optionsMenu(TextView view) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.inflate(R.menu.menu_card_movements);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.recharging:
                        recharging();
                        break;

                    case R.id.disaffiliate:
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.block:
                        block();
                        break;
                }
                return false;
            }
        });

        popupMenu.show();
//        MenuPopupHelper menuHelper = new MenuPopupHelper(getActivity(), (MenuBuilder) popupMenu.getMenu(), view);
//        menuHelper.setForceShowIcon(true);
//        menuHelper.show();
    }

    @SuppressLint("SetTextI18n")
    private void setData() {

        serialMovement.setText(getArguments().getString(Constant.ARGS_SERIAL));
        balanceMovement.setText(getArguments().getInt(Constant.ARGS_BALANCE) + " Bs");
        dateMovement.setText(getArguments().getString(Constant.ARGS_DESCRIPTION));
    }

    public void block() {

        Fragment fragment = CardBlockFragment.newInstance("0111", getArguments().getString(Constant.ARGS_CARD_ID),
                getArguments().getString(Constant.ARGS_SERIAL), String.valueOf(getArguments().getInt(Constant.ARGS_BALANCE)),
                getArguments().getString(Constant.ARGS_DESCRIPTION));
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    private void recharging() {

        Fragment fragment = CardRechargeFragment.newInstance(getArguments().getString(Constant.ARGS_CARD_ID),
                getArguments().getString(Constant.ARGS_SERIAL), getArguments().getString(Constant.ARGS_BALANCE),
                getArguments().getString(Constant.ARGS_DESCRIPTION));
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    public CardView getCardView() {
        return cardView;
    }
}
