package com.rsegrp.nossoticket.ui.express.di.ticketList;

import com.rsegrp.nossoticket.ui.express.fragments.TicketListFragment;
import com.rsegrp.nossoticket.ui.express.mvp.ticketList.TicketListInteract;
import com.rsegrp.nossoticket.ui.express.mvp.ticketList.TicketListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@Module
public class TicketListModule {

    private TicketListFragment ticketListFragment;

    public TicketListModule(TicketListFragment ticketListFragment) {
        this.ticketListFragment = ticketListFragment;
    }

    @Provides
    @TicketListScope
    public TicketListFragment providesTicketListFragment() {
        return ticketListFragment;
    }

    @Provides
    @TicketListScope
    public TicketListPresenter providesTicketListPresenter(TicketListInteract interact) {
        return new TicketListPresenter(ticketListFragment, interact);
    }

    @Provides
    @TicketListScope
    public TicketListInteract providesTicketListInteract() {
        return new TicketListInteract();
    }
}
