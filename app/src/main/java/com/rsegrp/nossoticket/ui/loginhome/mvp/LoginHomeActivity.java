package com.rsegrp.nossoticket.ui.loginhome.mvp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseActivity;
import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendPassFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;
import com.rsegrp.nossoticket.ui.loginhome.di.LoginHomeModule;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class LoginHomeActivity extends BaseActivity implements LoginHomeContract.View {

    @Inject
    public LoginHomePresenter presenter;

    @BindView(R.id.image_back)
    ImageView imageBack;

    @BindView(R.id.scrollViewLogin)
    ScrollView scrollView;

    @BindView(R.id.content_progress)
    RelativeLayout contentProgress;
    @BindView(R.id.sendMail)
    RelativeLayout sendMail;
    @BindView(R.id.successMail)
    RelativeLayout successMail;

    @BindView(R.id.btnForgot)
    Button btnForgot;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new LoginHomeModule(this)).inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_login);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent != null && intent.getData() != null) {
            Fragment fragment = SendPassFragment.newInstance(intent.getData().toString());
            setFragment(fragment);
        } else {
            Date expireToken = new Date(loadPreferencesDate());
            if (expireToken.after(Calendar.getInstance().getTime())) {
                autoLogin();
            } else {
                presenter.initFragment();
            }
        }

        hideProgress();

        setColorBackground(R.color.colorPrimary);
        hideArrow();

        checkBtn();
    }

    private void checkBtn() {

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new LoginFragment());
            }
        });

        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successMail.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void setFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fab_slide_in_from_right, R.anim.fab_slide_out_to_right)
                .replace(R.id.contentLogin, fragment)
                .commit();
    }

    @Override
    public void autoLogin() {

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fab_scale_up, R.anim.fab_scale_down);
        finish();
    }

    @Override
    public void showProgressForgot() {
        sendMail.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressForgot() {
        sendMail.setVisibility(View.GONE);
    }

    @Override
    public void showMsgSuccess() {

        successMail.setVisibility(View.VISIBLE);
        Fragment fragment = new LoginFragment();
        setFragment(fragment);
    }

    public void setColorBackground(int colorBackground) {

        scrollView.setBackgroundResource(colorBackground);
//        if (colorBackground.equals("white")) {
//            scrollView.setBackgroundResource(R.color.white);
//        } else {
//            scrollView.setBackgroundResource(R.color.colorPrimary);
//        }
    }

    public void showArrow() {

        Drawable iconArrow = ContextCompat.getDrawable(getBaseContext(), R.drawable.ic_arrow_back_black_24px);
        iconArrow.mutate().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        imageBack.setImageDrawable(iconArrow);
        imageBack.setVisibility(View.VISIBLE);
    }

    public void hideArrow() {
        imageBack.setVisibility(View.GONE);
    }

    public void showProgress() {
        contentProgress.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        contentProgress.setVisibility(View.GONE);
    }
}