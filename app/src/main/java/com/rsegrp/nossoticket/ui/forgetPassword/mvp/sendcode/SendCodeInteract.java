package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendCodeInteract implements SendCodeContract.Interact {
    @Override
    public boolean validateCode(String code) {
        return code.equals("1234");
    }
}
