package com.rsegrp.nossoticket.ui.express.di.showTicket;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ShowTicketScope {
}
