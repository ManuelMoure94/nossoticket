package com.rsegrp.nossoticket.ui.forgetPassword.di.sendmail;

import com.rsegrp.nossoticket.api.services.ForgotPasswordServices;
import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendMailFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail.SendMailInteract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail.SendMailPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@Module
public class SendMailModule {

    private SendMailFragment sendMailFragment;

    public SendMailModule(SendMailFragment sendMailFragment) {
        this.sendMailFragment = sendMailFragment;
    }

    @Provides
    @SendMailScope
    public SendMailFragment providesSendMailFragment() {
        return sendMailFragment;
    }

    @Provides
    @SendMailScope
    public SendMailPresenter providesSendMailPresenter(SendMailInteract interact) {
        return new SendMailPresenter(sendMailFragment, interact);
    }

    @Provides
    @SendMailScope
    public SendMailInteract providesSendMailInteract(ForgotPasswordServices forgotPasswordServices) {
        return new SendMailInteract(forgotPasswordServices);
    }
}
