package com.rsegrp.nossoticket.ui.home.adapter;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

public interface onClickListenerRecycler {

    interface clickHolder {
        void onClicked(int position);
    }

    interface clickAdapter {
        void onClicked(int position);
    }
}
