package com.rsegrp.nossoticket.ui.movements.adapter.interfaces;

/**
 * Created by ManuelMoure on 27/07/2018.
 */

public interface OnClick {

    interface OnClickAdapter {
        void onClickAddTitle(int position);
    }

    interface OnClickHolder {
        void onClickAddTitle(int position);
    }
}
