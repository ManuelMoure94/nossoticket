package com.rsegrp.nossoticket.ui.forgetPassword.di.sendmail;

import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendMailFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@SendMailScope
@Subcomponent(modules = {SendMailModule.class})
public interface SendMailComponent {
    void inject(SendMailFragment target);
}
