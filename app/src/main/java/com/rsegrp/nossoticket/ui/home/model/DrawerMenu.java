package com.rsegrp.nossoticket.ui.home.model;

/**
 * Created by RSE_VZLA_07 on 14/2/2018.
 */

public class DrawerMenu {

    private int icon;
    private String name;

    public DrawerMenu(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
