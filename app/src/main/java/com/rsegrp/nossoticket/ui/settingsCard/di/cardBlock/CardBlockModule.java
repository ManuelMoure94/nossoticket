package com.rsegrp.nossoticket.ui.settingsCard.di.cardBlock;

import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardBlockFragment;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock.CardBlockInteract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardBlock.CardBlockPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@Module
public class CardBlockModule {

    private CardBlockFragment cardBlockFragment;

    public CardBlockModule(CardBlockFragment cardBlockFragment) {
        this.cardBlockFragment = cardBlockFragment;
    }

    @Provides
    @CardBlockScope
    public CardBlockFragment providesCardBlockFragment() {
        return cardBlockFragment;
    }

    @Provides
    @CardBlockScope
    public CardBlockPresenter providesCardBlockPresenter(CardBlockInteract interact) {
        return new CardBlockPresenter(cardBlockFragment, interact);
    }

    @Provides
    @CardBlockScope
    public CardBlockInteract providesCardBlockInteract(TitleServices titleServices) {
        return new CardBlockInteract(titleServices);
    }
}
