package com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails;

import com.rsegrp.nossoticket.api.services.MovementServices;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardDetailsFragment;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails.CardDetailsInteract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails.CardDetailsPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@Module
public class CardDetailsModule {

    private CardDetailsFragment cardDetailsFragment;

    public CardDetailsModule(CardDetailsFragment cardDetailsFragment) {
        this.cardDetailsFragment = cardDetailsFragment;
    }

    @Provides
    @CardDetailsScope
    public CardDetailsFragment providesCardDetailsFragment() {
        return cardDetailsFragment;
    }

    @Provides
    @CardDetailsScope
    public CardDetailsPresenter providesCardDetailsPresenter(CardDetailsInteract interact) {
        return new CardDetailsPresenter(cardDetailsFragment, interact);
    }

    @Provides
    @CardDetailsScope
    public CardDetailsInteract providesCardDetailsInteract(MovementServices movementServices) {
        return new CardDetailsInteract(movementServices);
    }
}