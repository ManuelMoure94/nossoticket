package com.rsegrp.nossoticket.ui.recharge.fragment;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.recharge.di.recharge.RechargeModule;
import com.rsegrp.nossoticket.ui.recharge.mvp.recharge.RechargeContract;
import com.rsegrp.nossoticket.ui.recharge.mvp.recharge.RechargePresenter;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.AnimLayoutParams;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class RechargeFragment extends BaseFragment implements RechargeContract.View {

    @Inject
    RechargePresenter presenter;

    @BindView(R.id.containers_method)
    RelativeLayout containerMethod;
    @BindView(R.id.containers_bank)
    RelativeLayout containerBank;
    @BindView(R.id.containers_amount)
    RelativeLayout containerAmount;
    @BindView(R.id.content_recharge_method)
    RelativeLayout contentMethod;
    @BindView(R.id.content_bank_data_transference)
    RelativeLayout contentData;
    @BindView(R.id.content_amount)
    RelativeLayout contentAmount;

    @BindView(R.id.imageView5)
    ImageView iconMethod;
    @BindView(R.id.imageView6)
    ImageView iconData;
    @BindView(R.id.imageView7)
    ImageView iconAmount;

    @BindView(R.id.text_date)
    MaterialEditText textDate;
    @BindView(R.id.text_receipt)
    MaterialEditText textReceipt;
    @BindView(R.id.text_amount)
    MaterialEditText textAmount;

    @BindView(R.id.rdBtn_deposit)
    RadioButton rdBtnDeposit;
    @BindView(R.id.rdBtn_transference)
    RadioButton rdBtnTransference;

    @BindView(R.id.origin_spinner)
    MaterialBetterSpinner originSpinner;
    @BindView(R.id.target_spinner)
    MaterialBetterSpinner targetSpinner;

    @BindView(R.id.btn_recharge)
    Button btnRecharge;

    String[] listItems;
    ArrayAdapter<String> adapter;

    private boolean isOpenView = false;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new RechargeModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_balance_up, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).showToolbar();

        checkBtn();
        initItems();
    }

    private void initItems() {

        listItems = getResources().getStringArray(R.array.array_bank);

        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, listItems);

        targetSpinner.setAdapter(adapter);
        originSpinner.setAdapter(adapter);
    }

    private void checkBtn() {

        containerMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOpenView) {
                    presenter.btnMethodArrow();
                } else {
                    presenter.btnMethodArrowHide();
                }
            }
        });

        containerBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (contentData.getVisibility() == View.GONE) {
//                    if (rdBtnDeposit.isChecked() || rdBtnTransference.isChecked()) {
//                        if (rdBtnTransference.isChecked()) {
//                            presenter.isTransference();
//                        } else presenter.isDeposit();
//                        presenter.btnDataArrow();
//                    } else showError(getActivity().getString(R.string.message_recharge_1));
//                } else presenter.btnDataArrowHide();

                if (!isOpenView) {
                    if (rdBtnDeposit.isChecked() || rdBtnTransference.isChecked()) {
                        if (rdBtnTransference.isChecked()) {
                            presenter.isTransference();
                        } else presenter.isDeposit();
                        presenter.btnDataArrow();
                    } else showError(getActivity().getString(R.string.message_recharge_1));
                } else {
                    presenter.btnDataArrowHide();
                }
            }
        });

        containerAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (contentAmount.getVisibility() == View.GONE) {
//                    presenter.btnAmountArrow();
//                } else presenter.btnAmountArrowHide();
                if (!isOpenView) {
                    presenter.btnAmountArrow();
                } else {
                    presenter.btnAmountArrowHide();
                }
            }
        });

        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rdBtnTransference.isChecked()) {
                    presenter.btnRechargeTransference();
                } else if (rdBtnDeposit.isChecked()) {
                    presenter.btnRechargeDeposit();
                } else showError(getActivity().getString(R.string.message_recharge_1));
            }
        });
    }

    private void showDatePickerDialog() {

        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                final String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                textDate.setText(selectedDate);
            }
        });
        newFragment.show(getActivity().getFragmentManager(), "DATEPICKER");
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public String getBankTarget() {
        return targetSpinner.getText().toString();
    }

    @Override
    public String getBankOrigin() {
        return originSpinner.getText().toString();
    }

    @Override
    public String getReceipt() {
        return textReceipt.getText().toString();
    }

    @Override
    public String getDate() {
        return textDate.getText().toString();
    }

    @Override
    public String getAmount() {
        return textAmount.getText().toString();
    }

    @Override
    public void showMethodContent() {
        iconMethod.setImageResource(android.R.drawable.arrow_up_float);
        animateLayoutUp(contentMethod);
        hideAmountContent();
        hideDataContent();
//        isOpenView = true;
    }

    @Override
    public void showDataContent() {
        iconData.setImageResource(android.R.drawable.arrow_up_float);
        animateLayoutUp(contentData);
        hideAmountContent();
        hideMethodContent();
//        isOpenView = true;

    }

    @Override
    public void showAmountContent() {
        iconAmount.setImageResource(android.R.drawable.arrow_up_float);
        animateLayoutUp(contentAmount);
        hideDataContent();
        hideMethodContent();
//        isOpenView = true;
    }

    @Override
    public void showOriginBank() {
        originSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMethodContent() {
        iconMethod.setImageResource(android.R.drawable.arrow_down_float);
        animateLayoutDown(contentMethod);
    }

    @Override
    public void hideDataContent() {
        iconData.setImageResource(android.R.drawable.arrow_down_float);
        animateLayoutDown(contentData);
    }

    @Override
    public void hideAmountContent() {
        iconAmount.setImageResource(android.R.drawable.arrow_down_float);
        animateLayoutDown(contentAmount);
    }

    @Override
    public void hideOriginBank() {
        originSpinner.setVisibility(View.GONE);
    }

    public static class DatePickerFragment extends DialogFragment {

        private DatePickerDialog.OnDateSetListener listener;

        public static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener listener) {
            DatePickerFragment fragment = new DatePickerFragment();
            fragment.setListener(listener);
            return fragment;
        }

        public void setListener(DatePickerDialog.OnDateSetListener listener) {
            this.listener = listener;
        }

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar calendar = Calendar.getInstance();

            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), listener, year, month, day);
        }
    }

    @Override
    public void nextFragment() {

        String method = null;

        if (rdBtnTransference.isChecked()) {
            method = "Transference";
        } else if (rdBtnDeposit.isChecked()) {
            method = "Deposit";
        }

        Fragment fragment = ReceiptFragment.newInstance(method, getBankTarget(),
                getBankOrigin(), getReceipt(), getAmount(), getDate());

        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    public void animateLayoutUp(RelativeLayout relativeLayout) {

        AnimLayoutParams animLayoutParams = new AnimLayoutParams();
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator valueAnimator = animLayoutParams.animarLayoutHeight(relativeLayout, RelativeLayout.LayoutParams.WRAP_CONTENT, 350);
        animatorSet.playTogether(valueAnimator);
        animatorSet.start();
        isOpenView = true;
    }

    public void animateLayoutDown(RelativeLayout relativeLayout) {

        AnimLayoutParams animLayoutParams = new AnimLayoutParams();
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator valueAnimator = animLayoutParams.animarLayoutHeight(relativeLayout, 0, 350);
        animatorSet.playTogether(valueAnimator);
        animatorSet.start();
        isOpenView = false;
    }
}