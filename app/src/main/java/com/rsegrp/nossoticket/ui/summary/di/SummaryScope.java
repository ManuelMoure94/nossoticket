package com.rsegrp.nossoticket.ui.summary.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SummaryScope {
}
