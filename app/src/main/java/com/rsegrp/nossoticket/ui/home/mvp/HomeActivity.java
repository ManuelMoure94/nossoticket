package com.rsegrp.nossoticket.ui.home.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseActivity;
import com.rsegrp.nossoticket.ui.home.adapter.DrawerAdapter;
import com.rsegrp.nossoticket.ui.home.adapter.onClickListenerRecycler;
import com.rsegrp.nossoticket.ui.home.di.HomeModule;
import com.rsegrp.nossoticket.ui.home.model.DrawerMenu;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.AddCardFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public class HomeActivity extends BaseActivity implements onClickListenerRecycler.clickAdapter, HomeContract.View {

    private int control = 0;
    boolean times;

    long timeSystem;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    private double remaining;

    int image;
    int color = Color.parseColor("#ffffff");

    List<DrawerMenu> items = new ArrayList<>();

    @Inject
    public HomePresenter presenter;

    String[] tagTitles;

    @BindView(R.id.full_name)
    TextView fullNameText;
    @BindView(R.id.mail)
    TextView emailText;
    @BindView(R.id.phone)
    TextView phoneText;

    @BindView(R.id.leftDrawer)
    RecyclerView leftDrawer;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.btn_menu)
    ImageView btn_menu;
    @BindView(R.id.btn_plus)
    ImageView btnPlus;

    @BindView(R.id.drawer_content)
    LinearLayout drawerContent;

    public HomeActivity() {
        image = R.drawable.ic_menu;
        times = true;
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new HomeModule(this)).inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        presenter.getUser();
        initDrawer();
        checkBtn();
        presenter.initFragment();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (times) {
                timeSystem = System.currentTimeMillis();
                presenter.initFragment();
                times = false;
                return false;
            } else if (!times && System.currentTimeMillis() - timeSystem <= 1500) {
                Log.i("TAG", "onKeyDown: <" + timeSystem + " " + times + " " + System.currentTimeMillis());
                times = true;
                return super.onKeyDown(keyCode, event);
            } else if (!times && System.currentTimeMillis() - timeSystem >= 1500) {
                Log.i("TAG", "onKeyDown: >" + timeSystem + " " + times + " " + System.currentTimeMillis());
                presenter.initFragment();
                times = true;
                return false;
            }
        }
        return false;
    }

    private void checkBtn() {

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnDrawer(control);
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddCardFragment();
                setFragment(fragment);
            }
        });
    }

    private void initDrawer() {

        tagTitles = getResources().getStringArray(R.array.Tags);

        items.add(new DrawerMenu(R.drawable.ic_summary, tagTitles[0]));
        items.add(new DrawerMenu(R.drawable.ic_card, tagTitles[1]));
        items.add(new DrawerMenu(R.drawable.ic_recharge, tagTitles[2]));
        items.add(new DrawerMenu(R.drawable.ic_ticket_express, tagTitles[3]));
        items.add(new DrawerMenu(R.drawable.ic_user, tagTitles[4]));
        items.add(new DrawerMenu(R.drawable.ic_update, tagTitles[5]));
        items.add(new DrawerMenu(R.drawable.ic_change_pass, tagTitles[6]));
        items.add(new DrawerMenu(R.drawable.ic_turn_off, tagTitles[7]));

        leftDrawer.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        leftDrawer.setLayoutManager(lManager);

        adapter = new DrawerAdapter(items, this);
        leftDrawer.setAdapter(adapter);

        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(drawerContent);
    }

    @Override
    public void closeDrawer() {
        drawerLayout.closeDrawer(drawerContent);
    }

    @Override
    public void setFragment(Fragment fragment) {

        hidePlusBottom();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
//                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .setCustomAnimations(R.anim.fab_slide_in_from_right, R.anim.fab_slide_out_to_right)
                .replace(R.id.frameContent, fragment)
                .commit();
    }

    @Override
    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void hideIcon() {
        btn_menu.setVisibility(View.GONE);
    }

    public void showIcon() {
        btn_menu.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClicked(int position) {
        presenter.selectFragment(position);
    }

    @Override
    public void logout() {

        savePreferences(" ");
        Intent intent = new Intent(this, LoginHomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fab_scale_up, R.anim.fab_scale_down);
        finish();
    }

    @Override
    public void setImageToolbar(int image, int control) {
        this.image = image;
        this.control = control;

        Drawable iconMenu = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_menu);
        iconMenu.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        Drawable iconArrow = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_arrow_back_black_24px);
        iconArrow.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        btn_menu.setImageResource(image);
    }

    @Override
    public int getImageToolbar() {
        return image;
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public String getToken() {
        if (!loadPreferencesToken().equals(" ")) {
            return loadPreferencesToken();
        } else return null;
    }

    @Override
    public void setFullName(String fullName) {
        fullNameText.setText(fullName);
    }

    @Override
    public void setEmail(String email) {
        emailText.setText(email);
    }

    @Override
    public void setPhone(String phone) {
        phoneText.setText(phone);
    }

    @Override
    public void showPlusBottom() {
        btnPlus.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePlusBottom() {
        btnPlus.setVisibility(View.GONE);
    }

    public String getUserFullName() {
        return presenter.getUserFullName();
    }

    public String getUserId() {
        return presenter.getUserId();
    }

    public String getUserEmail() {
        return presenter.getUserEmail();
    }

    public String getDataUSer() {
        return presenter.getUserDate();
    }
}
