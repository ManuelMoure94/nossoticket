package com.rsegrp.nossoticket.ui.recharge.di.recharge;

import com.rsegrp.nossoticket.ui.recharge.fragment.RechargeFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

@RechargeScope
@Subcomponent(modules = {RechargeModule.class})
public interface RechargeComponent {
    void inject(RechargeFragment target);
}
