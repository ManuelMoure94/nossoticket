package com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.titles.AddTitle;
import com.rsegrp.nossoticket.api.data.titles.NewTitle;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class AddCardPresenter implements AddCardContract.Presenter {

    private AddCardContract.View view;
    private AddCardContract.Interact interact;

    public AddCardPresenter(AddCardContract.View view, AddCardContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnAdd() {

        if (checkField()) {
            interact.addTitle(new NewTitle(view.getSerial(), view.getDescription()), this);
            view.showProgress();
        }
    }

    @Override
    public void onSuccess(Response<AddTitle> response) {
        view.backCardList();
        view.hideProgress();
        view.showError("Titulo Agregado");
    }

    @Override
    public void retry() {
        view.retry();
    }

    @Override
    public void error() {
        view.error();
    }

    private boolean checkField() {
        return checkSerial() && checkDescription();
    }

    private boolean checkDescription() {

        if (TextUtils.isEmpty(view.getDescription())) {
            view.showError(App.getInstance().getString(R.string.empty_description));
            return false;
        } else return true;
    }

    private boolean checkSerial() {

        if (TextUtils.isEmpty(view.getSerial())) {
            view.showError(App.getInstance().getString(R.string.serial));
            return false;
        } else return true;
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }
}
