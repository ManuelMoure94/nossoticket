package com.rsegrp.nossoticket.ui.forgetPassword.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendmail.SendMailModule;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail.SendMailContract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail.SendMailPresenter;
import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendMailFragment extends BaseFragment implements SendMailContract.View {

    @Inject
    public SendMailPresenter presenter;

    @BindView(R.id.inputEmail)
    MaterialEditText inputEmail;

    @BindView(R.id.btn_next_1)
    Button btnNext;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new SendMailModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_correo, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((LoginHomeActivity) getActivity()).setColorBackground(R.color.white);
        ((LoginHomeActivity) getActivity()).showArrow();
        checkBtn();
    }

    private void checkBtn() {

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnNext();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(App.getInstance().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public String getEmail() {
        return inputEmail.getText().toString();
    }

    @Override
    public void nextFragment() {

        Fragment fragment = new LoginFragment();
        ((LoginHomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        ((LoginHomeActivity) getActivity()).showProgressForgot();
    }

    @Override
    public void hideProgress() {
        ((LoginHomeActivity) getActivity()).hideProgressForgot();
    }

    @Override
    public void showMsgSuccess(String msg) {
        ((LoginHomeActivity) getActivity()).showMsgSuccess();
//        Snackbar.make(getView(), msg, Snackbar.LENGTH_INDEFINITE)
//                .setAction("Ok", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        nextFragment();
//                    }
//                })
//                .show();
    }
}
