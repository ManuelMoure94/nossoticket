package com.rsegrp.nossoticket.ui.movements.model;

/**
 * Created by RSE_VZLA_07 on 4/7/2018.
 */

public class ListMovementsModel {

    private String type;
    private String text1;
    private String date;
    private String amount;

    public ListMovementsModel(String type, String text1, String date, String amount) {
        this.type = type;
        this.text1 = text1;
        this.date = date;
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getData() {
        return date;
    }

    public void setData(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
