package com.rsegrp.nossoticket.ui.settingsCard.di.addCard;

import com.rsegrp.nossoticket.ui.settingsCard.fragment.AddCardFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@AddCardScope
@Subcomponent(modules = {AddCardModule.class})
public interface AddCardComponent {
    void inject(AddCardFragment target);
}
