package com.rsegrp.nossoticket.ui.settingsCard.di.cardDetails;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CardDetailsScope {
}
