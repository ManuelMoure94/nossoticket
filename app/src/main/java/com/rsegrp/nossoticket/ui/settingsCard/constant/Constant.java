package com.rsegrp.nossoticket.ui.settingsCard.constant;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class Constant {

    public static final String ARGS_CARD_ID = "card_id";
    public static final String ARGS_SERIAL = "serial";
    public static final String ARGS_BALANCE = "balance";
    public static final String ARGS_DATE = "date";

    public static final String NEXT_PAGE = "page=";
}
