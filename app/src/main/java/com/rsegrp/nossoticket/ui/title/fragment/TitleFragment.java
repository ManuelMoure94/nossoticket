package com.rsegrp.nossoticket.ui.title.fragment;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.AddCardFragment;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardBlockFragment;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardDetailsFragment;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardRechargeFragment;
import com.rsegrp.nossoticket.ui.title.adapter.OnClick;
import com.rsegrp.nossoticket.ui.title.adapter.TitleAdapter;
import com.rsegrp.nossoticket.ui.title.di.TitleModule;
import com.rsegrp.nossoticket.ui.title.model.TitleModel;
import com.rsegrp.nossoticket.ui.title.mvp.TitleContract;
import com.rsegrp.nossoticket.ui.title.mvp.TitlePresenter;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

public class TitleFragment extends BaseFragment implements TitleContract.View, OnClick.OnClickAdapter, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    @Inject
    public TitlePresenter presenter;

    @BindView(R.id.recycler_card)
    RecyclerView recyclerCard;

    @BindView(R.id.fad_menu)
    FloatingActionMenu fabMenu;

    @BindView(R.id.fab_add_card)
    FloatingActionButton fabBtn;

    @BindView(R.id.swipe_title)
    SwipeRefreshLayout swipeTitle;

    @BindView(R.id.progress_title)
    RelativeLayout progressTitle;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager lManager;

    private List<TitleModel> items = new ArrayList<>();

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new TitleModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_list, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeActivity) getActivity()).showToolbar();
        presenter.getTitles(true);
        checkBtn();
    }

    private void checkBtn() {

        Drawable iconArrow = ContextCompat.getDrawable(getActivity(), R.drawable.ic_card_dark);
        iconArrow.mutate().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        fabBtn.setImageDrawable(iconArrow);

        fabMenu.setClosedOnTouchOutside(true);
        fabBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnAdd();
            }
        });

        swipeTitle.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                items.clear();
                adapter.notifyDataSetChanged();
                presenter.getTitles(false);
            }
        });
    }

    @Override
    public void initRecycler() {

        items.clear();

        if (presenter.getHaveTitles()) {
            Log.i("TAG", "initRecycler: no title");
            items.add(new TitleModel(Constant.NO_TITLE, " ", " ", " ", " "));
        } else {
            Log.i("TAG", "initRecycler: have title");
            int size = presenter.getSize();

            for (int i = 0; i < size; i++) {
                items.add(new TitleModel(presenter.getCardType(i), presenter.getCardId(i), presenter.getCardSerial(i),
                        String.valueOf(presenter.getCardAmount(i)) + " Bs", presenter.getCardDate(i)));
            }
        }

        recyclerCard.setHasFixedSize(true);

        lManager = new LinearLayoutManager(getActivity());
        recyclerCard.setLayoutManager(lManager);

        adapter = new TitleAdapter(getActivity(),items, this);
        recyclerCard.setAdapter(adapter);

        swipeTitle.setRefreshing(false);
    }

    @Override
    public void showProgress() {
        progressTitle.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressTitle.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {

        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void onClickRecharge(int position, String cardId, String serial, String balance, String date) {

        Bundle args = new Bundle();
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);

        Fragment fragment = CardRechargeFragment.newInstance(cardId, serial, balance, date);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void onClickDetails(int position, String cardId, String serial, String balance, String date) {

        Bundle args = new Bundle();
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);

        Fragment fragment = CardDetailsFragment.newInstance(items.get(position).getCardType(), cardId, serial, balance, date);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void onClickBlock(int position, String cardId, String serial, String balance, String date) {

        Bundle args = new Bundle();
        args.putString(Constant.ARGS_CARD_ID, cardId);
        args.putString(Constant.ARGS_SERIAL, serial);
        args.putString(Constant.ARGS_BALANCE, balance);
        args.putString(Constant.ARGS_DATE, date);

        Fragment fragment = CardBlockFragment.newInstance(items.get(position).getCardType(), cardId, serial, balance, date);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void addCard() {

        Fragment fragment = new AddCardFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void hideProgressUpdate() {
        swipeTitle.setRefreshing(false);
    }

    @Override
    public void retry() {
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
        hideProgress();
    }

    @Override
    public void error() {
        new DialogsOneBtn(this, getString(R.string.fail_title), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
        hideProgress();
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                swipeTitle.setRefreshing(false);
                break;
            case "retry":
                presenter.getTitles(true);
                break;
            case "accept":
                break;
        }
    }
}
