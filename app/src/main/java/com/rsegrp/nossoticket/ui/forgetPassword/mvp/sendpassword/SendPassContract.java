package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword;

import com.rsegrp.nossoticket.api.data.forgot.ChangePassword;
import com.rsegrp.nossoticket.api.data.forgot.CheckToken;
import com.rsegrp.nossoticket.ui.base.BaseContract;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public interface SendPassContract {

    interface View extends BaseContract.View {

        String getPassword();

        String get2ndPassword();

        void homeLogin();

        void showProgress();

        void hideProgress();

        void onSuccess(String type, boolean isLogin);

        void onError(String type);

        void onFailed(String type);
    }

    interface Presenter {

        void btnNext();

        void checkToken(CheckToken checkToken);

        void changePassword();

        void onSuccess(String type);

        void onError(String type);

        void onFailed(String type);
    }

    interface Interact {

        void checkToken(CheckToken checkToken, SendPassPresenter presenter);

        void changePassword(ChangePassword changePassword, SendPassPresenter presenter);
    }
}
