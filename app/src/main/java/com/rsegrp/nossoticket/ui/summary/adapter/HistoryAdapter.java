package com.rsegrp.nossoticket.ui.summary.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.summary.model.HistoryModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 14/8/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {

    private List<HistoryModel> items;

    public HistoryAdapter(List<HistoryModel> items) {
        this.items = items;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_for_item_list_history, parent, false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.icon.setImageResource(items.get(position).getIcon());
        holder.status.setText(items.get(position).getStatus());
        holder.date.setText(items.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class HistoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item_for_item)
        ImageView icon;
        @BindView(R.id.tv_item_for_item)
        TextView status;
        @BindView(R.id.date_item_for_item)
        TextView date;

        public HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
