package com.rsegrp.nossoticket.ui.settingsCard.di.cardBlock;

import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardBlockFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

@CardBlockScope
@Subcomponent(modules = {CardBlockModule.class})
public interface CardBlockComponent {
    void inject(CardBlockFragment target);
}