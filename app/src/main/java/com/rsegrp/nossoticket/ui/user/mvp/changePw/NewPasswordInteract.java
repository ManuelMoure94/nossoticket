package com.rsegrp.nossoticket.ui.user.mvp.changePw;

import com.rsegrp.nossoticket.api.data.singIn.DataNewPassword;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class NewPasswordInteract implements NewPasswordContract.Interact {

    private SingInServices singInServices;

    public NewPasswordInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void changedPassword(DataNewPassword dataNewPassword, final NewPasswordPresenter presenter) {

        Call<String> call = singInServices.changePassword(Constant.CONTENT_TYPE,
                Constant.TOKEN + presenter.getToken(), dataNewPassword);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else {
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                presenter.onFailed();
            }
        });
    }
}
