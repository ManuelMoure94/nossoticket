package com.rsegrp.nossoticket.ui.summary.model;

import java.util.List;

/**
 * Created by RSE_VZLA_07 on 13/8/2018.
 */

public class ChildSummaryModel {

    private int amount;
    private String bank;
    private String code;
    private String type;
    private List<HistoryModel> historyModel;
    private String admin;

    public ChildSummaryModel(int amount, String bank, String code, String type, List<HistoryModel> historyModel, String admin) {
        this.amount = amount;
        this.bank = bank;
        this.code = code;
        this.type = type;
        this.historyModel = historyModel;
        this.admin = admin;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<HistoryModel> getHistoryModel() {
        return historyModel;
    }

    public void setHistoryModel(List<HistoryModel> historyModel) {
        this.historyModel = historyModel;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }
}
