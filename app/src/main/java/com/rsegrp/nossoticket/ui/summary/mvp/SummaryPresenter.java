package com.rsegrp.nossoticket.ui.summary.mvp;

import com.google.gson.Gson;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.summary.Description;
import com.rsegrp.nossoticket.api.data.summary.Summary;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.summary.model.ChildSummaryModel;
import com.rsegrp.nossoticket.ui.summary.model.HistoryModel;
import com.rsegrp.nossoticket.ui.summary.model.SummaryModel;
import com.rsegrp.nossoticket.utils.DateUtils;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

public class SummaryPresenter implements SummaryContract.Presenter {

    private SummaryContract.View view;
    private SummaryContract.Interact interact;

    private Response<List<Summary>> mResponse;

    private List<SummaryModel> items = new ArrayList<>();

    private Gson gson = new Gson();

    private DateFormat format = new SimpleDateFormat(Constant.DATE_FORMAT_RECEIVED);
    private DateFormat sFormat = new SimpleDateFormat(Constant.DATE_FORMAT_REQUIRED);

    public SummaryPresenter(SummaryContract.View view, SummaryContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnRecharge() {
        view.rechargeFragment();
    }

    @Override
    public void isSuccess(Response<List<Summary>> response) {
        mResponse = response;
        view.hideProgress();
        view.hideProgressUpdate();
        view.initRecycler();
    }

    @Override
    public void isSuccessUser(Response<DataUser> response) {
        view.setRemaining(response.body().getRemaining());
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {
        view.showError(message);
    }

    @Override
    public void getSummary(boolean state) {

        if (state) {
            view.showProgress();
        }
        interact.getUser(this);
        interact.getSummary(this);
    }

    @Override
    public void retry() {
        view.retry();
    }

    @Override
    public void error() {
        view.error();
    }

    @Override
    public List<SummaryModel> getItems() {

        if (mResponse != null) {
            for (int i = 0; i < mResponse.body().size(); i++) {

                if (mResponse.body().get(i).getType().equals(Constant.TYPE_ITEM_SUMMARY_USER) || mResponse.body().get(i).getType().equals(Constant.TYPE_ITEM_SUMMARY_TITLLE)) {
                    items.add(new SummaryModel(getType(i), getDay(i), getMonthAndYear(i), getInfo(i, Constant.TYPE_ITEM_SUMMARY_USER), null));
                } else {
                    items.add(new SummaryModel(getType(i), getDay(i), getMonthAndYear(i), getInfo(i, Constant.TYPE_ITEM_SUMMARY_WALLET), getChildModel(i)));
                }
            }
            return items;
        }
        return null;
    }

    @Override
    public void hideProgress() {
        view.hideProgressUpdate();
        view.hideProgress();
    }

    private String getType(int position) {
        return mResponse.body().get(position).getType();
    }

    private String getDay(int position) {

        if (mResponse != null) {
            return mResponse.body().get(position).getCreatedAt().substring(8, 10);
        } else return null;
    }

    private String getMonthAndYear(int position) {

        String year = mResponse.body().get(position).getCreatedAt().substring(0, 4);
        String month = mResponse.body().get(position).getCreatedAt().substring(5, 7);

        return DateUtils.getMonth(month) + " " + year;
    }

    private String getInfo(int position, String type) {

        if (type.equals(Constant.TYPE_ITEM_SUMMARY_USER)) {
            return mResponse.body().get(position).getDescription().toString();
        } else {
            Description description = getDescription(mResponse.body().get(position).getDescription());
            return getStatus(description.getStatus());
        }
    }

    private ChildSummaryModel getChildModel(int position) {
        Description description = getDescription(mResponse.body().get(position).getDescription());
        return new ChildSummaryModel(description.getAmount(), description.getDestination(), description.getCode(),
                description.getType(), getHistoryData(description), description.getHistory().get(0).getUserAdmin());
    }

    private Description getDescription(Object o) {
        String json = gson.toJson(o);
        return gson.fromJson(json, Description.class);
    }

    private String getStatus(String status) {

        switch (status) {

            case Constant.APPROVED:
                return App.getInstance().getString(R.string.approved);

            case Constant.PENDING:
                return App.getInstance().getString(R.string.pending);

            case Constant.PROCESSING:
                return App.getInstance().getString(R.string.processing_item);

            case Constant.CANCELED:
                return App.getInstance().getString(R.string.canceled);

            case Constant.DENIED:
                return App.getInstance().getString(R.string.denied);

            default:
                return null;
        }
    }

    private int getIcon(String status) {

        switch (status) {

            case Constant.APPROVED:
                return R.drawable.ic_check_circle;

            case Constant.PENDING:
                return R.drawable.ic_error_black_24dp;

            case Constant.PROCESSING:
                return R.drawable.ic_access_time;

            case Constant.CANCELED:
                return R.drawable.ic_block_black_24dp;

            case Constant.DENIED:
                return R.drawable.ic_highlight_off;

            default:
                return 0;
        }
    }

    private List<HistoryModel> getHistoryData(Description description) {

        List<HistoryModel> historyModel = new ArrayList<>();
        for (int i = 0; i < description.getHistory().size(); i++) {
            historyModel.add(new HistoryModel(getIcon(description.getHistory().get(i).getNewStatus()), getStatus(description.getHistory().get(i).getNewStatus()),
                    getDateSpecific(description.getHistory().get(i).getCreatedAt())));
        }
        return historyModel;
    }

    private String getDateSpecific(String createdAt) {
        try {
            Date date = format.parse(createdAt);
            String s = sFormat.format(date);
            String[] split = s.split(" ");
            if (DateUtils.isToday(date)) {
                return split[3];
            } else {
                return split[0] + " " + DateUtils.getMonth(split[1]);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
