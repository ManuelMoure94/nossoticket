package com.rsegrp.nossoticket.ui.settingsCard.di.cardRecharge;

import com.rsegrp.nossoticket.ui.settingsCard.fragment.CardRechargeFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@CardRechargeScope
@Subcomponent(modules = {CardRechargeModule.class})
public interface CardRechargeComponent {
    void inject(CardRechargeFragment target);
}
