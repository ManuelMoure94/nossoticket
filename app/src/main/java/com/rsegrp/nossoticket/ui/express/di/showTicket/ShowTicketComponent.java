package com.rsegrp.nossoticket.ui.express.di.showTicket;

import com.rsegrp.nossoticket.ui.express.fragments.ShowTicketFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

@ShowTicketScope
@Subcomponent(modules = {ShowTicketModule.class})
public interface ShowTicketComponent {
    void inject(ShowTicketFragment target);
}
