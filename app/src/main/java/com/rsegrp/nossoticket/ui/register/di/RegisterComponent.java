package com.rsegrp.nossoticket.ui.register.di;

import com.rsegrp.nossoticket.ui.register.mvp.RegisterActivity;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

@RegisterScope
@Subcomponent(modules = {RegisterModule.class})
public interface RegisterComponent {
    void inject(RegisterActivity target);
}
