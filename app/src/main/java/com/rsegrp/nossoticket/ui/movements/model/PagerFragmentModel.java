package com.rsegrp.nossoticket.ui.movements.model;

/**
 * Created by RSE_VZLA_07 on 2/7/2018.
 */

public class PagerFragmentModel {

    private String cardType;
    private String cardId;
    private String serial;
    private int balance;
    private String description;

    public PagerFragmentModel(String cardType, String cardId, String serial, int balance, String description) {
        this.cardType = cardType;
        this.cardId = cardId;
        this.serial = serial;
        this.balance = balance;
        this.description = description;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
