package com.rsegrp.nossoticket.ui.base;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public interface BaseContract {

    interface View {

        void showError(String message);

        String getToken();
    }

    interface Presenter {

        String getToken();

        void showMessage(String message);
    }

    interface Interactor {

    }
}
