package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails;

import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.api.services.MovementServices;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class CardDetailsInteract implements CardDetailsContract.Interact {

    private MovementServices movementServices;

    public CardDetailsInteract(MovementServices movementServices) {
        this.movementServices = movementServices;
    }

    @Override
    public void getDetails(String cardId, final CardDetailsPresenter presenter) {

        Call<Movement> call = movementServices.Movement(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), cardId);

        call.enqueue(new Callback<Movement>() {
            @Override
            public void onResponse(Call<Movement> call, Response<Movement> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response, true);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<Movement> call, Throwable t) {
                presenter.retry();
            }
        });
    }

    @Override
    public void getMovement(String cardId, final String page, final CardDetailsPresenter presenter) {

        Call<Movement> call = movementServices.NextMovement(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), cardId, page);

        call.enqueue(new Callback<Movement>() {
            @Override
            public void onResponse(Call<Movement> call, Response<Movement> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response, false);
                } else if (response.code() >= 400) {
                    presenter.error();
                }
            }

            @Override
            public void onFailure(Call<Movement> call, Throwable t) {
                presenter.retry();
//                presenter.showMessage(App.getInstance().getString(R.string.connection_error));
//                presenter.hideProgress();
            }
        });
    }
}
