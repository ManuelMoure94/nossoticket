package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.RechargeTittle;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.TittleRequest;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.api.services.TitleServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class CardRechargeInteract implements CardRechargeContract.Interact {

    private TitleServices titleServices;
    private SingInServices singInServices;

    public CardRechargeInteract(TitleServices titleServices, SingInServices singInServices) {
        this.titleServices = titleServices;
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void rechargeTittle(final CardRechargePresenter presenter, final RechargeTittle rechargeTittle) {

        Call<TittleRequest> call = titleServices.tittleRecharge(Constant.CONTENT_TYPE, Constant.TOKEN +
                presenter.getToken(), rechargeTittle);

        call.enqueue(new Callback<TittleRequest>() {
            @Override
            public void onResponse(Call<TittleRequest> call, Response<TittleRequest> response) {

                Log.i("TAG", "onResponse: " + response.code());
                if (response.isSuccessful()) {
                    presenter.isSuccess(response);
                } else if (response.code() >= 400) {
                    presenter.errorConnection();
                }
            }

            @Override
            public void onFailure(Call<TittleRequest> call, Throwable t) {
                presenter.retryConnection();
            }
        });
    }

    @Override
    public void getAmount(final CardRechargePresenter presenter) {

        Call<DataUser> call = singInServices.getUser(Constant.CONTENT_TYPE, Constant.TOKEN + presenter.getToken());

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    presenter.successAmount(response);
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {

            }
        });
    }
}
