package com.rsegrp.nossoticket.ui.settingsCard.model;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class DetailsModel {

    private String line;
    private String station;
    private String date;
    private String action;
    private String amount;
    private String finalBalance;
    private int itemType;

    public DetailsModel(String line, String station, String date, String action, String amount, String finalBalance, int itemType) {
        this.line = line;
        this.station = station;
        this.date = date;
        this.action = action;
        this.amount = amount;
        this.finalBalance = finalBalance;
        this.itemType = itemType;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(String finalBalance) {
        this.finalBalance = finalBalance;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
