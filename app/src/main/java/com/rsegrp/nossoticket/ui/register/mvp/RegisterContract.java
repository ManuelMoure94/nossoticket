package com.rsegrp.nossoticket.ui.register.mvp;

import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.api.data.singUp.Register;
import com.rsegrp.nossoticket.api.data.singUp.SingUp;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public interface RegisterContract {

    interface View extends BaseContract.View {

        String getEmail();

        String getPassword();

        String getRePassword();

        void home();

        void retryPopup();

        void error();

        void errorConnection();

        void showProgress();

        void hideProgress();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnRegister();

        void onSuccessSingUn(Response<SingUp> response);

        void onSuccessSingIp(Response<SingIn> response);

        void hideProgress();

        void retry();

        void error();

        void errorConnection();
    }

    interface Interact {

        void newUser(Register register, RegisterPresenter presenter);

        void SingInNewUser(final User user, final RegisterPresenter presenter);
    }
}
