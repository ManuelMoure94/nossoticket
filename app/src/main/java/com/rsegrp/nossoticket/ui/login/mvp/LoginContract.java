package com.rsegrp.nossoticket.ui.login.mvp;

import com.rsegrp.nossoticket.api.data.singIn.SingIn;
import com.rsegrp.nossoticket.api.data.singIn.User;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public interface LoginContract {

    interface View extends BaseContract.View {

        String getEmail();

        String getPassword();

        void setEmail(String email);

        void setPassword(String password);

        void singUp();

        void home();

        void forgetFragment();

        void showProgress();

        void hideProgress();

        void retryPopup();

        void failLogin();
    }

    interface Presenter {

        void btnLogin();

        void forgetPassword();

        void sinUp();

        void isSuccess(Response<SingIn> response);

        String getAccessToken();

        void showMessage(String message);

        void retryConnection();

        void failLogin();
    }

    interface Interact {

        void checkUser(User user, LoginContract.Presenter presenter);
    }
}
