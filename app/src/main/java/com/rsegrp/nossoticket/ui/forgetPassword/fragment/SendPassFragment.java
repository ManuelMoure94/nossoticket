package com.rsegrp.nossoticket.ui.forgetPassword.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.forgot.CheckToken;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.di.sendpassword.SendPassModule;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword.SendPassContract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendpassword.SendPassPresenter;
import com.rsegrp.nossoticket.ui.login.fragment.LoginFragment;
import com.rsegrp.nossoticket.ui.loginhome.mvp.LoginHomeActivity;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendPassFragment extends BaseFragment implements SendPassContract.View, DialogsOneBtn.OnSetClickListener,
        DialogsTwoBtn.OnSetClickListener {

    @Inject
    SendPassPresenter presenter;

    @BindView(R.id.input_new_pass)
    MaterialEditText inputNewPass;
    @BindView(R.id.input_new_re_pass)
    MaterialEditText inputNewRePass;

    @BindView(R.id.btn_next_3)
    Button btnNext;

    private String[] data;
    private boolean isLogin = false;

    private String type;

    public static SendPassFragment newInstance(String dataToken) {

        SendPassFragment sendPassFragment = new SendPassFragment();
        Bundle args = new Bundle();

        args.putString(Constant.ARGS_DATA_TOKEN, dataToken);
        sendPassFragment.setArguments(args);

        return sendPassFragment;
    }

    public SendPassFragment() {
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new SendPassModule(this)).inject(this);
    }

    //4 and 5

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_new_pass, container, false);
        ButterKnife.bind(this, view);

        data = getArguments().getString(Constant.ARGS_DATA_TOKEN).split("/");

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter.checkToken(new CheckToken(data[4], data[5]));

        checkBtn();
    }

    private void checkBtn() {

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnNext();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(App.getInstance().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        return null;
    }

    @Override
    public String getPassword() {
        return inputNewPass.getText().toString();
    }

    @Override
    public String get2ndPassword() {
        return inputNewRePass.getText().toString();
    }

    @Override
    public void homeLogin() {

        Fragment fragment = new LoginFragment();
        ((LoginHomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        ((LoginHomeActivity) getActivity()).showProgress();
    }

    @Override
    public void hideProgress() {
        ((LoginHomeActivity) getActivity()).hideProgress();
    }

    @Override
    public void onSuccess(String type, boolean isLogin) {

        this.isLogin = isLogin;
        if (type.equals(Constant.TYPE_CHANGE_PASS)) {
            new DialogsOneBtn(this, getString(R.string.success_change_pass), getString(R.string.accept)).show(getChildFragmentManager(), "OnSuccess");
        }
    }

    @Override
    public void onError(String type) {

        String msg;
        if (type.equals(Constant.TYPE_CHANGE_PASS)) {
            msg = getString(R.string.error_change_pass);
        } else {
            isLogin = true;
            msg = getString(R.string.error_check_token);
        }

        new DialogsOneBtn(this, msg, getString(R.string.accept)).show(getChildFragmentManager(), "onError");
    }

    @Override
    public void onFailed(String type) {

        if (type.equals(Constant.TYPE_CHANGE_PASS)) {
            this.type = type;
        } else {
            this.type = type;
        }
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                break;

            case "retry":

                if (type.equals(Constant.TYPE_CHECK_TOKEN)) {
                    presenter.checkToken(new CheckToken(data[4], data[5]));
                } else {
                    presenter.changePassword();
                }
                break;

            case "accept":
                if (isLogin) {
                    homeLogin();
                }
                break;
        }
    }
}