package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendmail;

import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public interface SendMailContract {

    interface View extends BaseContract.View {

        String getEmail();

        void nextFragment();

        void showProgress();

        void hideProgress();

        void showMsgSuccess(String msg);
    }

    interface Presenter {

        void btnNext();

        void onSuccess(Response<String> response);

        void onError();

        void onFailed();
    }

    interface Interact {

        void sendMail(String email, SendMailPresenter presenter);
    }
}
