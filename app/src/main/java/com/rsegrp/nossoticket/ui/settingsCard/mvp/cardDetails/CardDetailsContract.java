package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardDetails;

import com.rsegrp.nossoticket.api.data.movement.Movement;
import com.rsegrp.nossoticket.ui.base.BaseContract;
import com.rsegrp.nossoticket.ui.settingsCard.model.DetailsModel;

import java.util.List;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public interface CardDetailsContract {

    interface View extends BaseContract.View {

        void setSerial();

        void setBalance();

        void setDate();

        void setColorIcon();

        void recharge();

        void block();

        void initRecycler();

        void updateList(List<DetailsModel> items);

        void showProgress();

        void hideProgress();

        void hideProgressUpdate();

        void retry();

        void error();
    }

    interface Presenter extends BaseContract.Presenter {

        void setText();

        void btnCardRecharge();

        void btnCardBlock();

        void getDetails(String cardId);

        void nextPage(String cardId);

        void onSuccess(Response<Movement> response, boolean firstTime);

        void hideProgress();

        void retry();

        void error();
    }

    interface Interact {

        void getDetails(String cardId, CardDetailsPresenter presenter);

        void getMovement(String cardId, String page, final CardDetailsPresenter presenter);
    }
}
