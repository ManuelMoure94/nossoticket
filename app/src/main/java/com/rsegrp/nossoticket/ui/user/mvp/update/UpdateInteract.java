package com.rsegrp.nossoticket.ui.user.mvp.update;

import android.util.Log;

import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.services.SingInServices;
import com.rsegrp.nossoticket.ui.root.App;
import com.rsegrp.nossoticket.utils.constants.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class UpdateInteract implements UpdateContract.Interact {

    private SingInServices singInServices;

    public UpdateInteract(SingInServices singInServices) {
        this.singInServices = singInServices;
        App.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void updateUser(DataUser dataUser, final UpdatePresenter presenter) {

        Call<DataUser> call = singInServices.updateUser(Constant.CONTENT_TYPE,
                Constant.TOKEN + presenter.getToken(), dataUser);

        call.enqueue(new Callback<DataUser>() {
            @Override
            public void onResponse(Call<DataUser> call, Response<DataUser> response) {

                if (response.isSuccessful()) {
                    presenter.onSuccess(response);
                } else {
                    Log.i("TAG", response.message() + " " + response.code());
                    presenter.onError();
                }
            }

            @Override
            public void onFailure(Call<DataUser> call, Throwable t) {
                Log.i("tag", t.getMessage());
                presenter.onFailed();
            }
        });
    }
}
