package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.root.App;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public class SendCodePresenter implements SendCodeContract.Presenter {

    private SendCodeContract.View view;
    private SendCodeContract.Interact interact;

    public SendCodePresenter(SendCodeContract.View view, SendCodeContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void btnNext() {

        if (checkCode()){
            if (interact.validateCode(view.getCode())){
                view.nextFragment();
            } else {
                view.showError(App.getInstance().getString(R.string.invalid_code));
            }
        }
    }

    private boolean checkCode() {

        if (TextUtils.isEmpty(view.getCode())){
            view.showError(App.getInstance().getString(R.string.field_code_empty));
            return false;
        } else return true;
    }
}
