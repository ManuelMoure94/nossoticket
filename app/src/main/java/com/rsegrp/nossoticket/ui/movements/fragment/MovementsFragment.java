package com.rsegrp.nossoticket.ui.movements.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.movements.adapter.MovementsListAdapter;
import com.rsegrp.nossoticket.ui.movements.adapter.MovementsPagerAdapter;
import com.rsegrp.nossoticket.ui.movements.adapter.interfaces.OnClick;
import com.rsegrp.nossoticket.ui.movements.di.MovementsModule;
import com.rsegrp.nossoticket.ui.movements.model.ListAllMovementsModel;
import com.rsegrp.nossoticket.ui.movements.model.PagerFragmentModel;
import com.rsegrp.nossoticket.ui.movements.mvp.MovementsContract;
import com.rsegrp.nossoticket.ui.movements.mvp.MovementsPresenter;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.fragment.AddCardFragment;
import com.rsegrp.nossoticket.utils.ShadowTransformer;
import com.rsegrp.nossoticket.utils.constants.Constant;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 29/6/2018.
 */

public class MovementsFragment extends BaseFragment implements MovementsContract.View, OnClick.OnClickAdapter {

    @Inject
    public MovementsPresenter presenter;

    @BindView(R.id.viewPagerMovement)
    ViewPager vpMovements;

    @BindView(R.id.swipe_title_movements)
    SwipeRefreshLayout swMovements;

    @BindView(R.id.circleIndicator)
    LinearLayout circleIndicator;

    @BindView(R.id.rv_movements)
    RecyclerView rvMovements;

    @BindView(R.id.progress_title_movements)
    RelativeLayout progressMovements;

    @BindView(R.id.progress_list_movements)
    ProgressBar progressList;

    private MovementsPagerAdapter adapter;

    private int dotsCount;
    private ImageView[] dots;

    private boolean[] updates;
    private boolean isValidUpdate = false;
    private boolean isEmpty = true;

    private List<PagerFragmentModel> fragments = new ArrayList<>();

    final private LinearLayoutManager lManager = new LinearLayoutManager(getActivity());
    private MovementsListAdapter adapterList = new MovementsListAdapter(getActivity(), this);
    ;

    private List<ListAllMovementsModel> listAllMovementsModels = new ArrayList<>();

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new MovementsModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_title_movements, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeActivity) getActivity()).showToolbar();
        ((HomeActivity) getActivity()).showPlusBottom();
        presenter.getTitles(true);
        checkBtn();

        initRecycler();
    }

    private void checkBtn() {

        vpMovements.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.nonactivity_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot));

                if (isValidUpdate) {
                    adapterList.updateListItem(listAllMovementsModels.get(position).getListMovementsModels(), true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (!isEmpty) {
            swMovements.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    if (isRefreshing()) {
//                    adapterList.updateListItem(listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels());
                        presenter.refreshMovements(vpMovements.getCurrentItem());
                    } else {
                        swMovements.setRefreshing(false);
                    }
                }
            });
        }

        rvMovements.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {

                    int visibleItemCount = lManager.getChildCount();
                    int totalItemCount = lManager.getItemCount();
                    int pastVisibleItem = lManager.findFirstVisibleItemPosition();

                    if (!isEmpty) {
                        if (updates[vpMovements.getCurrentItem()]) {
                            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {

                                updates[vpMovements.getCurrentItem()] = false;
                                listAllMovementsModels.get(vpMovements.getCurrentItem()).addListMovementsModels(Constant.TYPE_ITEM_PROGRESS, "", "", "");
                                update(listAllMovementsModels, vpMovements.getCurrentItem(), false);
                                presenter.getNextItems(vpMovements.getCurrentItem());
                            }
                        }
                    }
                }
            }
        });

        /*btnAddTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCard();
            }
        });*/
    }

    private boolean isProgressItem() {
        int size = listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels().size() - 1;
        String type = listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels().get(size).getType();

        return type.equals(Constant.TYPE_ITEM_PROGRESS);
    }

    private boolean isRefreshing() {
        int size = listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels().size() - 1;
        String type = listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels().get(size).getType();

        return !(type.equals(Constant.TYPE_ITEM_PROGRESS) || type.equals(Constant.TYPE_ITEM_NO_ITEM));
    }

    private void initRecycler() {

        rvMovements.setHasFixedSize(true);
        rvMovements.setLayoutManager(lManager);
        rvMovements.setAdapter(adapterList);

        swMovements.setRefreshing(false);

        for (int i = 0; i < dotsCount; i++) {
            updates[i] = true;
        }
    }

    @Override
    public void deleteProgressItem(int position) {
        int size = listAllMovementsModels.get(position).getListMovementsModels().size() - 1;
        listAllMovementsModels.get(position).getListMovementsModels().remove(size);
    }

    @Override
    public void initAdapterPager() {

        isEmpty = false;

        adapter = new MovementsPagerAdapter(getChildFragmentManager(), dpToPixels(2, getActivity()), getCards());
        ShadowTransformer shadowTransformer = new ShadowTransformer(vpMovements, adapter);
        shadowTransformer.enableScaling(true);

        vpMovements.setAdapter(adapter);
        vpMovements.setPageTransformer(false, shadowTransformer);
        vpMovements.setOffscreenPageLimit(3);

        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];

        updates = new boolean[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.nonactivity_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);

            circleIndicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.active_dot));
    }

    @Override
    public void showProgressList() {
        progressList.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressList() {
        progressList.setVisibility(View.GONE);
    }

    @Override
    public void hidePager() {
        vpMovements.setVisibility(View.GONE);
        circleIndicator.setVisibility(View.GONE);
        swMovements.setEnabled(false);
    }

    @Override
    public void update(List<ListAllMovementsModel> listAllMovementsModels, int position, boolean animated) {

        if (listAllMovementsModels != null) {
            this.listAllMovementsModels = listAllMovementsModels;
            if (isEmpty) {
                adapterList.updateListItem(listAllMovementsModels.get(position).getListMovementsModels(), animated);
            } else {
                adapterList.updateListItem(listAllMovementsModels.get(vpMovements.getCurrentItem()).getListMovementsModels(), animated);
            }
            isValidUpdate = true;

            int size;
            String type;

            for (int i = 0; i < dotsCount; i++) {
                size = listAllMovementsModels.get(i).getListMovementsModels().size() - 1;
                type = listAllMovementsModels.get(i).getListMovementsModels().get(size).getType();

                updates[i] = !(type.equals(Constant.TYPE_ITEM_NO_ITEM) || type.equals(Constant.TYPE_ITEM_PROGRESS)
                        || type.equals(Constant.TYPE_ITEM_EMPTY_ITEM));
            }
        }
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {

        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    public List<PagerFragmentModel> getCards() {

        for (int i = 0; i < presenter.getSize(); i++) {

            fragments.add(new PagerFragmentModel(presenter.getCardType(i), presenter.getCardId(i), presenter.getCardSerial(i), presenter.getCardAmount(i), presenter.getDescription(i)));
        }

        return fragments;
    }

    @Override
    public void addCard() {

        Fragment fragment = new AddCardFragment();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        progressMovements.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressMovements.setVisibility(View.GONE);
    }

    @Override
    public void hideProgressUpdate() {
        swMovements.setRefreshing(false);
    }

    @Override
    public void onClickAddTitle(int position) {
        addCard();
    }
}
