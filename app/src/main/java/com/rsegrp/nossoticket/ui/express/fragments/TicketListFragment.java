package com.rsegrp.nossoticket.ui.express.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.express.di.ticketList.TicketListModule;
import com.rsegrp.nossoticket.ui.express.mvp.ticketList.TicketListContract;
import com.rsegrp.nossoticket.ui.express.mvp.ticketList.TicketListPresenter;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class TicketListFragment extends BaseFragment implements TicketListContract.View {

    @Inject
    public TicketListPresenter presenter;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new TicketListModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ticket_express_list, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
