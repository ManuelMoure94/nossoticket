package com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode;

import com.rsegrp.nossoticket.ui.base.BaseContract;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

public interface SendCodeContract {

    interface View extends BaseContract.View{

        String getCode();

        void setEmail(String email);
        void nextFragment();
    }

    interface Presenter{

        void btnNext();
    }

    interface Interact{

        boolean validateCode(String code);
    }
}
