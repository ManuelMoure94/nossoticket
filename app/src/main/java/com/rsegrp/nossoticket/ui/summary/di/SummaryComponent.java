package com.rsegrp.nossoticket.ui.summary.di;

import com.rsegrp.nossoticket.ui.summary.fragment.SummaryFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 16/2/2018.
 */

@SummaryScope
@Subcomponent(modules = {SummaryModule.class})
public interface SummaryComponent {
    void inject(SummaryFragment target);
}
