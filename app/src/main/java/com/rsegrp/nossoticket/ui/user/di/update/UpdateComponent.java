package com.rsegrp.nossoticket.ui.user.di.update;

import com.rsegrp.nossoticket.ui.user.fragment.UpdateFragment;

import dagger.Subcomponent;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

@UpdateScope
@Subcomponent(modules = {UpdateModule.class})
public interface UpdateComponent {
    void inject(UpdateFragment target);
}
