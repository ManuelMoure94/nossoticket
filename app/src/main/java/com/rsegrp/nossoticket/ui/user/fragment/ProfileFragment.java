package com.rsegrp.nossoticket.ui.user.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.user.di.profile.ProfileModule;
import com.rsegrp.nossoticket.ui.user.mvp.profile.ProfileContract;
import com.rsegrp.nossoticket.ui.user.mvp.profile.ProfilePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class ProfileFragment extends BaseFragment implements ProfileContract.View {

    @BindView(R.id.show_fName)
    TextView showFName;
    @BindView(R.id.show_lName)
    TextView showLName;
    @BindView(R.id.show_date)
    TextView showDate;
    @BindView(R.id.show_id)
    TextView showId;
    @BindView(R.id.show_address)
    TextView showAddress;
    @BindView(R.id.show_email)
    TextView showEmail;
    @BindView(R.id.show_phone)
    TextView showPhone;

    @BindView(R.id.btn_profile)
    Button btnProfile;

    @BindView(R.id.progress_profile)
    RelativeLayout progressProfile;

    @Inject
    ProfilePresenter presenter;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new ProfileModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).showToolbar();

        presenter.initProfile();
        checked();
    }

    private void checked() {

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnProfile();
            }
        });
    }


    @Override
    public void setTextFName(String fName) {
        showFName.setText(fName);
    }

    @Override
    public void setTextLName(String lName) {
        showLName.setText(lName);
    }

    @Override
    public void setTextDate(String date) {
        showDate.setText(date);
    }

    @Override
    public void setTextId(String id) {
        showId.setText(id);
    }

    @Override
    public void setTextAddress(String address) {
        showAddress.setText(address);
    }

    @Override
    public void setTextMail(String mail) {
        showEmail.setText(mail);
    }

    @Override
    public void setTextPhone(String phone) {
        showPhone.setText(phone);
    }

    @Override
    public void nextFragment(DataUser dataUser) {

        Fragment fragment = UpdateFragment.newInstance(dataUser);
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void showProgress() {
        progressProfile.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressProfile.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
    }

    @Override
    public String getToken() {

        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }
}