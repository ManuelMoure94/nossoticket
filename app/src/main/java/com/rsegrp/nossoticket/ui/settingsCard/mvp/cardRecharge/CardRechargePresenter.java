package com.rsegrp.nossoticket.ui.settingsCard.mvp.cardRecharge;

import android.text.TextUtils;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.api.data.singIn.DataUser;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.RechargeTittle;
import com.rsegrp.nossoticket.api.data.titles.rechargeTittle.TittleRequest;
import com.rsegrp.nossoticket.ui.root.App;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public class CardRechargePresenter implements CardRechargeContract.Presenter {

    private CardRechargeContract.View view;
    private CardRechargeContract.Interact interact;

    public CardRechargePresenter(CardRechargeContract.View view, CardRechargeContract.Interact interact) {
        this.view = view;
        this.interact = interact;
    }

    @Override
    public void getBalance() {
        interact.getAmount(this);
    }

    @Override
    public void setText() {

        view.setSerial();
        view.setBalance();
        view.setDate();
    }

    @Override
    public void btnBalance(RechargeTittle rechargeTittle) {

        if (view.getTextBtn().equals(App.getInstance().getString(R.string.confirm))) {
            if (checkField()) {
                view.showProgress();
                view.showProcess();
                view.hideFieldRecharge();
                view.hideBtn();
                view.setTextBtn(" ");
                interact.rechargeTittle(this, rechargeTittle);
            }
        } else if (view.getTextBtn().equals(App.getInstance().getString(R.string.accept))) {
            view.goSummary();
        } else {
            view.showProcess();
        }
    }

    @Override
    public void isSuccess(Response<TittleRequest> response) {
        view.hideProgress();
        view.hideProcess();
        view.showBtn();
        view.showHeader();
        view.showMessage();
        view.setTextBtn(App.getInstance().getString(R.string.accept));
    }

    @Override
    public void successAmount(Response<DataUser> response) {

        view.setBalanceAccount(String.valueOf(response.body().getRemaining()));
    }

    @Override
    public void retryConnection() {
        view.hideProgress();
        view.hideProcess();
        view.showBtn();
        view.setTextBtn(App.getInstance().getString(R.string.confirm));
        view.retryConnection();
    }

    @Override
    public void errorConnection() {
        view.hideProgress();
        view.hideProcess();
        view.showBtn();
        view.setTextBtn(App.getInstance().getString(R.string.confirm));
        view.errorConnection();
    }

    private boolean checkField() {

        if (TextUtils.isEmpty(view.getAmount())) {
            view.showError(App.getInstance().getString(R.string.amount_empty));
            return false;
        } else return true;
    }

    @Override
    public String getToken() {
        return view.getToken();
    }

    @Override
    public void showMessage(String message) {

    }
}
