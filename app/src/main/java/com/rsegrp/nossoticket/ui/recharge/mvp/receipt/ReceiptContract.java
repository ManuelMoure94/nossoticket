package com.rsegrp.nossoticket.ui.recharge.mvp.receipt;

import com.rsegrp.nossoticket.api.data.wallet.recharge.RechargeRequest;
import com.rsegrp.nossoticket.api.data.wallet.recharge.WalletRecharge;
import com.rsegrp.nossoticket.ui.base.BaseContract;

import retrofit2.Response;

/**
 * Created by RSE_VZLA_07 on 21/2/2018.
 */

public interface ReceiptContract {

    interface View extends BaseContract.View {

        void setMessage(String message);

        void setTextBtn(String textBtn);

        void setMethod();

        void setTarget();

        void setOrigin();

        void setAmount();

        void setDate();

        void setReceipt();

        void showOrigin();

        void hideOrigin();

        void nextFragment(String fragmentTag);

        String getTextBtn();

        void showProgress();

        void hideProgress();

        void rechargingError();

        void retryConnection();
    }

    interface Presenter extends BaseContract.Presenter {

        void btnReceipt(RechargeRequest rechargeRequest);

        void checkedMethod(String method);

        void isSuccess(Response<WalletRecharge> response);

        void rechargingError();

        void retryConnection();
    }

    interface Interact {

        void rechargeRequest(ReceiptPresenter presenter, RechargeRequest request);
    }
}
