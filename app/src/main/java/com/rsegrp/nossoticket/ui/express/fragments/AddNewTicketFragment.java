package com.rsegrp.nossoticket.ui.express.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.express.di.addNewTicket.AddNewTicketModule;
import com.rsegrp.nossoticket.ui.express.mvp.addNewTicket.AddNewTicketContract;
import com.rsegrp.nossoticket.ui.express.mvp.addNewTicket.AddNewTicketPresenter;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.utils.constants.Constant;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 27/2/2018.
 */

public class AddNewTicketFragment extends BaseFragment implements AddNewTicketContract.View {

    private double remaining;

    @Inject
    public AddNewTicketPresenter presenter;

    @BindView(R.id.btn_add_ticket)
    Button btnAdd;

    @BindView(R.id.express_remaining)
    TextView expressRemaining;
    @BindView(R.id.see_ticket)
    TextView seeTicket;

    public static AddNewTicketFragment newInstance(double remaining) {

        AddNewTicketFragment addNewTicketFragment = new AddNewTicketFragment();
        Bundle args = new Bundle();
        args.putDouble(Constant.ARGS_REMAINING, remaining);
        addNewTicketFragment.setArguments(args);

        return addNewTicketFragment;
    }

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new AddNewTicketModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy_ticket_express, container, false);
        ButterKnife.bind(this, view);

        remaining = getArguments().getDouble(Constant.ARGS_REMAINING);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).showToolbar();

        expressRemaining.setText(String.format("%s Bs", remaining));
        presenter.getUser();

        checkedBtn();
    }

    private void checkedBtn() {

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnAddTicket();
            }
        });

        seeTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnExistingTickets();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public void nextFragment(Fragment fragment) {

        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void setRemaining(double remaining) {
        expressRemaining.setText(String.format("%s Bs", remaining));
    }
}