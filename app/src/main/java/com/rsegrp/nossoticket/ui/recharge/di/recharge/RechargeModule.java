package com.rsegrp.nossoticket.ui.recharge.di.recharge;

import com.rsegrp.nossoticket.ui.recharge.fragment.RechargeFragment;
import com.rsegrp.nossoticket.ui.recharge.mvp.recharge.RechargeInteract;
import com.rsegrp.nossoticket.ui.recharge.mvp.recharge.RechargePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 20/2/2018.
 */

@Module
public class RechargeModule {

    private RechargeFragment rechargeFragment;

    public RechargeModule(RechargeFragment rechargeFragment) {
        this.rechargeFragment = rechargeFragment;
    }

    @Provides
    @RechargeScope
    public RechargeFragment providesRechargeFragment(){
        return rechargeFragment;
    }

    @Provides
    @RechargeScope
    public RechargePresenter providesRechargePresenter(RechargeInteract interact){
        return new RechargePresenter(rechargeFragment, interact);
    }

    @Provides
    @RechargeScope
    public RechargeInteract providesRechargeInteract(){
        return new RechargeInteract();
    }
}
