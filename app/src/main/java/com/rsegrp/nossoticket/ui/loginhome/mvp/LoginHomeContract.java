package com.rsegrp.nossoticket.ui.loginhome.mvp;

import android.support.v4.app.Fragment;

/**
 * Created by RSE_VZLA_07 on 8/2/2018.
 */

public interface LoginHomeContract {

    interface View {

        void setFragment(Fragment fragment);

        void autoLogin();

        void showProgressForgot();

        void hideProgressForgot();

        void showMsgSuccess();
    }

    interface Presenter {

        void initFragment();

        void checkAutoLogin(boolean state);
    }

    interface Interact {

    }
}
