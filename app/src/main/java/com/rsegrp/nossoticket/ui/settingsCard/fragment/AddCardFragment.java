package com.rsegrp.nossoticket.ui.settingsCard.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rsegrp.nossoticket.R;
import com.rsegrp.nossoticket.ui.base.BaseFragment;
import com.rsegrp.nossoticket.ui.home.mvp.HomeActivity;
import com.rsegrp.nossoticket.ui.movements.fragment.MovementsFragment;
import com.rsegrp.nossoticket.ui.root.component.ApplicationComponent;
import com.rsegrp.nossoticket.ui.settingsCard.di.addCard.AddCardModule;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard.AddCardContract;
import com.rsegrp.nossoticket.ui.settingsCard.mvp.addCard.AddCardPresenter;
import com.rsegrp.nossoticket.utils.constants.Constant;
import com.rsegrp.nossoticket.utils.dialogs.DialogsOneBtn;
import com.rsegrp.nossoticket.utils.dialogs.DialogsTwoBtn;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RSE_VZLA_07 on 22/2/2018.
 */

public class AddCardFragment extends BaseFragment implements AddCardContract.View, DialogsTwoBtn.OnSetClickListener,
        DialogsOneBtn.OnSetClickListener {

    @Inject
    AddCardPresenter presenter;

    @BindView(R.id.text_serial_add)
    MaterialEditText textSerial;
    @BindView(R.id.text_description)
    MaterialEditText textDescription;

    @BindView(R.id.progress_add)
    RelativeLayout progressAdd;

    @BindView(R.id.back_arrow)
    ImageView backArrow;

    @BindView(R.id.btn_add)
    Button btnAdd;

    @Override
    protected void injectDependencies(ApplicationComponent applicationComponent) {
        applicationComponent.plus(new AddCardModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_card, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((HomeActivity) getActivity()).hideToolbar();
        ((HomeActivity) getActivity()).hidePlusBottom();

        checkedBtn();
    }

    private void checkedBtn() {

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnAdd();
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backCardList();
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getToken() {
        if (!((HomeActivity) getActivity()).loadPreferencesToken().equals(" ")) {
            return ((HomeActivity) getActivity()).loadPreferencesToken();
        } else return null;
    }

    @Override
    public String getSerial() {
        return textSerial.getText().toString();
    }

    @Override
    public String getDescription() {
        return textDescription.getText().toString();
    }

    @Override
    public void backCardList() {

        Fragment fragment = new MovementsFragment();
        ((HomeActivity) getActivity()).showToolbar();
        ((HomeActivity) getActivity()).setFragment(fragment);
    }

    @Override
    public void retry() {
        hideProgress();
        new DialogsTwoBtn(this, getString(R.string.message_failure), getString(R.string.cancel),
                getString(R.string.retry)).show(getChildFragmentManager(), Constant.RETRY_TAG);
    }

    @Override
    public void error() {
        hideProgress();
        new DialogsOneBtn(this, getString(R.string.fail_add_card), getString(R.string.accept)).show(getChildFragmentManager(), "Fail");
    }

    @Override
    public void showProgress() {
        progressAdd.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressAdd.setVisibility(View.GONE);
    }

    @Override
    public void clickListener(String options) {

        switch (options) {
            case "cancel":
                hideProgress();
                break;
            case "retry":
                presenter.btnAdd();
                break;
            case "accept":
                hideProgress();
                break;
        }
    }
}
