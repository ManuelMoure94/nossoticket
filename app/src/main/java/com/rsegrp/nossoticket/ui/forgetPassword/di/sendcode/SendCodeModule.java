package com.rsegrp.nossoticket.ui.forgetPassword.di.sendcode;

import com.rsegrp.nossoticket.ui.forgetPassword.fragment.SendCodeFragment;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode.SendCodeInteract;
import com.rsegrp.nossoticket.ui.forgetPassword.mvp.sendcode.SendCodePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by RSE_VZLA_07 on 9/2/2018.
 */

@Module
public class SendCodeModule {

    private SendCodeFragment sendCodeFragment;

    public SendCodeModule(SendCodeFragment sendCodeFragment) {
        this.sendCodeFragment = sendCodeFragment;
    }

    @Provides
    @SendCodeScope
    public SendCodeFragment providesSendCodeFragment() {
        return sendCodeFragment;
    }

    @Provides
    @SendCodeScope
    public SendCodePresenter providesSendCodePresenter(SendCodeInteract interact) {
        return new SendCodePresenter(sendCodeFragment, interact);
    }

    @Provides
    @SendCodeScope
    public SendCodeInteract providesSendCodeInteract() {
        return new SendCodeInteract();
    }
}